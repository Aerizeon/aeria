#include "A3Input.h"
#include "A3UI.h"
#define MAPVK_VK_TO_VSC    0
#define MAPVK_VSC_TO_VK    1
#define MAPVK_VK_TO_CHAR   2
#define MAPVK_VSC_TO_VK_EX 3
#define MAPVK_VK_TO_VSC_EX 4

A3Input::A3Input(HWND hwnd)
{
  RAWINPUTDEVICE Rid[2];

  Rid[0].usUsagePage = 0x01;
  Rid[0].usUsage = 0x02;
  Rid[0].dwFlags = 0;   // adds HID mouse and also ignores legacy mouse messages
  Rid[0].hwndTarget = hwnd;

  Rid[1].usUsagePage = 0x01;
  Rid[1].usUsage = 0x06;
  Rid[1].dwFlags = 0;   // adds HID keyboard and also ignores legacy keyboard messages
  Rid[1].hwndTarget = hwnd;

  if (RegisterRawInputDevices(Rid, 2, sizeof(Rid[0])) == FALSE)
    cerr << "Could not register Raw Input" << endl;
  else
    cout << "Successfully registered Raw Input" << endl;
}

void A3Input::GetInput(HWND hwnd, LPARAM lParam)
{
  char Buffer[sizeof(RAWINPUT)] = {};
  UINT Size = sizeof(RAWINPUT);
  GetRawInputData(reinterpret_cast<HRAWINPUT>(lParam), RID_INPUT, Buffer, &Size, sizeof(RAWINPUTHEADER));

  RAWINPUT* Raw = reinterpret_cast<RAWINPUT*>(Buffer);

  if (Raw->header.dwType == RIM_TYPEKEYBOARD)
  {
    const RAWKEYBOARD& RawKB = Raw->data.keyboard;
    UINT VKey   = RawKB.VKey;
    UINT SCode  = RawKB.MakeCode;
    UINT Flags  = RawKB.Flags;
    if (VKey == 255)
      return;
    else if (VKey == VK_SHIFT)
      VKey = MapVirtualKey(SCode, MAPVK_VSC_TO_VK_EX);
    else if (VKey == VK_NUMLOCK)
      SCode = (MapVirtualKey(VKey, MAPVK_VK_TO_VSC) | 0x100);

    if (Flags & RI_KEY_E1)
    {
      if (VKey == VK_PAUSE)
        SCode = 0x45;
      else
        SCode = MapVirtualKey(VKey, MAPVK_VK_TO_VSC);
    }
    wchar_t Out[8];
    unsigned char KS[255];
    GetKeyboardState(KS);
    int cnt = ToUnicode(VKey, SCode, KS, Out, 8, 0);

    for(int i = 0; i < cnt; i++)
    {
      A3KeyState KeyState;
      KeyState.Character  = Out[i];
      KeyState.Pressed    = (!(Flags & RI_KEY_BREAK));
      KeyState.Virtual    = VKey;
      KeyState.Scan       = SCode;
      A3UI::KeyEvent(KeyState);
    }
  }
  else if(Raw->header.dwType == RIM_TYPEMOUSE)
  {
    const RAWMOUSE &RawMouse = Raw->data.mouse;
    A3MouseState MouseState;
    POINT p;
    GetCursorPos(&p);
    ScreenToClient(hwnd,&p);
    MouseState.Position = glm::vec2(p.x,p.y);
    MouseState.Buttons[0] = GetAsyncKeyState(VK_LBUTTON);
    MouseState.Buttons[1] = GetAsyncKeyState(VK_RBUTTON);
    A3UI::MouseEvent(MouseState);
  }
  return;
}

A3Input::~A3Input()
{
  //dtor
}
