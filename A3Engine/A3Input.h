#ifndef A3INPUT_H
#define A3INPUT_H
#include "A3Include.h"

struct A3MouseState
{
  glm::vec2 Position    = glm::vec2();
  glm::vec2 Relative    = glm::vec2();
  bool      Buttons[10];
};

struct A3KeyState
{
  bool      Pressed     = false;
  UINT      Virtual     = 0;
  UINT      Scan        = 0;
  wchar_t   Character;
};

class A3Input
{
public:
  A3Input(HWND hwnd);
  void GetInput(HWND hwnd, LPARAM lParam);
  virtual ~A3Input();
private:
};

#endif // A3INPUT_H
