#include "A3Settings.h"


float     Settings::AFLevel;
float     Settings::AALevel;
bool      Settings::EnableVBO;
string    Settings::CachePath;
A3THeader Settings::DefaultTheme;
HWND      Settings::Handle;

void Settings::Initalize()
{
  glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &AFLevel);
  ifstream Theme("./cache/themes/default.a3t", ifstream::in|ifstream::binary);
  DefaultTheme.ParseFromIstream(&Theme);
  CachePath = "./cache/";
}
