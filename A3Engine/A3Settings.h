#ifndef SETTINGS_H
#define SETTINGS_H

#include "A3Include.h"
#include "A3Engine/Parser/Theme/A3Theme.pb.h"
#include <string>

using namespace std;

class Settings
{
public:
  Settings();
  static HWND       Handle;
  static float      AFLevel;
  static float      AALevel;
  static A3THeader  DefaultTheme;
  static float      ShaderModel;
  static string     CachePath;
  static bool       EnableVBO;
  static void Initalize();
protected:
private:
};

#endif // SETTINGS_H
