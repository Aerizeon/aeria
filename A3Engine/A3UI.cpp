#include "A3UI.h"
#include <sqstdio.h>
#include <sqrat.h>
#include <sqstdmath.h>
using namespace std;
using namespace Sqrat;


A3Camera          A3UI::CurrentCamera;
A3Panel           *A3UI::Root;
A3Program         *A3UI::TextShader;
glm::vec2         A3UI::MousePosition;
unsigned int      A3UI::MouseButtons;
bool              A3UI::Keys[256];
HSQUIRRELVM       A3UI::SquirrelVM;
Script *Main;
A3UI::A3UI()
{
}
A3Label *Input;
void A3UI::Create()
{
  cout << "Starting Squirrel" << endl;
  /*SquirrelVM = sq_open(2048);
  sq_setcompilererrorhandler(SquirrelVM, SquirrelErrorHandler);
  sq_setprintfunc(SquirrelVM,SquirrelOutputHandler, NULL);
  //sq_setnativedebughook(SquirrelVM,SquirrelDebug);
  sq_pushroottable(SquirrelVM);
  sqstd_register_mathlib(SquirrelVM);
  sq_pop(SquirrelVM,1);
  sq_newclosure(SquirrelVM, SquirrelRuntimeError, 0); // NEW!
  sq_seterrorhandler(SquirrelVM); // NEW!
  Sqrat::ErrorHandling::Enable(true);
  DefaultVM::Set(SquirrelVM);
  RootTable().Func("PrintTable",SquirrelPrintTable);
  //DefaultAllocator<A3Vector2f>
  RootTable().Bind("A3Vector2f",Class<glm::vec2,ConstAlloc<glm::vec2,float,float>>(SquirrelVM,"A3Vector2f")
                   .Var("X", &glm::vec2::x)
                   .Var("Y", &glm::vec2::y));
  RootTable().Bind("A3Region2f",Class<A3Region2f/*, ConstAlloc<A3Region2f,A3Vector2f, A3Vector2f>>(SquirrelVM,"A3Region2f")
                   .Var("Position", &A3Region2f::Position)
                   .Var("Size", &A3Region2f::Size));

  RootTable().Bind("A3Texture",Class<A3Texture,ConstAlloc<A3Texture,unsigned int>>(SquirrelVM,"A3Texture")
                   .StaticFunc("Create", &A3Texture::SQCreate)
                   .Var("Flags",&A3Texture::Flags));

  RootTable().Bind("A3Panel",Class<A3Panel,ConstAlloc<A3Panel,string,glm::vec2,glm::vec2,unsigned int>>(SquirrelVM,"A3Panel")
                   .Func("SetPosition", &A3Panel::SetPosition)
                   .Func("GetPosition",&A3Panel::GetPanelPosition)
                   .Func("SetPosition",&A3Panel::SetPosition)
                   .Func("GetSize",&A3Panel::GetPanelSize)
                   .Var("PanelRegion",&A3Panel::PanelRegion)
                   .Var("Name",&A3Panel::Name)
                   .Var("EventClose",&A3Panel::OnSqClose)
                   .Var("EventDraw", &A3Panel::OnSqDraw)
                   .Func("SetTexture",&A3Panel::SetTexture)
                   .Func("AddChild",&A3Panel::AddChild)
                   .Func("DeleteChild",&A3Panel::DeleteChild));

  RootTable().Bind("A3Window",DerivedClass<A3Window,A3Panel,ConstAlloc<A3Window,string,glm::vec2,glm::vec2,unsigned int>>(SquirrelVM,"A3Window")
                   .Func("SetPosition", &A3Window::SetPosition)
                   .Func("GetPosition",&A3Window::GetPanelPosition)
                   .Func("GetSize",&A3Window::GetPanelSize)
                   .Func("SetTexture",&A3Window::SetTexture)
                   .Func("AddChild",&A3Window::AddChild));


  Main = new Script();

  try
  {
    Main->CompileFile("test.sq");
    Main->Run();
  }
  catch(...)
  {
    //cout << ex.Message() << endl;
  }
  cout << "Compiled" << endl;*/
  srand(time(0));
  for(int i = 0; i< 256; i++)
    Keys[i] = 0;
  MouseButtons = 0;

  TextShader = new A3Program("Text");
  CurrentCamera.Name = "CAMERA2D";
  CurrentCamera.Initalize(A3Projection_Isometric,128,128);
  A3Texture *T = A3Texture::Create("Default.png", A3Texture_NoFilter);
  Root = new A3Window("RootNode", glm::vec2(0,0), glm::vec2(1024,1024), A3Style_Renderless);
  //Sqrat::RootTable().SetInstance("Root", Root);
  //Sqrat::Object SqRoot(Root);
  //Sqrat::ErrorHandling::Enable(true);
  //Sqrat::RootTable(SquirrelVM).Bind("Root", SqRoot);
  /*try
  {
    Sqrat::Function funcA(Sqrat::RootTable(), "OnInit");
    if(!funcA.IsNull())
      funcA.Execute();
  }
  catch(...)
  {
    //cout << ex.Message() << endl;
  }*/
  A3Panel *MainPanel = new A3Window("MainWindow", glm::vec2(8,0), glm::vec2(800,800));
  MainPanel->SetTexture(T);
  Root->AddChild(MainPanel);


  A3Font *F = A3Font::Create("./cache/fonts/Neou-Bold.ttf",12.0);
  F->GetGlyphs(L"ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890~`!@#$%^&*()_+{}|[]\\:\";'<>?,./ ");
  A3Font *FA = A3Font::Create("./cache/fonts/segoeui.ttf",12.0);
  FA->GetGlyphs(L"ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890~`!@#$%^&*()_+{}|[]\\:\";'<>?,./ ");

  for(int i = 0; i < 1; i++)
  {
    A3Window *SubPanel =  new A3Window("ChildWindow", glm::vec2(5*i,0), glm::vec2(300,300), A3Style_Default);
    A3Panel *TextBox = new A3Textbox("TextBox", glm::vec2(10,10),glm::vec2(200,25), A3Style_Borderless);
    TextBox->SetTexture(T);
    TextBox->Attachment = A3Attachment_Center;

    A3Panel *TextBox2 = new A3Textbox("Textbox", glm::vec2(0,0),glm::vec2(200,25), A3Style_Default);
    TextBox2->SetTexture(T);
    // TextBox2->Attachment = A3Attachment_Center;


    SubPanel->SetTexture(T);
    A3Label *TestLabel = new A3Label(L"Test Window - Nested labels via Freetype & OpenGL sdfhkjsdfjhsdkfhjskdfhskdhfksdhfksdhfksdhfkjsdhfsdjhfkjhsfksjhfkshdfkhsdfkjhsdfkjshdfkhsdkfhjskdfhjksfd",F);
    TestLabel->SetPosition(glm::vec2(20,40));
    SubPanel->AddChild(TestLabel);
    A3Label *TestLabel2 = new A3Label(L"Test Window - Nested labels via Freetype & OpenGL sdfhkjsdfjhsdkfhjskdfhskdhfksdhfksdhfksdhfkjsdhfsdjhfkjhsfksjhfkshdfkhsdfkjhsdfkjshdfkhsdkfhjskdfhjksfd",F);
    TestLabel2->SetPosition(glm::vec2(-60,310));
    SubPanel->AddChild(TestLabel2);
    SubPanel->AddChild(TextBox);
    SubPanel->AddChild(TextBox2);
    MainPanel->AddChild(SubPanel);
  }
}

void A3UI::AddPanel(A3Panel *P)
{
  Root->AddChild(P);
}

void A3UI::Render(float Delta)
{
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_SAMPLE_ALPHA_TO_COVERAGE);
  A3Panel *Target = NULL;
  CurrentCamera.Render(Delta);
  glUseProgram(A3UI::TextShader->ID);
  Root->Render(Delta);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_SAMPLE_ALPHA_TO_COVERAGE);
}

void A3UI::DeletePanel(A3Panel *P)
{
  Root->DeleteChild(P);
  delete P;
}

void A3UI::Destroy()
{
  //sq_close(SquirrelVM);
  delete Root;
}

void A3UI::MouseEvent(A3MouseState State)
{
  Root->MouseEvent(State);
}

void A3UI::KeyEvent(A3KeyState State)
{
  Root->KeyEvent(State);
}

A3UI::~A3UI()
{
  //dtor
}
