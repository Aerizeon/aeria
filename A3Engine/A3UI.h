#ifndef UI_H
#define UI_H

#include "A3Include.h"
#include "A3Engine/A3World.h"
#include "A3Engine/Entity/A3Camera.h"
#include "A3Engine/Entity/A3Label.h"
#include "A3Engine/Entity/A3Program.h"
#include "A3Engine/Entity/A3Window.h"
#include "A3Engine/Entity/A3Textbox.h"
#include "A3Engine/Data/A3Font.h"
#include <squirrel.h>
#include<winuser.h>



class A3UI
{
public:
  static A3Camera           CurrentCamera;
  static A3Program          *TextShader;
  static glm::vec2          MousePosition;
  static unsigned int       MouseButtons;
  static bool               Keys[256];
  static HSQUIRRELVM        SquirrelVM;
  static A3Panel            *Root;

  A3WindowControlFlags F;

  A3UI();
  static void Create();
  static void AddPanel(A3Panel *P);
  static void Focus(A3Panel *P);
  static void Render(float Delta);
  static void DeletePanel(A3Panel *P);
  static void Destroy();

  static void MouseEvent(A3MouseState State);
  static void KeyEvent(A3KeyState);
  virtual ~A3UI();
protected:
private:

};

#endif // UI_H
