#include "A3World.h"
#include <iostream>

#include "A3UI.h"
#include "A3Engine/Entity/A3Model.h"
#include "A3Engine/Data/A3Shader.h"
#include "A3Engine/Entity/A3Light.h"
#include "A3Engine/A3Settings.h"

using namespace std;

A3Camera        A3World::CurrentCamera;
Culling         A3World::CullingType;
list<A3Entity*>   A3World::Entities;
bool            A3World::SelectStart = false;
A3Program       *A3World::ShaderLighting;
A3Program       *A3World::ShaderShadow;
A3Program       *A3World::ActiveShader;

glm::vec3       A3World::RayOrigin;
glm::vec3       A3World::RayDirection;
A3Light         *A3World::Light;
A3Model *MM ;

list<A3Entity*>::reverse_iterator First;
list<A3Entity*>::reverse_iterator Current;
list<A3Entity*>::reverse_iterator End;

void A3World::Create()
{
  End = Entities.rend();
  First = Current = Entities.rbegin();
  A3Texture::Error = A3Texture::Create(128,0,0,255);
  A3Texture::Error->Name = "ERROR_TEX";
  A3Texture::ErrorMask = A3Texture::Create(255,255,255,255, A3Texture_Mask);
  A3Texture::ErrorMask->Name = "ERROR_MASK";

  //ShaderLighting = new A3Program("lighting");
  //ShaderShadow = new A3Program("Base");

  CurrentCamera.Name = "CAMERA3D";
  CurrentCamera.FOV = 70.0f;
  CurrentCamera.Initalize(A3Projection_Perspective,128,128,0.5,1000);
  Settings::Initalize();

  glm::quat Q = glm::quat(glm::vec3(glm::radians(0.0),glm::radians(0.0),glm::radians(0.0)));
  MM = new A3Model();
  MM->Create("plane.obj",glm::vec3(0,0,0));
  MM->Texture = A3Texture::Create("tile.png");
  MM->Position = glm::vec3(0,0,0);
  MM->Scale = glm::vec3(1000,1000,1000);
  MM->Rotation = Q;
  AddEntity(MM);

  MM = new A3Model();
  MM->Create("palmtree5.rwx", glm::vec3(0,0,-10));
  MM->Scale = glm::vec3(1,1,1);
  AddEntity(MM);

    MM = new A3Model();
  MM->Create("palmtree5.obj", glm::vec3(-0.5,0,-10));
  MM->Scale = glm::vec3(1,1,1);
  MM->Texture = A3Texture::Create("tile.png");
  AddEntity(MM);
  MM = new A3Model();
  MM->Create("birde.rwx", glm::vec3(0.5,0.25,-10));
  MM->Scale = glm::vec3(4,4,4);
  AddEntity(MM);
  MM = new A3Model();
  MM->Create("bunny.obj", glm::vec3(0.5,0.25,-9));
  MM->Scale = glm::vec3(1,1,1);
  MM->Texture = A3Texture::Create("fur.jpg");
  AddEntity(MM);
  MM = new A3Model();
  MM->Create("pp01.obj", glm::vec3(1.5,0,-10));
  MM->Scale = glm::vec3(1,1,1);
  AddEntity(MM);
  /*for(int i = 0; i< 100; i++)
  {
      for(int z = 0; z< 10; z++)
      {
                for(int y = 0; y< 1; y++)
      {

      }
      }
  }/**/

  CurrentCamera.Position = glm::vec3(0,-0.25,0);
}

void A3World::Enter(int Size, string Name)
{
  Destroy();
}

void A3World::AddEntity(A3Entity *E)
{
  Entities.push_back(E);
  End = Entities.rend();
  First = Entities.rbegin();
}



void A3World::Render(float Delta)
{
  Current = First;
  CurrentCamera.Render(Delta);
  while(Current != End)
  {
    //if(!CurrentCamera.Cull((*First)))
    {
      (*Current++)->Render(Delta);
    }
  }
}

void A3World::DeleteEntity(A3Entity *E)
{
  for(list<A3Entity*>::reverse_iterator i = Entities.rbegin(); i != Entities.rend(); ++i)
  {
    if((*i) == E)
    {
      delete (*i);
      Entities.remove(*i);
      return;
    }
  }
  End = Entities.rend();
  First = Entities.rbegin();
}

void A3World::Destroy()
{
  Entities.clear();
}
