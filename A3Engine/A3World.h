#ifndef A3WORLD_H
#define A3WORLD_H

#include "A3Engine/Entity/A3Camera.h"
#include "A3Engine/Entity/A3Light.h"
#include <string>
#include "A3Engine/Entity/A3Entity.h"
#include "A3Engine/Entity/A3Program.h"
#include <list>

using namespace std;

enum Culling:
char
{
  C_DISABLE_CHK = 0,
  C_FRUSTUM_CHK = 1,
  C_OCCLUDE_CHK = 2,
  C_PORTALS_CHK = 4
};

class A3World
{
public:
  static int              WorldSize;
  static int              UserLimit; //May be used for conn. limiting.
  static string           WorldName;
  static A3Camera         CurrentCamera;
  static Culling          CullingType;
  static list<A3Entity*>  Entities;
  static A3Program        *ShaderShadow;
  static A3Program        *ShaderLighting;
  static A3Program        *ActiveShader;
  static bool             SelectStart;
  static glm::vec3        RayOrigin;
  static glm::vec3        RayDirection;
  static A3Light          *Light;

  static void Create();
  static void Enter(int Size, string Name);
  static void AddEntity(A3Entity *E);
  static void Render(float Delta);
  static bool CullFrustum(A3Entity *E);
  static void DeleteEntity(A3Entity *E); //Only used for special cases (not view range)
  static void Destroy();
protected:
};

#endif // A3WORLD_H
