#ifndef A3DATA_H
#define A3DATA_H

#define VBO_DATA 0
#define VBO_INDICES 1
#define VBO_MAX 2

#include "A3Include.h"

enum A3DataError :
char
{
  ERR_NONE                = 0,
  ERR_FILE_TYPE_WRONG     = 1,
  ERR_FILE_PATH_WRONG     = 2,
  ERR_FILE_FORMAT_WRONG   = 4,
  ERR_FILE_LENGTH_WRONG   = 8,
  ERR_FILE_DATA_WRONG     = 16
};

class A3Data
{
public:
  string                  Name        = "";
  unsigned int            References  = 1;
  unsigned short          State       = ST_NULL;
  list<A3Data*>           Children;

  A3Data(string DName);
  ~A3Data();

  static list<A3Data*>    Cache;

  static A3Data   *Create(string File);
  static void     Destroy(A3Data *D);

protected:
private:
};

#endif // A3DATA_H
