#include "A3Font.h"

list <A3Font*>  A3Font::Cache;
FT_Library      A3Font::FreeType;

A3Font::A3Font(string File, float Size)
{
  Name = File;
  References = 1;
  FontSize = Size;
  TextureWidth = TextureHeight = 256;
  if(FT_Init_FreeType(&FreeType))
  {
    cout << "Could not init freetype library" << endl;
    return;
  }
  if(FT_New_Face(FreeType, File.c_str(), 0, &Face))
  {
    cout << "Could not open font " << File << endl;
    return;
  }
  if(FT_Select_Charmap(Face, FT_ENCODING_UNICODE ))
  {
    cout << "Could not set charmap" << endl;
    FT_Done_Face(Face);
    FT_Done_FreeType(FreeType);
    return;
  }

  FT_Set_Char_Size(Face,0,FontSize*64,0,96);
  FontHeight = (Face->bbox.yMax - Face->bbox.yMin)* ((float)Face->size->metrics.y_ppem / Face->units_per_EM);
  /*Create a texture that will be used to hold all glyphs */
  glActiveTexture(GL_TEXTURE0);
  glGenTextures(1, &Texture);
  glBindTexture(GL_TEXTURE_2D, Texture);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);


  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_BASE_LEVEL,0);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAX_LEVEL,0);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, TextureWidth, TextureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
  glGenerateMipmap(GL_TEXTURE_2D);
}

vector<A3Glyph> A3Font::GetGlyphs(wstring CharList)
{
  const wchar_t* List = CharList.c_str();
  unsigned int Nxt_Index = FT_Get_Char_Index(Face, List[0]);
  unsigned int Cur_Index = 0;
  map<wchar_t,A3Glyph*>::iterator Current;
  vector<A3Glyph> Out;
  for(int i = 0; i < CharList.size(); i++)
  {
    Current = Glyphs.find(List[i]);
    Cur_Index = Nxt_Index;
    if(i + 1 < CharList.size())
      Nxt_Index = FT_Get_Char_Index(Face, List[i+1]);
    A3Glyph *CurGlyph = 0;
    if(Current != Glyphs.end())
    {
      CurGlyph = Current->second;
    }
    else
    {
      CurGlyph = GenGlyph(List[i]);
      Glyphs.insert(pair<wchar_t,A3Glyph*>(List[i],CurGlyph));
    }
    A3Glyph OutGlyph = *CurGlyph;
    if(Cur_Index && Nxt_Index)
    {
      FT_Vector Pos;
      FT_Get_Kerning(Face, Cur_Index, Nxt_Index, FT_KERNING_DEFAULT, &Pos );
      OutGlyph.Advance.x += floor(Pos.x >> 6);
    }
    Out.push_back(OutGlyph);
  }
  return Out;
}

A3Glyph* A3Font::GenGlyph(wchar_t Char)
{
  FT_GlyphSlot g = Face->glyph;
  if(FT_Load_Char(Face, Char, FT_LOAD_DEFAULT|FT_LOAD_TARGET_LIGHT))
    wcerr << L"Failed to load character " << Char << endl;
  if(FT_Render_Glyph(Face->glyph, FT_RENDER_MODE_LIGHT ))
    wcerr << L"Failed to render character " << Char << endl;
  if(PenX + g->bitmap.width + 1 >= TextureWidth)
  {
    PenY += PenYMax;
    PenX = PenYMax = 0;
  }
  if(PenY > TextureHeight)
    return nullptr;

  A3Glyph *G = new A3Glyph();
  G->Advance.x = floor(g->advance.x >> 6);
  G->Advance.y = floor(g->advance.y >> 6);

  G->Size.x = floor(g->bitmap.width);
  G->Size.y = floor(g->bitmap.rows);

  G->Position.x = floor(g->bitmap_left);
  G->Position.y = floor(-g->bitmap_top);

  if(G->Size.x + G->Size.y > 0)
  {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, Texture);
    glTexSubImage2D(GL_TEXTURE_2D, 0, PenX, PenY, g->bitmap.width, g->bitmap.rows, GL_ALPHA, GL_UNSIGNED_BYTE, g->bitmap.buffer);

    G->TexCoord.x = (PenX / (float)TextureWidth);
    G->TexCoord.y = (PenY / (float)TextureHeight);
    PenX += g->bitmap.width + 1;
    PenYMax = std::max(PenYMax, (unsigned int)g->bitmap.rows);
  }
  else
  {
    G->TexCoord.x = G->TexCoord.y = 0;
  }
  return G;
}

A3Font *A3Font::Create(string File, float Size)
{
  for(list<A3Font*>::iterator i = Cache.begin(); i != Cache.end(); ++i)
  {
    if((*i)->Name==File && (fabs((*i)->FontSize - Size) < 0.01))
    {
      (*i)->References++;
      return (*i);
    }
  }
  A3Font *F = new A3Font(File, Size);
  Cache.push_back(F);
  cout << "Pushed new font " << File << " to cache" << endl;
  return F;
}

void A3Font::Destroy(A3Font *F)
{
  if(--F->References <= 0)
  {
    cout << "Removed font " << F->Name << " from cache" << endl;
    if(F->References < 0)
      cout << "Possible error removing " << F->Name << ". Refrence index less than zero."<<endl;
    Cache.remove(F);
    delete F;
    F = NULL;
  }
}

A3Font::~A3Font()
{
  //dtor
}
