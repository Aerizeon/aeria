#ifndef A3FONT_H
#define A3FONT_H

#include <ft2build.h>
#include <freetype.h>
#include <ftglyph.h>
#include <ftoutln.h>
#include <fttrigon.h>
#include <string>
#include <list>
#include <vector>
#include <map>
#include <iostream>

#include "A3Include.h"
#include "A3Engine/Structure/A3Vector.h"


using namespace std;

struct A3Glyph
{
  glm::vec2      Advance;
  glm::vec2      Size; //bw+bh
  glm::vec2      Position;//bl+bt
  glm::vec2      TexCoord;//tx+ty
};

class A3Font
{
public:
  unsigned int      TextureWidth;
  unsigned int      TextureHeight;
  unsigned int      FontHeight;
  A3Font            (string File, float Size);
  vector<A3Glyph>  GetGlyphs(wstring CharList);
  A3Glyph*          GenGlyph(wchar_t Char);
  unsigned int&      GetTexture()
  {
    return Texture;
  }
  virtual ~A3Font();

  static A3Font *Create(string File, float Size = 9.0f);
  static void   Destroy(A3Font *F);
protected:
private:
  unsigned int            ID        = 0;
  string                  Name      = "";
  unsigned short          State     = ST_NULL;
  unsigned int            References;

  unsigned int            Texture;

  map<wchar_t, A3Glyph*>  Glyphs;
  FT_Face                 Face;
  unsigned int            PenX      = 1;
  unsigned int            PenY      = 0;
  unsigned int            PenYMax   = 0;
  float                   FontSize  = 0;

  static FT_Library     FreeType;
  static list<A3Font*>  Cache;

};

#endif // A3FONT_H
