// *** ADDED BY HEADER FIXUP ***
#include <algorithm>
#include <iterator>
// *** END ***
#include "A3Mesh.h"

#include <iostream>
#include <fstream>
#include <string>

#include "A3Engine/Parser/Mesh/A3RWXParser.h"
#include "A3Engine/Parser/Mesh/A3MParser.h"
#include "A3Engine/Parser/Mesh/A3OBJParser.h"

using namespace std;

list <A3Mesh*> A3Mesh::Cache;
unsigned int A3Mesh::Last = 0;


A3Mesh::A3Mesh(string FileName)
  :A3Data(FileName)
{
  State = ST_ERROR;

  std::string LocalPath = "./cache/models/" + FileName;
  ifstream File;
  File.open(LocalPath,ifstream::in|ifstream::binary);
  if(!File)
  {
    cout << "Mesh::Create Error: Could not find " << FileName << endl;
    State = ST_DISABLED;
    return;
  }
  char FileError;
  A3RWXParser RWP;
  A3OBJParser OBP;
  if((FileError = RWP.Parse(File,this)) != ERR_NONE)
  {
    if(FileError != ERR_FILE_TYPE_WRONG)
    {
      cout << "Could not load RWX file: Error #" << (int)FileError << endl;
      File.close();
      return;
    }
    File.seekg(0,ios::beg);
    File.clear();
    if((FileError = OBP.Parse(File,this)) != ERR_NONE)
    {
      if(FileError != ERR_FILE_TYPE_WRONG)
      {
          cout << "Could not load OBJ file: Error #" << (int)FileError << endl;
          File.close();
          return;
      }
      File.seekg(0,ios::beg);
      File.clear();
    }
  }
/**/
  File.close();
  State = ST_LOADED;
}


void A3Mesh::Render(float Delta, bool TextureOverride)
{
  switch(State)
  {
  case ST_ENABLED:
  {
    glBindVertexArray(VAO);
    break;
  }
  case ST_LOADED:
  {
    glGenVertexArrays(1,&VAO);
    glBindVertexArray(VAO);
    glGenBuffers(2,VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[VBO_DATA]);
    glBufferData(GL_ARRAY_BUFFER, VertexCount * sizeof(A3Vertex) , 0, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, VertexCount * sizeof(A3Vertex), Vertices);

    glEnableVertexAttribArray(A3World::ShaderLighting->VertexPointer);
    glEnableVertexAttribArray(A3World::ShaderLighting->NormalPointer);
    glEnableVertexAttribArray(A3World::ShaderLighting->TexCoordPointer);

    glVertexAttribPointer(A3World::ShaderLighting->VertexPointer, 3, GL_FLOAT, GL_FALSE, sizeof(A3Vertex), offsetof(A3Vertex,X));
    glVertexAttribPointer(A3World::ShaderLighting->NormalPointer, 3, GL_FLOAT, GL_FALSE, sizeof(A3Vertex), (void*)offsetof(A3Vertex,NX));
    glVertexAttribPointer(A3World::ShaderLighting->TexCoordPointer, 2, GL_FLOAT, GL_FALSE, sizeof(A3Vertex), (void*)offsetof(A3Vertex,U));

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO[VBO_INDICES]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, IndexCount * sizeof(unsigned int), 0, GL_STATIC_DRAW);
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, IndexCount * sizeof(unsigned int), Indices);
    State = ST_ENABLED;
  }
  default:
  {
    return;
  }
  }


  for(unsigned int i =0; i<GroupCount; i++)
  {
    if(!TextureOverride)
    {
      if(Group[i].Material.Texture != NULL)
        Group[i].Material.Texture->Render(Delta);
      else
        A3Texture::Error->Render(Delta);

      if(Group[i].Material.Mask != NULL)
        Group[i].Material.Mask->Render(Delta);
      else
        A3Texture::ErrorMask->Render(Delta);
    }
    glDrawElements(GL_TRIANGLES, Group[i].IndexCount, GL_UNSIGNED_INT,(void*)Group[i].IndexOffset);
    //glDrawRangeElements(GL_TRIANGLES, Group[i].IndexOffset, Group[i].IndexOffset + Group[i].IndexCount-1, Group[i].IndexCount, GL_UNSIGNED_INT, 0);
  }
}

A3Mesh::~A3Mesh()
{
  State = ST_NULL;
  glDeleteBuffersARB(2, VBO);
  for(int i =0; i<GroupCount; i++)
  {
    if(Group[i].Material.Texture)
      A3Texture::Destroy(Group[i].Material.Texture);
  }

}
A3Mesh *A3Mesh::Create(string File)
{
  for(list<A3Mesh*>::iterator i = Cache.begin(); i != Cache.end(); ++i)
  {
    if((*i)->Name==File)
    {
      (*i)->References++;
      return (*i);
    }
  }
  A3Mesh *M = new A3Mesh(File);
  Cache.push_back(M);
  return M;
}

void A3Mesh::Destroy(A3Mesh *M)
{
  if(--M->References == 0)
  {
    Cache.remove(M);
    delete M;
    M = NULL;
    return;
  }
}
