// *** ADDED BY HEADER FIXUP ***
#include <list>
#include <string>
// *** END ***
#ifndef MESH_H
#define MESH_H

#include <vector>
#include "A3Include.h"
#include "A3Engine/A3World.h"
#include "A3Engine/Data/A3Texture.h"
#include "A3Engine/Data/A3Data.h"

using namespace std;

struct A3Material
{
  string Name;
  A3Texture *Mask     = NULL;
  A3Texture *Texture  = NULL;
};

struct A3VertexGroup
{
  string          Name;
  unsigned int    IndexCount  = 0;
  unsigned int    IndexOffset = 0;
  A3Material      Material;
};


class A3Mesh: public A3Data
{
public:
  bool          HasAlpha      = false;
  unsigned int  VAO           = 0;
  unsigned int  VBO[VBO_MAX];
  unsigned int  VertexCount   = 0;
  unsigned int  GroupCount    = 0;
  unsigned int  IndexCount    = 0;
  A3Vertex      *Vertices     = 0;
  unsigned int  *Indices      = 0;
  static unsigned int   Last;
  A3Mesh        *Parent       = 0;
  vector<A3VertexGroup> Group;

  A3Mesh(string FileName);
  void Render(float Delta, bool TextureOverride);
  A3Mesh* CreateChild();

  virtual ~A3Mesh();

  static A3Mesh *Create(string File);
  static void Destroy(A3Mesh *M);
private:
  static list<A3Mesh*> Cache;

};

#endif // MESH_H
