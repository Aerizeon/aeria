#include "A3Shader.h"

A3Shader::A3Shader(string FileName, unsigned int Type)
  :A3Data(FileName)
{
  ID = glCreateShader(Type);
  References = 1;
  std::string LocalPath = "./cache/shaders/" + FileName;
  cout << LocalPath<<endl;
  ifstream File;
  File.open(LocalPath,ios_base::binary);
  if(!File)
  {
    cout << "Could not open shader" << endl;
    return;
  }
  File.seekg (0, ios::end);
  unsigned int Length = File.tellg();
  File.seekg (0, ios::beg);

  char *Data = new char[Length];
  File.read(Data,Length);
  glShaderSource(ID,1,(const char **)&Data,(int*)&Length);
  glCompileShader(ID);

  int RC;
  int RCLength;

  glGetShaderiv(ID, GL_COMPILE_STATUS, &RC);
  if(RC == GL_FALSE)
  {
    glGetShaderiv(ID, GL_INFO_LOG_LENGTH, &RCLength);
    char *log = new char[RCLength];
    glGetShaderInfoLog(ID, RCLength, &RC, log);

    cout << "Unable to compile Shader:" << log << endl;
    delete[] log;
    log = NULL;
    glDeleteShader(ID);
  }
  File.close();
  delete[] Data;
  Data = NULL;

}

A3Shader::~A3Shader()
{
  //glDeleteShader(ID);
}
