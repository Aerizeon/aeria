// *** ADDED BY HEADER FIXUP ***
#include <string>
// *** END ***
#ifndef SHADER_H
#define SHADER_H

#include <list>
#include "A3Engine/Data/A3Data.h"

using namespace std;
class A3Shader:public A3Data
{
public:
  unsigned int  ID;

  A3Shader  (string File, unsigned int Type);
  void      Render(float Delta);
  virtual   ~A3Shader();


  static list<A3Shader*>    Cache;
  static unsigned int     Last;

  static A3Shader   *Create(string FileName);
  static void     Destroy(A3Shader *M);
protected:
private:

};

#endif // SHADER_H
