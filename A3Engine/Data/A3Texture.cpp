#include "A3Texture.h"

#include <string>
#include <sstream>

#include "A3Engine/A3Settings.h"
#include "A3Engine/A3World.h"
#include "A3Engine/Parser/Texture/A3BMPParser.h"
#include "A3Engine/Parser/Texture/A3JPGParser.h"
#include "A3Engine/Parser/Texture/A3PNGParser.h"

using namespace std;

list <A3Texture*>   A3Texture::Cache;
unsigned int        A3Texture::Last     =   0;
A3Texture           *A3Texture::Error;
A3Texture           *A3Texture::ErrorMask;

A3Texture::A3Texture(unsigned short PFlags)
  :A3Data(""), Flags(PFlags), TextureUnit(0)
{

}

bool A3Texture::CreateFromFile(const string &PFile)
{
  Name = PFile;
  std::string LocalPath = "./cache/textures/" + Name;
  ifstream File;
  File.open(LocalPath,ios_base::binary);
  if(!File)
  {
    cout << "Could not find " << Name << endl;
    return false;
  }

  char RC = ERR_NONE;
  if(A3PNGParser::Test(File))
    RC = A3PNGParser::Parse(File, this);
  //else if(A3JPGParser::Test(File))
    //RC = A3JPGParser::Parse(File, this);
  else
    RC = A3BMPParser::Parse(File, this);
  File.close();

  if(RC != ERR_NONE)
  {
    cout << "ERR: " << (unsigned int)RC << " in " << PFile << endl;
    return false;
  }
  Resize();
  State = ST_LOADED;
  return true;
}

bool A3Texture::CreateFromSize(const glm::ivec2 &PSize)
{
  Name = "RawTextureBuffer";
  Size = PSize;
  Channels = 3;
  State = ST_LOADED;
  if((Flags & A3Texture_FBO) == A3Texture_FBO)
  {
    cout << "Creating FBO target" << endl;
    glGenTextures(1, &GLID);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, GLID);
    /*glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);/**/
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 4);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, Settings::AFLevel);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
    glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, Size.x, Size.y, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
    if(Flags & A3Texture_NoFilter != A3Texture_NoFilter)
      glGenerateMipmap(GL_TEXTURE_2D);
    State = ST_ENABLED;
    return true;
  }
  else
  {
    cout << "Creating Raw Buffer" << endl;
    Data = new char[Size.x * Size.y * 3];
    return true;
  }
}

bool A3Texture::CreateFromColor (const unsigned char Red, const unsigned char Green, const unsigned char Blue, const unsigned char Alpha)
{
  stringstream name;
  name << "COLOR_" << (unsigned int)Red << "," << (unsigned int)Green << "," << (unsigned int)Blue << "," << (unsigned int)Alpha << "::";
  Name = name.str();
  Size.x = 1;
  Size.y = 1;
  Channels = 4;
  Data = new char[4];
  Data[0] = Red;
  Data[1] = Green;
  Data[2] = Blue;
  Data[3] = Alpha;
  State = ST_LOADED;
  if(Alpha < 250)
    Flags |= A3Texture_Transparent;
  return true;
}

void A3Texture::Render(float Delta)
{
  switch(State)
  {
  case ST_ENABLED:
  {
    if((Flags & A3Texture_Mask) == A3Texture_Mask)
      glActiveTexture(GL_TEXTURE2);
    else
      glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, GLID);
    return;
  }
  case ST_LOADED:
  {
    glGenTextures(1, &GLID);
    if((Flags & A3Texture_Mask) == A3Texture_Mask)
      glActiveTexture(GL_TEXTURE2);
    else
      glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, GLID);
    if(((Flags & A3Texture_NoFilter) == A3Texture_NoFilter))
    {
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    }
    else
    {
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 4);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, Settings::AFLevel);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
      glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }
    if(Data == NULL || sizeof(Data) < 3)
      return;
    switch(Channels)
    {
    case 3:
      glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,Size.x,Size.y,0,GL_RGB,GL_UNSIGNED_BYTE,Data);
      break;
    case 4:
      glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,Size.x,Size.y,0,GL_RGBA,GL_UNSIGNED_BYTE,Data);
      break;
    default:
      State = ST_DISABLED;
      return;
    }
    if(!(Flags & A3Texture_Animated))
    {
      delete []Data;
      Data = NULL;
    }
    if(!(Flags & A3Texture_NoFilter) && !(Flags & A3Texture_FBO) )
      glGenerateMipmap(GL_TEXTURE_2D);
    State = ST_ENABLED;
    return;
  }
  }
}

void A3Texture::Resize()
{
  glm::ivec2 Target(NextPow2(Size.x), NextPow2(Size.y));
  if(Target == Size || Data == NULL)
    return;
  char *Tmp = new char[(int)(Target.x * Target.y) * Channels];
  unsigned int *TP = reinterpret_cast<unsigned int*> (Tmp);
  unsigned int *DP = reinterpret_cast<unsigned int*> (Data);
  unsigned int TargetPixel = 0;
  unsigned int NearestPixel = 0;
  glm::vec2 Scale(1.0/((double)Target.x/Size.x), 1.0/((double)Target.y/Size.y));
  if(Channels == 4)
  {
    for(unsigned int y = 0; y < Target.y; y++)
    {
      for(unsigned int x = 0; x < Target.x; x++)
      {
        NearestPixel = (((int)(y*Scale.y) * Size.x) + (int)(x*Scale.x));
        TP[TargetPixel++] = DP[NearestPixel];
      }
    }
  }
  else
  {
    for(int y = 0; y < Target.y; y++)
    {
      for(int x = 0; x < Target.x; x++)
      {
        NearestPixel = (((int)(y*Scale.y) * (Size.x * Channels)) + ((int)(x*Scale.x) * Channels));
        Tmp[TargetPixel++] = Data[NearestPixel + 0];
        Tmp[TargetPixel++] = Data[NearestPixel + 1];
        Tmp[TargetPixel++] = Data[NearestPixel + 2];
      }
    }
  }

  delete []Data;
  Data = Tmp;
  Size = Target;
}

unsigned int A3Texture::GetID()
{
  return GLID;
}

A3Texture::~A3Texture()
{
  if(Data)
  {
    delete[] Data;
    Data = NULL;
  }
}

A3Texture *A3Texture::Create(const string &FileName, unsigned short Flags)
{
  for(list<A3Texture*>::iterator i = Cache.begin(); i != Cache.end(); ++i)
  {
    if((*i)->Name==FileName)
    {
      (*i)->References++;
      return (*i);
    }
  }
  A3Texture *T = new A3Texture(Flags);
  if(!T->CreateFromFile(FileName))
  {
    delete T;
    T = NULL;
  }
  else
  {
    Cache.push_back(T);
  }
  return T;
}

A3Texture *A3Texture::Create(const glm::ivec2 &Size, unsigned short Flags)
{
  A3Texture *T = new A3Texture(Flags);
  T->CreateFromSize(Size);
  Cache.push_back(T);
  return T;
}

A3Texture* A3Texture::Create(const unsigned char Red, const unsigned char Green, const unsigned char Blue, const unsigned char Alpha, const unsigned short Flags)
{
  stringstream name;
  name << "COLOR_" << (unsigned int)Red << "," << (unsigned int)Green << "," << (unsigned int)Blue << "," << (unsigned int)Alpha << "::";
  for(list<A3Texture*>::iterator i = Cache.begin(); i != Cache.end(); ++i)
  {
    if((*i)->Name==name.str())
    {
      (*i)->References++;
      return (*i);
    }
  }
  unsigned int IFlags = Flags | A3Texture_Static|A3Texture_NoFilter;
  if(Alpha != 255)
    IFlags |= A3Texture_Transparent;

  A3Texture *T = new A3Texture(IFlags);
  T->CreateFromColor(Red,Green,Blue,Alpha);
  Cache.push_back(T);
  return T;
}

A3Texture *A3Texture::SQCreate(const string &FileName, unsigned short Flags)
{
  return A3Texture::Create(FileName,Flags);
}

void A3Texture::Destroy(A3Texture *T)
{
  if(--T->References == 0)
  {
    Cache.remove(T);
    delete T;
    T = NULL;
  }
}





