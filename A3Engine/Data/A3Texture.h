#ifndef TEXTURE_H
#define TEXTURE_H

#include "A3Engine/Data/A3Data.h"

enum A3TextureFlags
{
  A3Texture_Static      = 0,
  A3Texture_Animated    = 1,
  A3Texture_Transparent = 2,
  A3Texture_Mask        = 4,
  A3Texture_NoFilter    = 8,
  A3Texture_FBO         = 64
};

class A3Texture: public A3Data
{
public:

  glm::ivec2      Size      = glm::ivec2(0.0);
  unsigned int    Channels  = 0;
  unsigned short  Flags     = A3Texture_Static;
  char            *Data     = NULL;
  unsigned int    DataSize  = 0;
  unsigned int    GLID      = 0;


  A3Texture     (unsigned short PFlags = A3Texture_Static);
  void          Render(float Delta);
  void          Resize();
  unsigned int  GetID();
  virtual       ~A3Texture();

  static        A3Texture        *Error;
  static        A3Texture        *ErrorMask;
  static        A3Texture* Create(const string &FileName, unsigned short Flags = A3Texture_Static);
  static        A3Texture* Create(const glm::ivec2 &Size, unsigned short Flags = A3Texture_Static);
  static        A3Texture* Create(const unsigned char Red, const unsigned char Green, const unsigned char Blue, const unsigned char Alpha = 255, const unsigned short Flags = A3Texture_Static);
  static        A3Texture* SQCreate(const string &FileName, unsigned short Flags = A3Texture_Static);
  static        void Destroy(A3Texture *T);

private:
  unsigned int            TextureUnit = 0;


  static unsigned int     Last;
  static list<A3Texture*> Cache;

  bool    CreateFromFile  (const string &PFile);
  bool    CreateFromColor (const unsigned char Red, const unsigned char Green, const unsigned char Blue, const unsigned char Alpha = 255);
  bool    CreateFromSize  (const glm::ivec2 &PSize);
};

#endif // TEXTURE_H
