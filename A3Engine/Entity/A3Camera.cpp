#include "A3Camera.h"

A3Camera::A3Camera()
  :A3Entity::A3Entity()
{
  Type = ENT_CAMERA;
  CulledObjects = 0;
  FrameDelta = 0.25f;
  FrameCount = 32766;
  TimeElapsed = 0.0f;
  FPS = 0.0f;
  FOV = -75.0f;
  Aspect = 1.0f;
  Near = 0.001f;
  Far = 200.0f;
  ProjectionType = A3Projection_None;
  Timer.Init();
  TimeFirst = TimeLast = Timer.GetTime();
  cout << "New Camera created" << endl;
  cout << "Name : " << Name << endl;
}
void A3Camera::Initalize(A3Projection Type, int X, int Y, float NearPlane, float FarPlane)
{
  ProjectionType  = Type;
  Near            = NearPlane;
  Far             = FarPlane;
  Resize(X,Y);
}

void A3Camera::Resize(int X, int Y)
{
  Size.x  = X;
  Size.y  = Y;
  Aspect  = ((float)X/(float)Y);
  switch(ProjectionType)
  {
  case A3Projection_Perspective:
    Projection = glm::perspective(FOV,Aspect,Near,Far);
    break;
  case A3Projection_Isometric:
    Projection = glm::ortho(0.0f,(float)X,(float)Y,0.0f,Near,Far);
    break;
  case A3Projection_None:
    cout << "ERROR:\tCamera projection mode not set, resize Vector2i(" << X << "," << Y << ") was ignored. Name: "  << Name  << endl;
    break;
  default:
    cout << "ERROR:\tProjection Type of " << ProjectionType << " is invalid. Name: "  << Name  << endl;
    break;
  }
}

void A3Camera::Render(float Delta)
{
  CulledObjects = 0;
  MRotate = glm::rotate(glm::rotate(glm::mat4(1.0), Rotation.x, glm::vec3(1,0,0)), Rotation.y, glm::vec3(0,1,0));
  ModelViewProjection =  Projection * glm::translate(MRotate, Position);
  //CalculateFrustum();
  float FPDiff = ((FPS*0.5)+1);
  if(++FrameCount >= FPDiff)
  {
    double TimeCurrent = Timer.GetTime();
    FrameDelta = (TimeCurrent - TimeLast)/FPDiff;
    TimeElapsed = (TimeCurrent - TimeFirst);
    FPS = 1/FrameDelta;
    cout << "FPS: " << FPS << " (" << FrameDelta << " S/F)" << endl;
    TimeLast =  TimeCurrent;
    FrameCount = 0;
  }


}

bool A3Camera::Cull(A3Entity *E)
{
  /*float D;
  for(int p = 0; p < 6; p++ )
  {
      D = Frustum[p][0] * E->Position.X + Frustum[p][1] * E->Position.Y + Frustum[p][2] * E->Position.Z + Frustum[p][3];
      if(  D <= -E->Radius )
      {
          CulledObjects++;
          return true;
      }
  }
  E->Distance = D+E->Radius;
  return false;*/
}
void A3Camera::CalculateFrustum()
{
//    float   t;
//
//    /* Extract the numbers for the RIGHT plane */
//    Frustum[0][0] = ModelViewProjection[ 3] - ModelViewProjection[ 0];
//    Frustum[0][1] = ModelViewProjection[ 7] - ModelViewProjection[ 4];
//    Frustum[0][2] = ModelViewProjection[11] - ModelViewProjection[ 8];
//    Frustum[0][3] = ModelViewProjection[15] - ModelViewProjection[12];
//
//    /* Normalize the result */
//    t = sqrt( Frustum[0][0] * Frustum[0][0] + Frustum[0][1] * Frustum[0][1] + Frustum[0][2] * Frustum[0][2] );
//    Frustum[0][0] /= t;
//    Frustum[0][1] /= t;
//    Frustum[0][2] /= t;
//    Frustum[0][3] /= t;
//
//    /* Extract the numbers for the LEFT plane */
//    Frustum[1][0] = ModelViewProjection[ 3] + ModelViewProjection[ 0];
//    Frustum[1][1] = ModelViewProjection[ 7] + ModelViewProjection[ 4];
//    Frustum[1][2] = ModelViewProjection[11] + ModelViewProjection[ 8];
//    Frustum[1][3] = ModelViewProjection[15] + ModelViewProjection[12];
//
//    /* Normalize the result */
//    t = sqrt( Frustum[1][0] * Frustum[1][0] + Frustum[1][1] * Frustum[1][1] + Frustum[1][2] * Frustum[1][2] );
//    Frustum[1][0] /= t;
//    Frustum[1][1] /= t;
//    Frustum[1][2] /= t;
//    Frustum[1][3] /= t;
//
//    /* Extract the BOTTOM plane */
//    Frustum[2][0] = ModelViewProjection[ 3] + ModelViewProjection[ 1];
//    Frustum[2][1] = ModelViewProjection[ 7] + ModelViewProjection[ 5];
//    Frustum[2][2] = ModelViewProjection[11] + ModelViewProjection[ 9];
//    Frustum[2][3] = ModelViewProjection[15] + ModelViewProjection[13];
//
//    /* Normalize the result */
//    t = sqrt( Frustum[2][0] * Frustum[2][0] + Frustum[2][1] * Frustum[2][1] + Frustum[2][2] * Frustum[2][2] );
//    Frustum[2][0] /= t;
//    Frustum[2][1] /= t;
//    Frustum[2][2] /= t;
//    Frustum[2][3] /= t;
//
//    /* Extract the TOP plane */
//    Frustum[3][0] = ModelViewProjection[ 3] - ModelViewProjection[ 1];
//    Frustum[3][1] = ModelViewProjection[ 7] - ModelViewProjection[ 5];
//    Frustum[3][2] = ModelViewProjection[11] - ModelViewProjection[ 9];
//    Frustum[3][3] = ModelViewProjection[15] - ModelViewProjection[13];
//
//    /* Normalize the result */
//    t = sqrt( Frustum[3][0] * Frustum[3][0] + Frustum[3][1] * Frustum[3][1] + Frustum[3][2] * Frustum[3][2] );
//    Frustum[3][0] /= t;
//    Frustum[3][1] /= t;
//    Frustum[3][2] /= t;
//    Frustum[3][3] /= t;
//
//    /* Extract the FAR plane */
//    Frustum[4][0] = ModelViewProjection[ 3] - ModelViewProjection[ 2];
//    Frustum[4][1] = ModelViewProjection[ 7] - ModelViewProjection[ 6];
//    Frustum[4][2] = ModelViewProjection[11] - ModelViewProjection[10];
//    Frustum[4][3] = ModelViewProjection[15] - ModelViewProjection[14];
//
//    /* Normalize the result */
//    t = sqrt( Frustum[4][0] * Frustum[4][0] + Frustum[4][1] * Frustum[4][1] + Frustum[4][2] * Frustum[4][2] );
//    Frustum[4][0] /= t;
//    Frustum[4][1] /= t;
//    Frustum[4][2] /= t;
//    Frustum[4][3] /= t;
//
//    /* Extract the NEAR plane */
//    Frustum[5][0] = ModelViewProjection[ 3] + ModelViewProjection[ 2];
//    Frustum[5][1] = ModelViewProjection[ 7] + ModelViewProjection[ 6];
//    Frustum[5][2] = ModelViewProjection[11] + ModelViewProjection[10];
//    Frustum[5][3] = ModelViewProjection[15] + ModelViewProjection[14];
//
//    /* Normalize the result */
//    t = sqrt( Frustum[5][0] * Frustum[5][0] + Frustum[5][1] * Frustum[5][1] + Frustum[5][2] * Frustum[5][2] );
//    Frustum[5][0] /= t;
//    Frustum[5][1] /= t;
//    Frustum[5][2] /= t;
//    Frustum[5][3] /= t;
}

A3Camera::~A3Camera()
{
  //dtor
}
