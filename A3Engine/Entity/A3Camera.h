#ifndef A3CAMERA_H
#define A3CAMERA_H

#include "A3Engine/Entity/A3Entity.h"
#include "A3Engine/Structure/A3Timer.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/quaternion.hpp>

enum A3Projection:
char
{
  A3Projection_None = 0,
  A3Projection_Perspective = 1,
  A3Projection_Isometric = 2
};

class A3Camera: public A3Entity
{
public:
  glm::vec3       Position;
  glm::vec2       Size;
  glm::vec2       Rotation;
  A3Projection    ProjectionType;
  double          FrameDelta;
  glm::mat4       Projection;
  glm::mat4       ModelView;
  glm::mat4       ModelViewProjection;
  float           FPS;
  float           TimeElapsed;
  float           FOV;
  float           Aspect;
  float           Near;
  float           Far;
  unsigned int    CulledObjects;
private:
  float           Frustum[6][4];
  short           FrameCount;
  double          TimeLast;
  double          TimeFirst;
  CTimer          Timer;
public:
  A3Camera();
  void          Initalize(A3Projection Type, int X, int Y, float Near = 0.001, float Far = 200);
  void          Resize(int SizeX, int SizeY);
  virtual void  Render(float Delta);
  bool          Cull (A3Entity *E);
  ~A3Camera();
protected:
private:
  void CalculateFrustum();
  glm::mat4 MRotate;
  glm::mat4 MTranslate;
};

#endif // A3CAMERA_H
