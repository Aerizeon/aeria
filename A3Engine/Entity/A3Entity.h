#ifndef ENTITY_H
#define ENTITY_H

#include "A3Include.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/quaternion.hpp>
#include "A3Engine/Data/A3Texture.h"
enum Ent_Type:
char
{
  ENT_INVALID,
  ENT_MODEL,
  ENT_CAMERA,
  ENT_TERRAIN,
  ENT_EMITTER,
  ENT_LABEL
};


class A3Entity
{
public:
  unsigned int    ID        = 0;
  string          Name      = "";
  bool            HasAlpha  = false;
  unsigned short  State     = ST_NULL;
  glm::vec3       Position  = glm::vec3();     ///Current Position
  glm::vec3       Target    = glm::vec3();     ///Target Position
  glm::vec3       Scale     = glm::vec3();      ///Current Scale
  glm::vec4       Color     = glm::vec4(1.0);
  glm::quat       Rotation  = glm::quat();
  A3Texture       *Texture  = NULL;
  A3Texture       *Mask     = NULL;
  Ent_Type        Type      = ENT_INVALID;      ///Object Type Identifier (For Serialization)
  float           Radius    = 0.0f;             ///TEMP: Radius for bounding sphere
  float           Distance  = 0.0f;             ///Distance from camera (for sorting)

  A3Entity();
  virtual void Render(float Delta) = 0;
  virtual ~A3Entity();
protected:
private:
};

#endif // ENTITY_H
