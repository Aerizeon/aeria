#include "A3Label.h"
#include "A3Engine/A3UI.h"

A3Label::A3Label()
  :A3Panel()
{
}

A3Label::A3Label(string LabelText, A3Font *LabelFont)
  :A3Panel()
{
  Name = LabelText;
  Font = LabelFont;
  SetText(s2ws(LabelText));
}

A3Label::A3Label(wstring LabelText, A3Font *LabelFont)
  :A3Panel()
{
  Name = ws2s(LabelText);
  Font = LabelFont;
  SetText(LabelText);
}

void A3Label::SetText(wstring LabelText)
{
  Text = LabelText;
  TextLength = Text.length();
  if(TextLength == 0)
  {
    ClientRegion.Size.x = 0;
    RenderState = ST_DISABLED;
  }
  else
    RenderState = ST_LOADED;
}

void A3Label::Render(float Delta)
{
  switch(RenderState)
  {
  case ST_LOADED:
    Build();
    break;
  case ST_ENABLED:
    glBindVertexArray(VAO);
    break;
  default:
    return;
  }
  if(Attachment & A3Attachment_Center)
    AttachOffset = (Parent->GetClientSize()*0.5f)-GetClientSize()*0.5f;
  if(Attachment & A3Attachment_Right)
    AttachOffset.x = Parent->GetClientSize().x;
  else if(Attachment & A3Attachment_Left)
    AttachOffset.x = 0;
  if(Attachment & A3Attachment_Top)
    AttachOffset.y = 0;
  else if(Attachment & A3Attachment_Bottom)
    AttachOffset.y = Parent->GetClientSize().y;
  glm::vec2 AbsPos = PanelRegion.Position + AttachOffset + Parent->ClientToScreen();
  glm::mat4 Out = glm::translate(A3UI::CurrentCamera.Projection, glm::vec3(floor(AbsPos.x),floor(AbsPos.y), -1.0));
  glUniformMatrix4fv(A3UI::TextShader->ObjectCombinedMatrix, 1,false, glm::value_ptr(Out));
  //glUniform4f(A3UI::TextShader->ObjectColor,(0/255.0),(0/255.0),(0/255.0),1.0);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, Font->GetTexture());
  glDrawElements(GL_TRIANGLES, TextLength*6 ,GL_UNSIGNED_INT,0);
}

void A3Label::Build()
{
  RenderState = ST_LOADING;

  if(TextLength > TextBuffer)
  {
    TextBuffer = NextPow2(TextLength + 1);
    IndexCount = TextBuffer * 6;
    VertexCount = TextBuffer * 4;
    ///We need to resize the vertex buffers
    if(VAO != 0)
    {
      glBindVertexArray(VAO);
      glDeleteBuffers(2,VBO);
      glDeleteVertexArrays(1,&VAO);
    }
    if(Vertices != NULL)
      delete[] Vertices;
    if(Indices != NULL)
      delete[] Indices;

    Vertices = new A3Vertex[VertexCount];
    Indices = new unsigned int[IndexCount];
    glGenVertexArrays(1,&VAO);
    glBindVertexArray(VAO);
    glGenBuffers(2,VBO);
  }
  else
  {
    glBindVertexArray(VAO);
  }
  vector<A3Glyph> GlStr = Font->GetGlyphs(Text);
  glm::vec2 Offset;
  float mx = 0;
  float my = 0;
  for(unsigned int i = 0; i < TextLength; i++)
  {
    A3Glyph G = GlStr[i];

    Indices[i*6+0] = i*4+0;
    Indices[i*6+1] = i*4+1;
    Indices[i*6+2] = i*4+2;

    Indices[i*6+3] = i*4+1;
    Indices[i*6+4] = i*4+3;
    Indices[i*6+5] = i*4+2;

    //BOTTOM LEFT//
    Vertices[i*4+0] = A3Vertex();
    Vertices[i*4+0].X = round(Offset.x + G.Position.x);
    Vertices[i*4+0].Y = round(Offset.y + G.Position.y + G.Size.y);
    Vertices[i*4+0].Z = 0;
    Vertices[i*4+0].U = G.TexCoord.x;
    Vertices[i*4+0].V = G.TexCoord.y + (G.Size.y / ((float)Font->TextureHeight));


    //BOTTOM RIGHT//
    Vertices[i*4+1] = A3Vertex();
    Vertices[i*4+1].X = round(Offset.x + G.Position.x + G.Size.x);
    Vertices[i*4+1].Y = round(Offset.y + G.Position.y + G.Size.y);
    Vertices[i*4+1].Z = 0;
    Vertices[i*4+1].U = G.TexCoord.x + (G.Size.x / ((float)Font->TextureWidth));
    Vertices[i*4+1].V = G.TexCoord.y + (G.Size.y / ((float)Font->TextureHeight));

    //TOP LEFT//
    Vertices[i*4+2] = A3Vertex();
    Vertices[i*4+2].X = round(Offset.x + G.Position.x);
    Vertices[i*4+2].Y = round(Offset.y + G.Position.y);
    Vertices[i*4+2].Z = 0;
    Vertices[i*4+2].U = G.TexCoord.x;
    Vertices[i*4+2].V = G.TexCoord.y;

    //TOP RIGHT//
    Vertices[i*4+3] = A3Vertex();
    Vertices[i*4+3].X = round(Offset.x + G.Position.x + G.Size.x);
    Vertices[i*4+3].Y = round(Offset.y + G.Position.y);
    Vertices[i*4+3].Z = 0;
    Vertices[i*4+3].U = G.TexCoord.x + (G.Size.x / ((float)Font->TextureWidth));
    Vertices[i*4+3].V = G.TexCoord.y;
    Offset.x += G.Advance.x;
    Offset.y += G.Advance.y;
    mx += G.Advance.x;
    my = std::max(my,Offset.y + G.Position.y + G.Size.y);

  }
  ClientRegion = A3Region2f(glm::vec2(), glm::vec2(mx+3,my));
  glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
  glBufferData(GL_ARRAY_BUFFER, sizeof(A3Vertex)*VertexCount, Vertices, GL_STATIC_DRAW);
  glEnableVertexAttribArray(A3UI::TextShader->TexCoordPointer);
  glEnableVertexAttribArray(A3UI::TextShader->VertexPointer);
  glVertexAttribPointer(A3UI::TextShader->VertexPointer, 3, GL_FLOAT, GL_FALSE, sizeof(A3Vertex), offsetof(A3Vertex,X));
  glVertexAttribPointer(A3UI::TextShader->TexCoordPointer, 2, GL_FLOAT, GL_FALSE, sizeof(A3Vertex), (void*)offsetof(A3Vertex,U));
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO[1]);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(unsigned int)*IndexCount, Indices, GL_STATIC_DRAW);
  RenderState = ST_ENABLED;
  return;
}
A3Label::~A3Label()
{
  return;
}
