#ifndef A3LABEL_H
#define A3LABEL_H

#include <string>

#include "A3Engine/Entity/A3Panel.h"
#include "A3Engine/Data/A3Font.h"

class A3Label: public A3Panel
{
public:
  A3Label();
  A3Label(A3Label &Label);
  A3Label(string LabelText, A3Font *LabelFont = DefaultFont);
  A3Label(wstring LabelText, A3Font *LabelFont = DefaultFont);
  void SetText(wstring LabelText);
  virtual void Render(float Delta);
  void Build();
  ~A3Label();
protected:
private:
  wstring       Text        = L"";
  unsigned int  TextLength  = 0;
  unsigned int  TextBuffer  = 0;
  A3Font        *Font       = NULL;
  static A3Font *DefaultFont;
};
#endif // A3LABEL_H
