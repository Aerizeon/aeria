#include "A3Light.h"
#include "A3Engine/A3World.h"

A3Light::A3Light()
{
  FBOTexture = A3Texture::Create(glm::vec2(1024,1024), (A3Texture_NoFilter | A3Texture_FBO));
  /*glGenFramebuffers(1, &FBO);
  glBindFramebuffer(GL_FRAMEBUFFER, FBO);

  glDrawBuffer(GL_NONE);
  glReadBuffer(GL_NONE);

  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, FBOTexture->GLID, 0);

  GLenum RC = glCheckFramebufferStatus(GL_FRAMEBUFFER);
  if(RC != GL_FRAMEBUFFER_COMPLETE)
    cout << "FBO failed: " << RC << endl;
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  //Projection.CreatePerspective(80, 1.0, 0.001, 400.0);
  glm::mat4 Proj = glm::ortho(0.0f, 1024.0f, 1024.0f ,0.0f, 0.01f, 1.0f);
  Projection = glm::scale(Proj,glm::vec3(100,100,1));*/
}

void A3Light::Render_Start()
{
  /*glBindFramebuffer(GL_FRAMEBUFFER, FBO);

  glUseProgram(A3World::ShaderShadow->ID);
  glViewport(0, 0, 1024,1024);
  glClear( GL_DEPTH_BUFFER_BIT);
  glCullFace(GL_FRONT);*/
}

void A3Light::Render(float Delta)
{

}

void A3Light::Render_End()
{
  /*glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D,FBOTexture->GLID);
  glGenerateMipmap(GL_TEXTURE_2D);
  glActiveTexture(GL_TEXTURE0);
  glBindFramebuffer(GL_FRAMEBUFFER,0);

  glm::vec2 Size = A3World::CurrentCamera.Size;
  glViewport(0,0,Size.x,Size.y);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glUseProgram(A3World::ShaderLighting->ID);
  ModelViewProjectionBias = ModelViewProjection * glm::mat4(0.5,  0,  0,  0,
                            0,  0.5,  0,  0,
                            0,  0,  0.5,  0,
                            0.5,0.5,0.5,1.0);
  glCullFace(GL_BACK);*/

}

A3Light::~A3Light()
{
  //dtor
}
