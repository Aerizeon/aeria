#ifndef A3LIGHT_H
#define A3LIGHT_H

#include "A3Include.h"
#include "A3Entity.h"
#include "A3Program.h"
#include "A3Engine/Data/A3Texture.h"


class A3Light: public A3Entity
{
public:
  glm::mat4			Projection;
  glm::mat4     ModelView;
  glm::mat4     ModelViewProjection;
  glm::mat4     ModelViewProjectionBias;
  A3Texture     *FBOTexture = NULL;
  unsigned int  FBO         = 0;

  A3Light();
  void Render_Start();
  void Render(float Delta);
  void Render_End();
  ~A3Light();
protected:
private:
  glm::mat4 MRotate;
  glm::mat4 MTranslate;
  glm::mat4 MTemp;
  glm::mat4 MBias;
};

#endif // A3LIGHT_H
