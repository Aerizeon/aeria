#include "A3Model.h"

#include "A3Engine/A3World.h"

A3Model::A3Model()
  :A3Entity()
{
  Type = ENT_MODEL;
}

void A3Model::Create()
{
  Mesh = A3Mesh::Create("cube.obj");
  Scale = glm::vec3(0.125f,0.125f,0.125f);
  State = ST_ENABLED;
  return;
}

void A3Model::Create(string Name, glm::vec3 Pos)
{
  Mesh = A3Mesh::Create(Name);
  HasAlpha = Mesh->HasAlpha;
  Position = Pos;
  Scale = glm::vec3(1.0,1.0,1.0);
  State = ST_ENABLED;
  return;
}

void A3Model::Render(float Delta)
{
  if(State != ST_ENABLED) return;
  if(!GMSet)
  {
    GMScale = glm::scale(glm::mat4_cast(Rotation), Scale);
    GMTranslate = glm::translate(glm::mat4(1.0), Position);
    GMPreCombine = GMTranslate * GMScale * GMShear;
    GMSet = true;
  }
  glUniformMatrix4fv(A3World::ShaderLighting->ObjectSeparateMatrix, 1,false, glm::value_ptr(GMPreCombine));
  glUniformMatrix4fv(A3World::ShaderLighting->ObjectCombinedMatrix, 1,false, glm::value_ptr(A3World::CurrentCamera.ModelViewProjection * GMPreCombine));
  if(Texture != NULL)
    Texture->Render(Delta);
  if(Mask != NULL)
    Mask->Render(Delta);
  else
    A3Texture::ErrorMask->Render(Delta);
  Mesh->Render(Delta, Texture != NULL);
}

A3Model::~A3Model()
{
  if(Mesh)
  {
    A3Mesh::Destroy(Mesh);
  }
}
