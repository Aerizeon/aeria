#ifndef MODEL_H
#define MODEL_H

#include "A3Entity.h"
#include "A3Engine/Data/A3Mesh.h"

class A3Model: public A3Entity
{
public:
  A3Mesh          *Mesh     = NULL;
  glm::mat4       GMShear;
  A3Model();
  void Create();
  void Create(string Name, glm::vec3 Pos);
  void Render(float Delta);
  ~A3Model();
protected:
private:
  bool       GMSet    = false;
  glm::mat4  GMRotate;
  glm::mat4  GMTranslate;
  glm::mat4  GMScale;
  glm::mat4  GMPostTranslate;
  glm::mat4  GMPreCombine;
  glm::mat4  GMCombined;
};

#endif // MODEL_H
