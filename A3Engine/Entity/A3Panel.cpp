#include "A3Panel.h"
#include "A3Engine/A3UI.h"

A3Panel::A3Panel()
{
  //Inheritance constructor, does nothing
  ifstream Themef("./cache/themes/default.a3t", ifstream::in|ifstream::binary);
  Theme.ParseFromIstream(&Themef);
  Themef.close();
}

A3Panel::A3Panel(const A3Panel &pan)
{
  cout << "TODO: Copy Construct for A3Panel" << endl;
}

A3Panel::A3Panel(const string PName, const glm::vec2 PPosition, const glm::vec2 PSize, unsigned int PStyle)
  :PanelRegion(PPosition,PSize),ClientRegion((PStyle == A3Style_Default)?glm::vec2(10.0f, 24.0f):glm::vec2(),(PSize - ((PStyle == A3Style_Default)?glm::vec2(20.0f, 34.0f):glm::vec2())))
{
  Name            = PName;
  Vertices        = new A3Vertex[36];
  Indices         = new unsigned int[54];
  RenderState     = ST_LOADED;
  Style           = PStyle;
  ifstream Themef("./cache/themes/default.a3t", ifstream::in|ifstream::binary);
  Theme.ParseFromIstream(&Themef);
  Themef.close();
}
void A3Panel::SetPosition(const glm::vec2 PPosition)
{
  PanelRegion.Position = PPosition;
}

void A3Panel::SetSize(const glm::vec2 PSize)
{
  glm::vec2 Diff = PanelRegion.Size - ClientRegion.Size;
  PanelRegion.Size = PSize;
  if(PanelRegion.Size.x < MinSize.x)
    PanelRegion.Size.x = MinSize.x;
  if(PanelRegion.Size.y < MinSize.y)
    PanelRegion.Size.y = MinSize.y;
  ClientRegion.Size = (PanelRegion.Size - Diff);
  RenderState = ST_UPDATED;
}


void A3Panel::SetTexture(A3Texture *PTexture)
{
  Texture = PTexture;
}

glm::vec2 A3Panel::GetPanelPosition()
{
  return PanelRegion.Position;
}

glm::vec2 A3Panel::GetPanelSize()
{
  return PanelRegion.Size;
}

glm::vec2 A3Panel::GetClientSize()
{
  return ClientRegion.Size - ClientRegion.Position;
}

glm::vec2 A3Panel::PanelToScreen()
{
  if(Parent)
    return Parent->ClientToScreen() + AttachOffset + PanelRegion.Position;
  else
    return PanelRegion.Position + AttachOffset;
}

glm::vec2 A3Panel::PanelToScreen(glm::vec2 In)
{
  if(Parent)
    return Parent->ClientToScreen() + AttachOffset + PanelRegion.Position +In;
  else
    return PanelRegion.Position + AttachOffset + In;
}

glm::vec2 A3Panel::ClientToScreen()
{
  if(Parent)
    return Parent->ClientToScreen() + PanelRegion.Position + AttachOffset + ClientRegion.Position;
  else
    return PanelRegion.Position + AttachOffset + ClientRegion.Position;
}

glm::vec2 A3Panel::ClientToScreen(glm::vec2 In)
{
  if(Parent)
    return Parent->ClientToScreen() + PanelRegion.Position + AttachOffset + ClientRegion.Position + In;
  else
    return PanelRegion.Position + AttachOffset + ClientRegion.Position + In;
}

A3Texture* A3Panel::GetTexture()
{
  return Texture;
}

void A3Panel::Focus()
{
  if(!Parent)
    return;
  (*Parent->Children.begin())->State = UI_INACTIVE;
// if((*Parent->Children.begin())->Children.size() > 0)
  //  (*(*Parent->Children.begin())->Children.begin())->State = UI_INACTIVE;

  Parent->Children.remove(this);
  this->State = UI_ACTIVE;
  (*this->Children.begin())->State = UI_ACTIVE;
  Parent->Children.push_front(this);

  cout << "Focused: " << this->State << endl;
}

bool A3Panel::HasFocus()
{
  if(!Parent)
    return false;
  if((*Parent->Children.begin()) == this)
    return true;
  return false;
}
bool A3Panel::IntMouseEvent(A3MouseState Mouse)
{
  list<A3Panel *>::iterator Current = IntChildren.begin();
  list<A3Panel *>::iterator End = IntChildren.end();
  while(*Current && Current != End)
  {
    A3Region2f CPanelRegion = (*Current)->PanelRegion;
    CPanelRegion.Position += (*Current)->AttachOffset;
    if(CPanelRegion.Test(Mouse.Position) && (*Current)->State == UI_ACTIVE)
    {
      A3MouseState Sender = Mouse;
      Sender.Position = Sender.Position - (*Current)->PanelToScreen(glm::vec2());
      (*Current)->IntMouseEvent(Sender);
      return true;
    }
    Current++;
  }
  if(!Mouse.Buttons[0] && LastMouseEvent.Buttons[0])
  {
    MouseLeftUp.emit();
    MouseLeftClick.emit();
  }
  LastMouseEvent = Mouse;
  return false;
}
bool A3Panel::IntKeyEvent(A3KeyState State)
{
  return false;
}

bool A3Panel::KeyEvent(A3KeyState State)
{
  list<A3Panel *>::iterator Current = Children.begin();
  list<A3Panel *>::iterator End = Children.end();
  while(*Current && Current != End)
  {
    A3Panel *CPanel = (*Current);
    if(CPanel->HasFocus())
    {
      if(CPanel->IntKeyEvent(State))
        return true;
      else
      {
        if(CPanel->KeyEvent(State))
        {
          return true;
        }
      }
    }
  }
}

bool A3Panel::MouseEvent(A3MouseState Mouse)
{
  if(!Mouse.Buttons[0] && LastMouseEvent.Buttons[0])
  {
    MouseLeftClick.emit();
    MouseLeftUp.emit();
  }
  else if(Mouse.Buttons[0] && !LastMouseEvent.Buttons[0])
  {
    MouseLeftDown.emit();
  }
  LastMouseEvent = Mouse;
  list<A3Panel *>::iterator Current = Children.begin();
  list<A3Panel *>::iterator End = Children.end();
  while(*Current && Current != End)
  {
    A3Panel *CPanel = (*Current);
    A3Region2f CPanelRegion = CPanel->PanelRegion;
    CPanelRegion.Position += CPanel->AttachOffset;
    A3Region2f CClientRegion = CPanel->ClientRegion;
    unsigned int CState = CPanel->State;
    if(CPanelRegion.Test(Mouse.Position)||CState == UI_MOVING||CState == UI_RESIZING)
    {
      A3MouseState Sender = Mouse;
      Sender.Position = Sender.Position - CPanelRegion.Position - CClientRegion.Position;
      if(CPanel->IntMouseEvent(Sender))
        return true;
      glm::vec2 CMousePosition = Mouse.Position - CPanelRegion.Position; ///Move mouse position to current window context
      if(Mouse.Buttons[0] && (*Current)->State == UI_INACTIVE)
      {
        CPanel->Focus();
      }

      if(CPanel->HasFocus())
      {
        if(CClientRegion.Test(CMousePosition) && CState == UI_ACTIVE)
        {
          ///Mouse is within client area; forward mouse input (with client offset)
          A3MouseState Sender = Mouse;
          Sender.Position = Sender.Position - CPanelRegion.Position - CClientRegion.Position;//CPanel->ClientToScreen();
          CPanel->MouseEvent(Sender);
        }
        else if(Mouse.Buttons[0] && (CMousePosition.y < CClientRegion.Position.y|| CState == UI_MOVING) && CState != UI_RESIZING)
        {
          ///Active window is being moved by title bar
          if(CState != UI_MOVING)
          {
            MouseOffset = Mouse.Position - (CPanelRegion.Position-CPanel->AttachOffset); //Mouse position relative to edge of panel
            CPanel->LastState = CPanel->State;  //Last UI state (for restoration on reset)
            CPanel->State = UI_MOVING;
          }
          glm::vec2 PositionNext = Mouse.Position - MouseOffset; //Calculates the window position based on the panel offset and new mouse

          if(PositionNext.x + CPanel->GetPanelSize().x + CPanel->AttachOffset.x  > GetClientSize().x)
            PositionNext.x = GetClientSize().x - CPanel->AttachOffset.x - CPanel->GetPanelSize().x;

          if(PositionNext.y + CPanel->GetPanelSize().y + CPanel->AttachOffset.y > GetClientSize().y)
            PositionNext.y = GetClientSize().y - CPanel->AttachOffset.y - CPanel->GetPanelSize().y;

          if(PositionNext.y + CPanel->GetPanelPosition().y + (2*CPanel->AttachOffset.y)  <= 0)
            PositionNext.y = -CPanel->AttachOffset.y;

          if(PositionNext.x + CPanel->GetPanelPosition().x + (2*CPanel->AttachOffset.x) <= 0)
            PositionNext.x = -CPanel->AttachOffset.x;



          CPanel->SetPosition(PositionNext);
          glm::vec2 ABPOS = ClientToScreen();
          POINT P = {0,0};
          ::ClientToScreen(Settings::Handle,&P);
          RECT R = {P.x + ABPOS.x + 1, P.y + ABPOS.y + 1, P.x + ABPOS.x + ClientRegion.Size.x, P.y + ABPOS.y + ClientRegion.Size.y};
          ClipCursor(&R);
        }
        else if(Mouse.Buttons[0] && ((CMousePosition.x > CClientRegion.Size.x && CMousePosition.y > CClientRegion.Size.y) || CState == UI_RESIZING))
        {
          //Active window is being resized (lower right)
          if(CState != UI_RESIZING)
          {
            MouseOffset =  (CPanelRegion.Position + CPanelRegion.Size) - Mouse.Position;
            CPanel->LastState = (A3UI_State)CState;
            CPanel->State = UI_RESIZING;
          }
          CPanel->SetSize(Mouse.Position - CPanelRegion.Position + MouseOffset);
        }
        else
        {
          if(CState == UI_MOVING || CState == UI_RESIZING)
            CPanel->State = CPanel->LastState;
          ClipCursor(NULL);
        }

      }
      return true;
    }
    Current++;
  }
  return false;
}

void A3Panel::AddChild(A3Panel *P)
{
  if(Children.size() > 0)
    (*Children.begin())->State = UI_INACTIVE;
  P->State = UI_ACTIVE;
  P->Parent = this;
  Children.push_front(P);
}

void A3Panel::AddIntChild(A3Panel *P)
{
  P->State = UI_ACTIVE;
  P->Parent = this;
  IntChildren.push_front(P);
}

void A3Panel::Render(float Delta)
{
  if(Style != A3Style_Renderless)
  {
    switch(RenderState)
    {
    case ST_ENABLED:
      glBindVertexArray(VAO);
      break;
    case ST_LOADED:
      Build();
      break;
    case ST_UPDATED:
      Update();
      break;
    default:
      return;
    }

    //A3Matrix Out;
    if(Attachment & A3Attachment_Center)
      AttachOffset = (Parent->GetClientSize()*0.5f)-GetPanelSize()*0.5f;
    if(Attachment & A3Attachment_Right)
      AttachOffset.x = Parent->GetClientSize().x;
    else if(Attachment & A3Attachment_Left)
      AttachOffset.x = 0;
    if(Attachment & A3Attachment_Top)
      AttachOffset.y = 0;
    else if(Attachment & A3Attachment_Bottom)
      AttachOffset.y = Parent->GetClientSize().y;
    glm::vec2 AbsPos = PanelRegion.Position + AttachOffset + Parent->ClientToScreen();
    glm::mat4 Out = glm::translate(A3UI::CurrentCamera.Projection, glm::vec3(floor(AbsPos.x),floor(AbsPos.y), -1.0));
    glUniformMatrix4fv(A3UI::TextShader->ObjectCombinedMatrix, 1,false, glm::value_ptr(Out));
    switch(State)
    {
    case UI_MOVING:
    case UI_RESIZING:
    case UI_ACTIVE:
      //glUniform4f(A3UI::TextShader->ObjectColor,(255.0/255.0),(255.0/255.0),(255.0/255.0),1.0);
      break;
    case UI_INACTIVE:
      //glUniform4f(A3UI::TextShader->ObjectColor,(180.0/255.0),(180.0/255.0),(180.0/255.0),0.8);
      break;
    }
    if(Texture)
      Texture->Render(Delta);
    glDrawElements(GL_TRIANGLES, (MonoPatch?6:54) ,GL_UNSIGNED_INT,0);

  }
  list<A3Panel*>::reverse_iterator ICurrent = IntChildren.rbegin();
  list<A3Panel*>::reverse_iterator IEnd = IntChildren.rend();

  while(*ICurrent && ICurrent != IEnd)
  {
    glEnable(GL_SCISSOR_TEST);
    int PX = PanelToScreen().x;
    int PY = A3UI::Root->ClientRegion.Size.y - std::min(Parent->ClientToScreen(Parent->ClientRegion.Size).y,PanelToScreen(PanelRegion.Size).y);
    int SX = std::min(Parent->ClientRegion.Size.x,PanelRegion.Size.x);
    int SY = std::min(Parent->ClientRegion.Size.y,PanelRegion.Size.y);

    glScissor(PX,PY,SX,SY);
    if((*ICurrent) != nullptr)
    {
      (*ICurrent)->Render(Delta);
    }
    glDisable(GL_SCISSOR_TEST);
    ICurrent++;
  }

  list<A3Panel*>::reverse_iterator Current = Children.rbegin();
  list<A3Panel*>::reverse_iterator End = Children.rend();
  Current = Children.rbegin();
  End = Children.rend();
  while(Current != End)
  {
    if(Parent)
    {
      glEnable(GL_SCISSOR_TEST);
      int PX = ClientToScreen().x;
      int PY = A3UI::Root->ClientRegion.Size.y - std::min(Parent->ClientToScreen(Parent->ClientRegion.Size).y,ClientToScreen(ClientRegion.Size).y);
      int SX = std::min(Parent->ClientToScreen(Parent->ClientRegion.Size).x,ClientToScreen(ClientRegion.Size).x) - PX;
      int SY = std::min(Parent->ClientRegion.Size.y,ClientRegion.Size.y);
      glScissor(PX,PY,SX,SY);
    }
    if((*Current) != nullptr)
    {
      (*Current)->Render(Delta);
    }
    glDisable(GL_SCISSOR_TEST);
    Current++;
  }
}

void A3Panel::DeleteChild(A3Panel *P)
{
  list<A3Panel*>::iterator Current = Children.begin();
  list<A3Panel*>::iterator End = Children.end();
  while(Current != End)
  {
    if((*Current) == P)
    {
      Current = Children.erase(Current);
    }
    else
      Current++;
  }
}

void A3Panel::Update()
{
  if(Style == A3Style_Renderless)
  {
    RenderState = ST_ENABLED;
    return;
  }
  RenderState = ST_ENABLED;
  glBindVertexArray(VAO);

  A3TClass ThemeClass;
  for(int i = 0; i < Theme.class__size(); i++)
  {
    if(Theme.class_(i).type() == ThemeClassType)
    {
      ThemeClass = Theme.class_(i);
      break;
    }
  }

  A3TElement ThemeElement;
  for(int i = 0; i < ThemeClass.element_size(); i++)
  {
    if(ThemeClass.element(i).type() == ThemeElementType)
    {
      ThemeElement = ThemeClass.element(i);
      break;
    }
  }

  A3TPatch AP[9];
  float SZX[9];
  float SZY[9];
  for(int i = 0; i < ThemeElement.patch_size(); i++)
  {
    AP[i] = ThemeElement.patch(i);
    SZX[i] = floor((AP[i].xend() - AP[i].xstart())*64.0);
    SZY[i] = floor((AP[i].yend() - AP[i].ystart())*64.0);
  }
  if(!MonoPatch)
  {
    //Top Left (0)
    Vertices[0] =   A3Vertex(0,                           0,                            0,  0,  0,  0,  AP[0].xstart(),   AP[0].ystart());  //Start X,  Start Y
    Vertices[1] =   A3Vertex(0,                           SZY[0],                       0,  0,  0,  0,  AP[0].xstart(),   AP[0].yend());    //Start X,  End Y
    Vertices[2] =   A3Vertex(SZX[0],                      SZY[0],                       0,  0,  0,  0,  AP[0].xend(),     AP[0].yend());    //End X,    End Y
    Vertices[3] =   A3Vertex(SZX[0],                      0,                            0,  0,  0,  0,  AP[0].xend(),     AP[0].ystart());  //End X,    Start Y

    //Top Center (1)
    Vertices[4] =   A3Vertex(SZX[0],                      0,                            0,  0,  0,  0,  AP[1].xstart(),   AP[1].ystart());
    Vertices[5] =   A3Vertex(SZX[0],                      SZY[1],                       0,  0,  0,  0,  AP[1].xstart(),   AP[1].yend());
    Vertices[6] =   A3Vertex(PanelRegion.Size.x - SZX[2], SZY[1],                       0,  0,  0,  0,  AP[1].xend(),     AP[1].yend());
    Vertices[7] =   A3Vertex(PanelRegion.Size.x - SZX[2], 0,                            0,  0,  0,  0,  AP[1].xend(),     AP[1].ystart());

    //Top right (2)
    Vertices[8] =   A3Vertex(PanelRegion.Size.x - SZX[2], 0,                            0,  0,  0,  0,  AP[2].xstart(),   AP[2].ystart());
    Vertices[9] =   A3Vertex(PanelRegion.Size.x - SZX[2], SZY[2],                       0,  0,  0,  0,  AP[2].xstart(),   AP[2].yend());
    Vertices[10] =  A3Vertex(PanelRegion.Size.x,          SZY[2],                       0,  0,  0,  0,  AP[2].xend(),     AP[2].yend());
    Vertices[11] =  A3Vertex(PanelRegion.Size.x,          0,                            0,  0,  0,  0,  AP[2].xend(),     AP[2].ystart());

    //Center Left (3)
    Vertices[12] =  A3Vertex(0,                           SZY[0],                       0,  0,  0,  0,  AP[3].xstart(),   AP[3].ystart());
    Vertices[13] =  A3Vertex(0,                           PanelRegion.Size.y - SZY[6],  0,  0,  0,  0,  AP[3].xstart(),   AP[3].yend());
    Vertices[14] =  A3Vertex(SZX[3],                      PanelRegion.Size.y - SZY[6],  0,  0,  0,  0,  AP[3].xend(),     AP[3].yend());
    Vertices[15] =  A3Vertex(SZX[3],                      SZY[0],                       0,  0,  0,  0,  AP[3].xend(),     AP[3].ystart());

    //Center (4)
    Vertices[16] =  A3Vertex(SZX[3],                      SZY[1],                       0,  0,  0,  0,  AP[4].xstart(),   AP[4].ystart());
    Vertices[17] =  A3Vertex(SZX[3],                      PanelRegion.Size.y - SZY[7],  0,  0,  0,  0,  AP[4].xstart(),   AP[4].yend());
    Vertices[18] =  A3Vertex(PanelRegion.Size.x - SZX[5], PanelRegion.Size.y - SZY[7],  0,  0,  0,  0,  AP[4].xend(),     AP[4].yend());
    Vertices[19] =  A3Vertex(PanelRegion.Size.x - SZX[5], SZY[1],                       0,  0,  0,  0,  AP[4].xend(),     AP[4].ystart());

    //Center right (5)
    Vertices[20] =  A3Vertex(PanelRegion.Size.x - SZX[5], SZY[2],                       0,  0,  0,  0,  AP[5].xstart(),   AP[5].ystart());
    Vertices[21] =  A3Vertex(PanelRegion.Size.x - SZX[5], PanelRegion.Size.y - SZY[8],  0,  0,  0,  0,  AP[5].xstart(),   AP[5].yend());
    Vertices[22] =  A3Vertex(PanelRegion.Size.x,          PanelRegion.Size.y - SZY[8],  0,  0,  0,  0,  AP[5].xend(),     AP[5].yend());
    Vertices[23] =  A3Vertex(PanelRegion.Size.x,          SZY[2],                       0,  0,  0,  0,  AP[5].xend(),     AP[5].ystart());

    //Bottom left (6)
    Vertices[24] =  A3Vertex(0,                           PanelRegion.Size.y - SZY[6],  0,  0,  0,  0,  AP[6].xstart(),   AP[6].ystart());
    Vertices[25] =  A3Vertex(0,                           PanelRegion.Size.y,           0,  0,  0,  0,  AP[6].xstart(),   AP[6].yend());
    Vertices[26] =  A3Vertex(SZX[6],                      PanelRegion.Size.y,           0,  0,  0,  0,  AP[6].xend(),     AP[6].yend());
    Vertices[27] =  A3Vertex(SZX[6],                      PanelRegion.Size.y - SZY[6],  0,  0,  0,  0,  AP[6].xend(),     AP[6].ystart());

    //Bottom Center (7)
    Vertices[28] =  A3Vertex(SZX[6],                      PanelRegion.Size.y - SZY[7],  0,  0,  0,  0,  AP[7].xstart(),   AP[7].ystart());
    Vertices[29] =  A3Vertex(SZX[6],                      PanelRegion.Size.y,           0,  0,  0,  0,  AP[7].xstart(),   AP[7].yend());
    Vertices[30] =  A3Vertex(PanelRegion.Size.x - SZX[8], PanelRegion.Size.y,           0,  0,  0,  0,  AP[7].xend(),     AP[7].yend());
    Vertices[31] =  A3Vertex(PanelRegion.Size.x - SZX[8], PanelRegion.Size.y - SZY[7],  0,  0,  0,  0,  AP[7].xend(),     AP[7].ystart());

    //Bottom Right (8)
    Vertices[32] =  A3Vertex(PanelRegion.Size.x - SZX[8], PanelRegion.Size.y - SZY[8],  0,  0,  0,  0,  AP[8].xstart(),   AP[8].ystart());
    Vertices[33] =  A3Vertex(PanelRegion.Size.x - SZX[8], PanelRegion.Size.y,           0,  0,  0,  0,  AP[8].xstart(),   AP[8].yend());
    Vertices[34] =  A3Vertex(PanelRegion.Size.x,          PanelRegion.Size.y,           0,  0,  0,  0,  AP[8].xend(),     AP[8].yend());
    Vertices[35] =  A3Vertex(PanelRegion.Size.x,          PanelRegion.Size.y - SZY[8],  0,  0,  0,  0,  AP[8].xend(),     AP[8].ystart());
  }
  else
  {
    Vertices[0] =   A3Vertex(0,                           0,                            0,  0,  0,  0,  AP[0].xstart(),   AP[0].ystart());  //Start X,  Start Y
    Vertices[1] =   A3Vertex(0,                           PanelRegion.Size.y,           0,  0,  0,  0,  AP[0].xstart(),   AP[0].yend());    //Start X,  End Y
    Vertices[2] =   A3Vertex(PanelRegion.Size.x,          PanelRegion.Size.y,           0,  0,  0,  0,  AP[0].xend(),     AP[0].yend());    //End X,    End Y
    Vertices[3] =   A3Vertex(PanelRegion.Size.x,          0,                            0,  0,  0,  0,  AP[0].xend(),     AP[0].ystart());  //End X,    Start Y
  }

  glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
  glBufferSubData(GL_ARRAY_BUFFER, 0, (MonoPatch?4:36)* sizeof(A3Vertex), Vertices);
}

void A3Panel::Build()
{
  if(Style == A3Style_Renderless)
  {
    RenderState = ST_ENABLED;
    return;
  }


  glGenVertexArrays(1,&VAO);
  glBindVertexArray(VAO);
  glGenBuffers(2,VBO);

  A3TClass ThemeClass;
  for(int i = 0; i < Theme.class__size(); i++)
  {
    if(Theme.class_(i).type() == ThemeClassType)
    {
      ThemeClass = Theme.class_(i);
      break;
    }
  }

  A3TElement ThemeElement;
  for(int i = 0; i < ThemeClass.element_size(); i++)
  {
    if(ThemeClass.element(i).type() == ThemeElementType)
    {
      ThemeElement = ThemeClass.element(i);
      break;
    }
  }
  MonoPatch = (ThemeElement.patch_size() == 1);
  A3TPatch AP[9];
  float SZX[9];
  float SZY[9];
  for(int i = 0; i < ThemeElement.patch_size(); i++)
  {
    AP[i] = ThemeElement.patch(i);
    SZX[i] = (AP[i].xend() - AP[i].xstart())*64.0;
    SZY[i] = (AP[i].yend() - AP[i].ystart())*64.0;
  }
  if(!MonoPatch)
  {
    //Top Left (0)
    Vertices[0] =   A3Vertex(0,                           0,                            0,  0,  0,  0,  AP[0].xstart(),   AP[0].ystart());  //Start X,  Start Y
    Vertices[1] =   A3Vertex(0,                           SZY[0],                       0,  0,  0,  0,  AP[0].xstart(),   AP[0].yend());    //Start X,  End Y
    Vertices[2] =   A3Vertex(SZX[0],                      SZY[0],                       0,  0,  0,  0,  AP[0].xend(),     AP[0].yend());    //End X,    End Y
    Vertices[3] =   A3Vertex(SZX[0],                      0,                            0,  0,  0,  0,  AP[0].xend(),     AP[0].ystart());  //End X,    Start Y

    //Top Center (1)
    Vertices[4] =   A3Vertex(SZX[0],                      0,                            0,  0,  0,  0,  AP[1].xstart(),   AP[1].ystart());
    Vertices[5] =   A3Vertex(SZX[0],                      SZY[1],                       0,  0,  0,  0,  AP[1].xstart(),   AP[1].yend());
    Vertices[6] =   A3Vertex(PanelRegion.Size.x - SZX[2], SZY[1],                       0,  0,  0,  0,  AP[1].xend(),     AP[1].yend());
    Vertices[7] =   A3Vertex(PanelRegion.Size.x - SZX[2], 0,                            0,  0,  0,  0,  AP[1].xend(),     AP[1].ystart());

    //Top right (2)
    Vertices[8] =   A3Vertex(PanelRegion.Size.x - SZX[2], 0,                            0,  0,  0,  0,  AP[2].xstart(),   AP[2].ystart());
    Vertices[9] =   A3Vertex(PanelRegion.Size.x - SZX[2], SZY[2],                       0,  0,  0,  0,  AP[2].xstart(),   AP[2].yend());
    Vertices[10] =  A3Vertex(PanelRegion.Size.x,          SZY[2],                       0,  0,  0,  0,  AP[2].xend(),     AP[2].yend());
    Vertices[11] =  A3Vertex(PanelRegion.Size.x,          0,                            0,  0,  0,  0,  AP[2].xend(),     AP[2].ystart());

    //Center Left (3)
    Vertices[12] =  A3Vertex(0,                           SZY[0],                       0,  0,  0,  0,  AP[3].xstart(),   AP[3].ystart());
    Vertices[13] =  A3Vertex(0,                           PanelRegion.Size.y - SZY[6],  0,  0,  0,  0,  AP[3].xstart(),   AP[3].yend());
    Vertices[14] =  A3Vertex(SZX[3],                      PanelRegion.Size.y - SZY[6],  0,  0,  0,  0,  AP[3].xend(),     AP[3].yend());
    Vertices[15] =  A3Vertex(SZX[3],                      SZY[0],                       0,  0,  0,  0,  AP[3].xend(),     AP[3].ystart());

    //Center (4)
    Vertices[16] =  A3Vertex(SZX[3],                      SZY[1],                       0,  0,  0,  0,  AP[4].xstart(),   AP[4].ystart());
    Vertices[17] =  A3Vertex(SZX[3],                      PanelRegion.Size.y - SZY[7],  0,  0,  0,  0,  AP[4].xstart(),   AP[4].yend());
    Vertices[18] =  A3Vertex(PanelRegion.Size.x - SZX[5], PanelRegion.Size.y - SZY[7],  0,  0,  0,  0,  AP[4].xend(),     AP[4].yend());
    Vertices[19] =  A3Vertex(PanelRegion.Size.x - SZX[5], SZY[1],                       0,  0,  0,  0,  AP[4].xend(),     AP[4].ystart());

    //Center right (5)
    Vertices[20] =  A3Vertex(PanelRegion.Size.x - SZX[5], SZY[2],                       0,  0,  0,  0,  AP[5].xstart(),   AP[5].ystart());
    Vertices[21] =  A3Vertex(PanelRegion.Size.x - SZX[5], PanelRegion.Size.y - SZY[8],  0,  0,  0,  0,  AP[5].xstart(),   AP[5].yend());
    Vertices[22] =  A3Vertex(PanelRegion.Size.x,          PanelRegion.Size.y - SZY[8],  0,  0,  0,  0,  AP[5].xend(),     AP[5].yend());
    Vertices[23] =  A3Vertex(PanelRegion.Size.x,          SZY[2],                       0,  0,  0,  0,  AP[5].xend(),     AP[5].ystart());

    //Bottom left (6)
    Vertices[24] =  A3Vertex(0,                           PanelRegion.Size.y - SZY[6],  0,  0,  0,  0,  AP[6].xstart(),   AP[6].ystart());
    Vertices[25] =  A3Vertex(0,                           PanelRegion.Size.y,           0,  0,  0,  0,  AP[6].xstart(),   AP[6].yend());
    Vertices[26] =  A3Vertex(SZX[6],                      PanelRegion.Size.y,           0,  0,  0,  0,  AP[6].xend(),     AP[6].yend());
    Vertices[27] =  A3Vertex(SZX[6],                      PanelRegion.Size.y - SZY[6],  0,  0,  0,  0,  AP[6].xend(),     AP[6].ystart());

    //Bottom Center (7)
    Vertices[28] =  A3Vertex(SZX[6],                      PanelRegion.Size.y - SZY[7],  0,  0,  0,  0,  AP[7].xstart(),   AP[7].ystart());
    Vertices[29] =  A3Vertex(SZX[6],                      PanelRegion.Size.y,           0,  0,  0,  0,  AP[7].xstart(),   AP[7].yend());
    Vertices[30] =  A3Vertex(PanelRegion.Size.x - SZX[8], PanelRegion.Size.y,           0,  0,  0,  0,  AP[7].xend(),     AP[7].yend());
    Vertices[31] =  A3Vertex(PanelRegion.Size.x - SZX[8], PanelRegion.Size.y - SZY[7],  0,  0,  0,  0,  AP[7].xend(),     AP[7].ystart());

    //Bottom Right (8)
    Vertices[32] =  A3Vertex(PanelRegion.Size.x - SZX[8], PanelRegion.Size.y - SZY[8],  0,  0,  0,  0,  AP[8].xstart(),   AP[8].ystart());
    Vertices[33] =  A3Vertex(PanelRegion.Size.x - SZX[8], PanelRegion.Size.y,           0,  0,  0,  0,  AP[8].xstart(),   AP[8].yend());
    Vertices[34] =  A3Vertex(PanelRegion.Size.x,          PanelRegion.Size.y,           0,  0,  0,  0,  AP[8].xend(),     AP[8].yend());
    Vertices[35] =  A3Vertex(PanelRegion.Size.x,          PanelRegion.Size.y - SZY[8],  0,  0,  0,  0,  AP[8].xend(),     AP[8].ystart());
  }
  else
  {
    cout << "BUILD MONOPATCH:" << Name << endl;
    Vertices[0] =   A3Vertex(0,                           0,                            0,  0,  0,  0,  AP[0].xstart(),   AP[0].ystart());  //Start X,  Start Y
    Vertices[1] =   A3Vertex(0,                           PanelRegion.Size.y,           0,  0,  0,  0,  AP[0].xstart(),   AP[0].yend());    //Start X,  End Y
    Vertices[2] =   A3Vertex(PanelRegion.Size.x,          PanelRegion.Size.y,           0,  0,  0,  0,  AP[0].xend(),     AP[0].yend());    //End X,    End Y
    Vertices[3] =   A3Vertex(PanelRegion.Size.x,          0,                            0,  0,  0,  0,  AP[0].xend(),     AP[0].ystart());  //End X,    Start Y
  }

  Indices[0] = 0;
  Indices[1] = 1;
  Indices[2] = 2;
  Indices[3] = 0;
  Indices[4] = 2;
  Indices[5] = 3;
  if(!MonoPatch)
  {

    Indices[6] = 4;
    Indices[7] = 5;
    Indices[8] = 6;
    Indices[9] = 4;
    Indices[10] = 6;
    Indices[11] = 7;

    Indices[12] = 8;
    Indices[13] = 9;
    Indices[14] = 10;
    Indices[15] = 8;
    Indices[16] = 10;
    Indices[17] = 11;

    Indices[18] = 12;
    Indices[19] = 13;
    Indices[20] = 14;
    Indices[21] = 12;
    Indices[22] = 14;
    Indices[23] = 15;

    Indices[24] = 16;
    Indices[25] = 17;
    Indices[26] = 18;
    Indices[27] = 16;
    Indices[28] = 18;
    Indices[29] = 19;

    Indices[30] = 20;
    Indices[31] = 21;
    Indices[32] = 22;
    Indices[33] = 20;
    Indices[34] = 22;
    Indices[35] = 23;

    Indices[36] = 24;
    Indices[37] = 25;
    Indices[38] = 26;
    Indices[39] = 24;
    Indices[40] = 26;
    Indices[41] = 27;

    Indices[42] = 28;
    Indices[43] = 29;
    Indices[44] = 30;
    Indices[45] = 28;
    Indices[46] = 30;
    Indices[47] = 31;

    Indices[48] = 32;
    Indices[49] = 33;
    Indices[50] = 34;
    Indices[51] = 32;
    Indices[52] = 34;
    Indices[53] = 35;
  }

  glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
  glBufferData(GL_ARRAY_BUFFER, (MonoPatch?4:36) * sizeof(A3Vertex) , 0, GL_DYNAMIC_DRAW);
  glBufferSubData(GL_ARRAY_BUFFER, 0, (MonoPatch?4:36)* sizeof(A3Vertex), Vertices);

  glEnableVertexAttribArray(A3UI::TextShader->VertexPointer);
  glEnableVertexAttribArray(A3UI::TextShader->NormalPointer);
  glEnableVertexAttribArray(A3UI::TextShader->TexCoordPointer);


  glVertexAttribPointer(A3UI::TextShader->VertexPointer, 3, GL_FLOAT, GL_FALSE, sizeof(A3Vertex), offsetof(A3Vertex,X));
  glVertexAttribPointer(A3UI::TextShader->NormalPointer, 3, GL_FLOAT, GL_FALSE, sizeof(A3Vertex), (void *)offsetof(A3Vertex,NX));
  glVertexAttribPointer(A3UI::TextShader->TexCoordPointer, 2, GL_FLOAT, GL_FALSE, sizeof(A3Vertex), (void *)offsetof(A3Vertex,U));

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO[1]);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, (MonoPatch?6:54) * sizeof(unsigned int), 0, GL_STATIC_DRAW);
  glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, (MonoPatch?6:54) * sizeof(unsigned int), Indices);
  RenderState = ST_ENABLED;
}


A3Panel::~A3Panel()
{
  if(Parent)
    Parent->DeleteChild(this);
  cout << "Calling Dispose on " << Name << endl;
  RenderState = ST_DESTROYING;
  if(Vertices)
    delete[] Vertices;
  if(Indices)
    delete[] Indices;
  if(VBO[0])
    glDeleteBuffersARB(1, &VBO[0]);
  if(VBO[1])
    glDeleteBuffersARB(1, &VBO[1]);
  if(VAO)
    glDeleteVertexArrays(1, &VAO);

  if(!Parent)
    cout << "Disposed Root Node" << endl;
  else
    cout << "Disposed Panel: " << Name << endl;
}
