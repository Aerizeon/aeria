#ifndef A3PANEL_H
#define A3PANEL_H

#include "A3Include.h"
#include "A3Engine/A3Input.h"
#include "A3Engine/Parser/Theme/A3Theme.pb.h"
#include "A3Engine/Data/A3Texture.h"
#include "SimpleSignal.hpp"
#include <sqrat.h>
#include <sqstdmath.h>

using namespace Simple;

enum A3UI_State
{
  UI_DISABLED,  ///Element is entirely disabled
  UI_ACTIVE,    ///Element is focused and accepting input
  UI_INACTIVE,  ///Element is in the background, and not accepting input
  UI_MOVING,    ///Element is currently being moved
  UI_RESIZING,  ///Element is currently being resized
};

enum A3StyleFlags
{
  A3Style_Default     = 0,
  A3Style_Borderless  = 1,
  A3Style_Title       = 2,
  A3Style_Sizable     = 4,
  A3Style_Static      = 8,
  A3Style_Renderless  = 0xFFFFFFFF
};

enum A3Attachment
{
  A3Attachment_None   = 0,
  A3Attachment_Top    = 1,
  A3Attachment_Bottom = 2,
  A3Attachment_Left   = 4,
  A3Attachment_Right  = 8,
  A3Attachment_Center = 16
};

class A3Panel
{
public:
  unsigned int  ID          = 0;
  string        Name        = "";
  A3UI_State    State       = UI_DISABLED;
  A3_State      RenderState = ST_NULL;
  A3THeader     Theme       = Settings::DefaultTheme;

  A3Panel ();
  A3Panel (const A3Panel &pan);
  A3Panel (const string PName, const glm::vec2 PPosition, const glm::vec2 PSize, unsigned int PStyle = A3Style_Default);

  virtual void        SetPosition (const glm::vec2 PPosition);
  virtual void        SetSize     (const glm::vec2 PSize);
  virtual void        SetTexture  (A3Texture *PTexture);
  virtual glm::vec2   GetPanelPosition();
  virtual glm::vec2   GetPanelSize();
  virtual glm::vec2   GetClientSize();

  virtual glm::vec2   PanelToScreen();
  virtual glm::vec2   PanelToScreen(glm::vec2 In);
  virtual glm::vec2   ClientToScreen();
  virtual glm::vec2   ClientToScreen(glm::vec2 In);
  virtual A3Texture*  GetTexture();
  virtual void        Focus();
  virtual bool        HasFocus();
  virtual bool        IntMouseEvent(A3MouseState Mouse);
  virtual bool        IntKeyEvent(A3KeyState Key);
  virtual bool        MouseEvent(A3MouseState Mouse);
  virtual bool        KeyEvent(A3KeyState Key);
  Signal<bool()>      MouseLeftClick;
  Signal<bool()>      MouseLeftDown;
  Signal<bool(), CollectorWhile0<bool>>      MouseLeftUp;

  virtual void        AddChild(A3Panel *P);
  virtual void        AddIntChild(A3Panel *P);
  virtual void        Render(float Delta);
  virtual void        DeleteChild(A3Panel *P);
  virtual             ~A3Panel();

//protected:

  A3Vertex        *Vertices       = NULL;
  unsigned int    *Indices        = NULL;
  unsigned int    VertexCount     = 0;
  unsigned int    IndexCount      = 0;
  unsigned int    VBO[2]          = {0,0};
  unsigned int    VAO             = 0;

  A3Region2f      PanelRegion     = A3Region2f();
  A3Region2f      ClientRegion    = A3Region2f();
  glm::vec2       MinSize         = glm::vec2();
  glm::quat       Rotation        = glm::quat();   ///Current Rotation

  glm::vec2       MouseOffset     = glm::vec2();
  A3MouseState    LastMouseEvent  = A3MouseState();
  glm::vec2       AttachOffset    = glm::vec2();
  A3UI_State      LastState       = UI_DISABLED;

  unsigned int    Style           = A3Style_Default;
  unsigned int    Attachment      = A3Attachment_None;
  string          ThemeClassType  = "Window";
  string          ThemeElementType= "Default";
  bool            MonoPatch       = false;
  A3Texture       *Texture        = NULL;
  A3Panel         *Parent         = NULL;
  Sqrat::Function OnSqClose;
  Sqrat::Function OnSqDraw;
  list<A3Panel*>  Children;
  list<A3Panel*>  IntChildren;


  void Update();
  void Build();

private:

};

#endif // A3PANEL_H
