// *** ADDED BY HEADER FIXUP ***
#include <cstdio>
// *** END ***
#include "A3Program.h"

#include "A3Engine/Data/A3Shader.h"


A3Program::A3Program(string FileName)
{
  ID = glCreateProgram();
  A3Shader *Frag = new A3Shader(FileName+".glf",GL_FRAGMENT_SHADER);
  A3Shader *Vert = new A3Shader(FileName+".glv",GL_VERTEX_SHADER);
  glAttachShader(ID,Frag->ID);
  glAttachShader(ID,Vert->ID);
  glLinkProgram(ID);

  int result;
  glGetProgramiv(ID, GL_LINK_STATUS, &result);
  if(result == GL_FALSE)
  {
    int length;
    glGetProgramiv(ID, GL_INFO_LOG_LENGTH, &length);
    char *log = new char[length];
    glGetProgramInfoLog(ID, length, &result, log);

    fprintf(stderr, "Program linking failed: %s\n", log);
    delete[] log;
    log = NULL;
    glDeleteProgram(ID);
  }

  VertexPointer   = glGetAttribLocation(ID, "VertexPosition");
  NormalPointer   = glGetAttribLocation(ID, "VertexNormal");
  TexCoordPointer = glGetAttribLocation(ID, "VertexTextureCoord");

  CameraCombinedMatrix = glGetUniformLocation(ID, "CameraCombinedMatrix");
  ObjectCombinedMatrix = glGetUniformLocation(ID, "ObjectCombinedMatrix");
  ObjectSeparateMatrix = glGetUniformLocation(ID, "ObjectSeparateMatrix");
  ObjectColor          = glGetUniformLocation(ID, "ObjectColor");

  TextureDiffuse  = glGetUniformLocation(ID, "TextureDiffuse");
  TextureNormal   = glGetUniformLocation(ID, "TextureNormal");
  TextureMask     = glGetUniformLocation(ID, "TextureMask");

  ColorBuffer     = glGetUniformLocation(ID, "ColorBuffer");
  NormalBuffer    = glGetUniformLocation(ID, "NormalBuffer");
  PositionBuffer  = glGetUniformLocation(ID, "PositionBuffer");
  glUseProgram(ID);

  glUniform1i(TextureDiffuse, 0);
  glUniform1i(TextureNormal,  1);
  glUniform1i(TextureMask,    2);

  glUniform1i(ColorBuffer,    0);
  glUniform1i(NormalBuffer,   1);
  glUniform1i(PositionBuffer, 2);

  LightBuffer = glGetUniformBlockIndex(ID, "LightBuffer");
  GLint blockSize;
  glGetActiveUniformBlockiv(ID, LightBuffer, GL_UNIFORM_BLOCK_DATA_SIZE, &blockSize);
  cout << "SizeExt:" << blockSize << ", SizeInt: " << sizeof(LightInfoBuffer) << endl;

  glGenBuffers(1, &LightBufferObj);
  glBindBuffer(GL_UNIFORM_BUFFER, LightBufferObj);
  glBufferData(GL_UNIFORM_BUFFER, sizeof(LightInfoBuffer), 0, GL_STATIC_DRAW );
  LightInfoBuffer.LightCount = 10;
  for(int i = 0; i <10; i++)
  {

    LightInfoBuffer.LightDynamic[i].Position = glm::vec3((12-(((float)rand())/((float)RAND_MAX))*24), 0.01, -9.0+ (12-((((float)rand())/((float)RAND_MAX))*24)));
    LightInfoBuffer.LightDynamic[i].Color    = glm::vec3((((float)rand())/((float)RAND_MAX))*50,(((float)rand())/((float)RAND_MAX))*50,(((float)rand())/((float)RAND_MAX))*50);
  }
  LightInfoBuffer.LightStatic.Color = glm::vec3(0.5);
  glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(LightInfoBuffer), &LightInfoBuffer);
  glBindBufferBase(GL_UNIFORM_BUFFER, LightBuffer,LightBufferObj);
}
float ctr = 0;
void A3Program::UpdateLights(float Delta)
{
  ctr+=Delta;
  //LightInfoBuffer.LightDynamic[0].Position = glm::vec3(cos(ctr)*2,1.0,-9.0 + (sin(ctr)*2));
  //LightInfoBuffer.LightDynamic[0].Color = glm::vec3(0,0,100.0);
  glBindBuffer(GL_UNIFORM_BUFFER, LightBufferObj);
  for(int i = 0; i <10; i++)
  {
    //LightInfoBuffer.LightDynamic[i].Position.y = cos(ctr)+1.001;
    LightInfoBuffer.LightDynamic[i].Attenuation = glm::vec3(0.0025, 1, 3);
  }
  glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(LightInfoBuffer), &LightInfoBuffer);
}

A3Program::~A3Program()
{
  //dtor
}
