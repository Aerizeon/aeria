#ifndef PROGRAM_H
#define PROGRAM_H

#include <string>
#include "A3Include.h"

using namespace std;

#pragma pack(push, 1)
struct LightSettings
{
  glm::vec3 Position    = glm::vec3();
  float     Padding0    = 0;
  glm::vec3 Color       = glm::vec3();
  float     Padding1    = 0;
  glm::vec3 Attenuation = glm::vec3();
  float     Padding2    = 0;
};

struct Lights
{
  LightSettings LightDynamic[100];
  LightSettings LightStatic;
  int           LightCount;
};
#pragma pack(pop)

class A3Program
{
public:
  unsigned int ID;

  unsigned int VertexPointer;
  unsigned int NormalPointer;
  unsigned int TexCoordPointer;

  unsigned int CameraCombinedMatrix;
  unsigned int ObjectCombinedMatrix;
  unsigned int ObjectSeparateMatrix;

  unsigned int ObjectColor;


  unsigned int TextureDiffuse;
  unsigned int TextureNormal;
  unsigned int TextureMask;
  unsigned int TextureShadow;
  unsigned int ColorBuffer;
  unsigned int NormalBuffer;
  unsigned int PositionBuffer;

  unsigned int LightBuffer; //LightBuffer uniform object
  unsigned int LightBufferObj;
  signed int LightBufferOffsets[2]; // LightCount + LightInfo
  Lights       LightInfoBuffer;


  A3Program (string FileName);
  void UpdateLights(float Delta);
  virtual ~A3Program();
protected:
private:

};

#endif // PROGRAM_H
