#include "Terrain.h"
#include "math.h"
int Max;
Terrain::Terrain()
{
    Position.SetValue(0,-4,0);
    Loaded = false;
    glGenVertexArrays(1,&VAO);
    glBindVertexArray(VAO);
    glGenBuffers(2, VBO);
    TerrainSize = 4096;
    Max = sqrt(TerrainSize);
    cout << Max << endl;
    int IndexCount = (Max * 2) * (Max - 1) + (Max - 2);
    Vertices = new Vertex[TerrainSize];
    Indices = new unsigned int[IndexCount];
    float half = ((float)Max - 1.0f) / 2.0f;
    int index = 0;
    for(int z=0; z<Max; z++)
    {
        for(int x=0; x<Max; x++)
        {
            Vertex V;
            V.X = (float)x-half;
            V.Y = 0;
            (float)(rand()%5)/2;
            V.Z = (float)z-half;
            V.NX = 0;
            V.NY = 1.0f;
            V.NZ = 0;
            V.U =  (float)x / (Max - 1);
            V.V = (float)z / (Max - 1);
            Vertices[(z*Max)+x] = V;
        }
    }

    for ( int z = 0; z < Max - 1; z++ )
    {
        if ( z % 2 == 0 )
        {
            int x;
            for (x = 0; x < Max; x++ )
            {
                Indices[index++] = x + (z * Max);
                Indices[index++] = x + (z * Max) + Max;
            }
            if ( z != Max - 2)
            {
                Indices[index++] = --x + (z * Max);
            }
        }
        else
        {
            // Odd row
            int x;
            for ( x = Max - 1; x >= 0; x-- )
            {
                Indices[index++] = x + (z * Max);
                Indices[index++] = x + (z * Max) + Max;
            }
            if ( z != Max - 2)
            {
                Indices[index++] = ++x + (z * Max);
            }
        }
    }
    cout << "Binding.." << endl;
    glBindBuffer(GL_ARRAY_BUFFER, VBO[VBO_DATA]);
    glBufferData(GL_ARRAY_BUFFER, TerrainSize * sizeof(Vertex) , 0, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0,TerrainSize* sizeof(Vertex), Vertices);


    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBO[VBO_INDICES]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) *IndexCount, 0, GL_STATIC_DRAW);
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER,0,sizeof(unsigned int) * IndexCount,Indices);

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(World::PLighting->NormalPointer);
    glEnableVertexAttribArray(World::PLighting->TexCoordPointer);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), offsetof(Vertex,X));
    glVertexAttribPointer(World::PLighting->NormalPointer, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex,NX));
    glVertexAttribPointer(World::PLighting->TexCoordPointer, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex,U));
    cout << "Terrain Generated" << endl;
    Loaded = true;
}
Matrix M;
Quaternion Q;
void Terrain::Render(float delta)
{
    if(!Loaded)
        return;
    //Q = Quaternion(Vector3f(0,100*delta,0)) * Q;
    Q.ToMatrix(M);
    M.Translate(Position);
    //glUniformMatrix4fv(World::PLighting->ObjectCombinedMatrix, 1,false, (M*World::CurrentCamera.ModelView).Elements);
    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLE_STRIP, (Max*2) * (Max-1) + (Max-2), GL_UNSIGNED_INT, 0);
}
Terrain::~Terrain()
{
    //dtor
}
