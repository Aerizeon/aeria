#ifndef TERRAIN_H
#define TERRAIN_H

#include "Entity.h"
#include "A3Engine/Data/Texture.h"
#include "A3Engine/World.h"

class Terrain:public Entity
{
public:
  int             TerrainSize;
  unsigned int    VBO[VBO_MAX];
  unsigned int    VAO;
  Vertex          *Vertices;
  unsigned int    *Indices;
  bool Loaded;

  Terrain();
  void Render(float Delta);
  virtual ~Terrain();
protected:
private:
};

#endif // TERRAIN_H
