#include "A3Textbox.h"
#include "A3Engine/A3UI.h"

A3Textbox::A3Textbox()
  :A3Panel()
{
  //Does nothing
}

A3Textbox::A3Textbox(const A3Textbox &win)
{
  cout << "TODO: Add copy constructor to A3Textbox" << endl;
}
A3Panel *Carat = 0;
A3Textbox::A3Textbox(const string PName, const glm::vec2 PPosition, const glm::vec2 PSize, unsigned int PStyle)
  :A3Panel(PName, PPosition, PSize, PStyle)
{
  ThemeClassType = "Window";
  ThemeElementType = "Textbox";
  ClientRegion.Position = glm::vec2(3.0,3.0);
  ClientRegion.Size = (PSize - glm::vec2(3*2,3*2));
  PanelRegion.Position = PPosition;
  PanelRegion.Size = PSize;
  A3Font *F = A3Font::Create("./cache/fonts/segoeui.ttf",10.0);
  Lbl_Text = new A3Label(L"", F);
  Lbl_Text->SetPosition(glm::vec2(2,15));
  AddChild(Lbl_Text);
  Carat = new A3Panel("Carat", glm::vec2(0,2), glm::vec2(1, F->FontHeight), A3Style_Borderless);
  Carat->ThemeElementType = "Carat";
  AddIntChild(Carat);
}

void A3Textbox::Render(float Delta)
{
  A3Panel::Render(Delta);
  if(HasFocus())
  {
    if(fmod(A3UI::CurrentCamera.TimeElapsed,1.0) < 0.5)
    {
      Carat->Style = A3Style_Renderless;
    }
    else
    {
      Carat->Style = A3Style_Borderless;
      Carat->SetPosition(glm::vec2(Lbl_Text->ClientRegion.Size.x,2.0));
    }
  }
  else
  {
    Carat->Style = A3Style_Renderless;
  }
}

bool A3Textbox::IntKeyEvent(A3KeyState State)
{
  if(State.Pressed)
  {
    if(State.Character == 8)
    {
      if(Text.length() > 0)
        Text.pop_back();
    }
    else
    {
      Text.push_back(State.Character);
    }

    Lbl_Text->SetText(Text);
  }
  return true;
}

bool A3Textbox::MouseEvent(A3MouseState State)
{
  //this->Focus();
}

A3Textbox::~A3Textbox()
{
  //dtor
}
