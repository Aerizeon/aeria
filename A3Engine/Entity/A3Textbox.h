#ifndef A3TEXTBOX_H
#define A3TEXTBOX_H

#include "A3Engine/Entity/A3Panel.h"
#include "A3Engine/Entity/A3Label.h"

class A3Textbox: public A3Panel
{
public:
  wstring   Text;
  A3Textbox();
  A3Textbox (const A3Textbox &win);
  A3Textbox (const string PName, const glm::vec2 PPosition, const glm::vec2 PSize, unsigned int PStyle = A3Style_Default);
  void      Render(float Delta);
  bool      IntKeyEvent(A3KeyState State);
  bool      MouseEvent(A3MouseState State);
  virtual   ~A3Textbox();
protected:
private:
  bool      Multiline = false;
  A3Label   *Lbl_Text = nullptr;
  A3Panel   *Carat    = nullptr;
};

#endif // A3TEXTBOX_H
