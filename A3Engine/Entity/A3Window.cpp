#include "A3Window.h"
A3Window::A3Window()
  :A3Panel()
{
  //Does nothing
}

A3Window::A3Window(const A3Window &win)
{
  cout << "TODO: Add Copy Construct for A3Window" << endl;
}
A3Window::A3Window(const string PName, const glm::vec2 PPosition, const glm::vec2 PSize, unsigned int PStyle)
  :A3Panel(PName, PPosition, PSize, PStyle)
{
  MinSize = glm::vec2(50,50);
  if(PStyle != A3Style_Renderless && PStyle != A3Style_Borderless)
  {
    A3Texture *T = A3Texture::Create("Default.png", A3Texture_NoFilter);
    Btn_Minimize  = new A3Panel("BTN_MINIMIZE", glm::vec2(-32, -22), glm::vec2(21,21),A3Style_Default);
    Btn_Close     = new A3Panel("BTN_CLOSE", glm::vec2(-16,-22), glm::vec2(21,21),A3Style_Default);
    Btn_Close->Attachment = A3Attachment_Right|A3Attachment_Top;
    Btn_Close->ThemeClassType = "Window";
    Btn_Close->ThemeElementType = "Button_Close";
    Btn_Close->State = UI_ACTIVE;
    Btn_Close->SetTexture(T);
    Btn_Close->MouseLeftClick += Simple::slot(this,&A3Window::OnClosing);
    this->AddIntChild(Btn_Close);

    A3Font *F = A3Font::Create("./cache/fonts/segoeui.ttf");
    Lbl_Title     = new A3Label(PName,F);
    Lbl_Title->Name.append("_LBL");
    Lbl_Title->SetPosition(glm::vec2(0,-8));
    Lbl_Title->Attachment = A3Attachment_Center|A3Attachment_Top;
    this->AddIntChild(Lbl_Title);
  }
}

bool A3Window::MouseEvent(A3MouseState Mouse)
{
  A3Panel::MouseEvent(Mouse);
}


void A3Window::Render(float Delta)
{
  A3Panel::Render(Delta);
  if(!OnSqDraw.IsNull())
  {
    OnSqDraw.Execute(this, Delta);
  }
}

void A3Window::OnLeftClick()
{
  cout << "OnClick Fired!" << endl;
}

bool A3Window::OnClosing()
{
  if(!OnSqClose.IsNull())
  {
    try
    {
      cout << "Calling SquirrelClose" << endl;
      OnSqClose.Execute();
    }
    catch(...)
    {
      cout << "SQUIRREL EXCEPTION: " /*ex.Message()*/<< endl;
    }
  }
  delete this;
}

void A3Window::OnClose()
{
  cout << "OnClick Fired!" << endl;
}
A3Window::~A3Window()
{
}
