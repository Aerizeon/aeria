#ifndef A3WINDOW_H
#define A3WINDOW_H

#include "A3Engine/A3Input.h"
#include "A3Engine/Data/A3Texture.h"
#include "A3Engine/Entity/A3Label.h"
enum A3WindowControlFlags
{
  Control_None = 0
};

class A3Window: public A3Panel
{
public:
  unsigned int  ControlFlags  = Control_None;
  A3Window      ();
  A3Window      (const A3Window &win);
  A3Window      (const string PName, const glm::vec2 PPosition, const glm::vec2 PSize, unsigned int PStyle = A3Style_Default);
  bool          MouseEvent(A3MouseState Mouse);
  virtual void  Render(float Delta);
  void OnLeftClick();
  bool OnClosing();
  void OnClose();
  ~A3Window();

protected:
private:
  A3Panel *Btn_Close    = NULL;
  A3Panel *Btn_Minimize = NULL;
  A3Label *Lbl_Title    = NULL;
};

#endif // A3WINDOW_H
