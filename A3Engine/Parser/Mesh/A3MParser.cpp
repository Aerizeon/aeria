#include "A3MParser.h"
#include "A3MData.pb.h"

char A3MParser::Parse(ifstream &File, A3Mesh *Mesh)
{
  A3MHeader       Header;

  if(!Header.ParseFromIstream(&File))
    return ERR_FILE_TYPE_WRONG;

  if(Header.identifier() != A3M_IDENTIFIER)
    return ERR_FILE_FORMAT_WRONG;

  unsigned int MaterialCount  =   Header.material_size();
  unsigned int LODCount       =   Header.lod_size();

  A3Material Materials[MaterialCount];

  for(unsigned int CMaterial = 0; CMaterial < MaterialCount; CMaterial++)
  {
    const A3MMaterial Material      =   Header.material(CMaterial);
    Materials[CMaterial].Name       =   basename(Material.texture_diffuse());
    Materials[CMaterial].Texture    =   0;
  }

  for(unsigned int CLod = 0; CLod < LODCount; CLod++)
  {
    const A3MLod LOD = Header.lod(CLod);
    unsigned int SurfaceCount = LOD.surface_size();

    unsigned int ITriangle  =   0;
    unsigned int IVertex    =   0;
    unsigned int IIndices   =   0;

    for(unsigned int CSurface = 0; CSurface < SurfaceCount; CSurface++)
    {
      const A3MSurface Surface = LOD.surface(CSurface);

      unsigned int TriangleCount  =   Surface.triangle_size();
      unsigned int VertexCount    =   Surface.vertex_size();

      A3VertexGroup Group;
      Group.Name          =   Surface.name();
      Group.IndexOffset   =   Mesh->IndexCount * sizeof(unsigned int);
      Group.IndexCount    =   TriangleCount * 3;
      Mesh->Group.push_back(Group);
      Mesh->GroupCount++;

      Mesh->VertexCount   +=  VertexCount;
      Mesh->IndexCount    +=  TriangleCount * 3;
    }

    Mesh->Vertices  =   new A3Vertex[Mesh->VertexCount];
    Mesh->Indices   =   new unsigned int[Mesh->IndexCount];

    for(unsigned int CSurface = 0; CSurface < SurfaceCount; CSurface++)
    {
      const A3MSurface Surface    =   LOD.surface(CSurface);

      unsigned int MaterialID     =   Surface.material();
      unsigned int TriangleCount  =   Surface.triangle_size();
      unsigned int VertexCount    =   Surface.vertex_size();

      for(unsigned int CVertex = 0; CVertex < VertexCount; CVertex++)
      {
        const A3MVertex Vertex  =   Surface.vertex(CVertex);

        Mesh->Vertices[IVertex].X   =   Vertex.x();
        Mesh->Vertices[IVertex].Y   =   Vertex.y();
        Mesh->Vertices[IVertex].Z   =   Vertex.z();
        Mesh->Vertices[IVertex].NX  =   Vertex.nx();
        Mesh->Vertices[IVertex].NY  =   Vertex.ny();
        Mesh->Vertices[IVertex].NZ  =   Vertex.nz();
        Mesh->Vertices[IVertex].U   =   Vertex.u();
        Mesh->Vertices[IVertex].V   =   Vertex.v();
        IVertex++;
      }

      for(unsigned int CTriangle = 0; CTriangle < TriangleCount; CTriangle++)
      {
        const A3MTriangle Triangle  =   Surface.triangle(CTriangle);

        Mesh->Indices[(IIndices * 3) + 0]   =   Triangle.index(0) + ITriangle;
        Mesh->Indices[(IIndices * 3) + 1]   =   Triangle.index(1) + ITriangle;
        Mesh->Indices[(IIndices * 3) + 2]   =   Triangle.index(2) + ITriangle;
        IIndices++;
      }

      if(MaterialID < MaterialCount)
      {
        if(!Materials[MaterialID].Texture)
        {
          Materials[MaterialID].Texture   =   A3Texture::Create(string(Materials[MaterialID].Name.c_str()));
        }
        Mesh->Group[CSurface].Material  =   Materials[MaterialID];
      }

      ITriangle   +=  VertexCount;
    }
  }
  return ERR_NONE;
}
