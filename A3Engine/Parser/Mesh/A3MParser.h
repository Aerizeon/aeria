#ifndef A3MPARSER_H
#define A3MPARSER_H

#include "A3Engine/Data/A3Mesh.h"
#include "A3Engine/A3Settings.h"

#define A3M_IDENTIFIER   0x30464D41
#define A3M_VERSION     1001

struct MatchPathSeparator
{
  bool operator()( char ch ) const
  {
    return ch == '\\' || ch == '/';
  }
};

class A3MParser
{
public:
  char Parse(ifstream &File, A3Mesh *Mesh);

private:
  static inline std::string basename( std::string pathname )
  {
    return std::string(
             std::find_if( pathname.rbegin(), pathname.rend(),
                           MatchPathSeparator() ).base(),
             pathname.end() );
  }
};

#endif // A3MPARSER_H
