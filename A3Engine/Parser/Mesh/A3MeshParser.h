#ifndef MESHPARSER_H
#define MESHPARSER_H

#include "A3Engine/Data/A3Mesh.h"

class A3MeshParser
{
public:
  A3MeshParser();
  virtual ~A3MeshParser();
protected:
  unsigned int FileSize       = 0;
  unsigned int VertexCount    = 0;
  unsigned int TexUVCount     = 0;
  unsigned int NormalCount    = 0;
  unsigned int IndexCount     = 0;

  A3Vertex        *Vertices   = 0;
  A3Vertex        *TexCoords  = 0;
  A3Vertex        *Normals    = 0;
  unsigned int    *Indices    = 0;
};

#endif // MESHPARSER_H
