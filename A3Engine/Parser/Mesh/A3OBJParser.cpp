#include "A3OBJParser.h"
#include <strtk.hpp>

char A3OBJParser::Scan(ifstream &File, A3Mesh *Mesh)
{
  //Get filesize and allocate a buffer to contain the data
  File.seekg (0, ios::end);
  FileSize = File.tellg();
  FileBuffer =  new char[FileSize];
  File.seekg(0,ios::beg);
  File.clear();

  //Read file contents and split it into lines
  File.read(FileBuffer,FileSize);
  strtk::parse(FileBuffer,"\r\n",Line,strtk::split_options::compress_delimiters);
  delete[] FileBuffer;
  FileBuffer = NULL;

  unsigned int LSize = Line.size();
  A3VertexGroup Group;
  for(unsigned int LIndex = 0; LIndex < LSize; LIndex++)
  {
    Segment.clear();
    strtk::trim(" ", Line[LIndex]);
    strtk::convert_to_uppercase(Line[LIndex]);
    strtk::parse(Line[LIndex]," ",Segment);
    unsigned int SegmentCount = Segment.size();
    if(Segment[0] == "V")
    {
      VertexCount++;
    }

    else if(Segment[0] == "VT")
    {
      TexUVCount+=2;
    }

    else if(Segment[0] == "VN")
    {
      NormalCount+=2;
    }

    else if(Segment[0] == "F" && SegmentCount == 4)
    {
      IndexCount+=3;
      Group.IndexCount+=3;
    }

    else if(Segment[0] == "F" && SegmentCount == 5)
    {
      IndexCount+=6;
      Group.IndexCount +=6;
    }

    else if(Segment[0] == "G" || Segment[0] == "USEMTL")
    {
      /*Mesh->Group.push_back(Group);
      Group.IndexCount = 0;
      Group.IndexOffset = IndexCount*sizeof(unsigned int);*/
    }
  }
  Mesh->Group.push_back(Group);
  Mesh->GroupCount = Mesh->Group.size();
  return ERR_NONE;
}

char A3OBJParser::Parse(ifstream &File, A3Mesh *Mesh)
{
  char rc;
  if((rc = Scan(File,Mesh)) != ERR_NONE )
    return rc;

  Mesh->Vertices = new A3Vertex[VertexCount];
  Mesh->Indices = new unsigned int[IndexCount];
  TexCoords = new A3Vertex[TexUVCount];
  Normals = new A3Vertex[NormalCount];

  unsigned int LSize = Line.size();
  unsigned int IndexCounter   = 0;
  unsigned int VertexCounter  = 0;
  unsigned int TexUVCounter   = 0;
  unsigned int NormalCounter  = 0;

  for(unsigned int LIndex = 0; LIndex < LSize; LIndex++)
  {
    Segment.clear();
    strtk::convert_to_uppercase(Line[LIndex]);
    strtk::parse(Line[LIndex]," ",Segment);
    unsigned int SSize = Segment.size();
    strtk::ignore_token ignore;

    if(Segment[0] == "V")
    {
      A3Vertex V;
      strtk::parse(Line[LIndex]," ", ignore,V.X,V.Y,V.Z);
      Mesh->Vertices[VertexCounter++] = V;
    }

    else if(Segment[0] == "VT")
    {
      A3Vertex V;
      strtk::parse(Line[LIndex], " ", ignore, V.U, V.V);
      TexCoords[TexUVCounter++] = V;
    }
    else if(Segment[0] == "VN")
    {
      A3Vertex V;
      strtk::parse(Line[LIndex], " ", ignore, V.NX, V.NY, V.NZ);
      Normals[NormalCounter++] = V;
    }
    else if(Segment[0] == "F")
    {
      unsigned int FaceIndex[4] = {0,0,0,0};
      for(int i = 1; i<SSize; i++)
      {
        unsigned int NormIndex = 0;
        unsigned int UVIndex = 0;
        if(!strtk::parse(Segment[i], "/", FaceIndex[i-1], UVIndex, NormIndex))
          strtk::parse(Segment[i], "/", FaceIndex[i-1], NormIndex);
        if(FaceIndex[i-1] < 1)  //The face index must at least be 1
        {
          cout << Segment[i] << endl;
          unsigned int x = 100;
          return ERR_FILE_FORMAT_WRONG;
        }

        if(NormIndex > 0)
        {
          Mesh->Vertices[FaceIndex[i-1]-1].NX = Normals[NormIndex-1].NX;
          Mesh->Vertices[FaceIndex[i-1]-1].NY = Normals[NormIndex-1].NY;
          Mesh->Vertices[FaceIndex[i-1]-1].NZ = Normals[NormIndex-1].NZ;
        }
        if(UVIndex >0)
        {
          Mesh->Vertices[FaceIndex[i-1]-1].U = TexCoords[UVIndex-1].U;
          Mesh->Vertices[FaceIndex[i-1]-1].V = TexCoords[UVIndex-1].V;
        }
      }

      switch(SSize)
      {
      case 5:
      {
        Mesh->Indices[IndexCounter++] = FaceIndex[2]-1;
        Mesh->Indices[IndexCounter++] = FaceIndex[3]-1;
        Mesh->Indices[IndexCounter++] = FaceIndex[0]-1;
      }
      case 4:
      {
        Mesh->Indices[IndexCounter++] = FaceIndex[2]-1;
        Mesh->Indices[IndexCounter++] = FaceIndex[0]-1;
        Mesh->Indices[IndexCounter++] = FaceIndex[1]-1;
        break;
      }
      }

    }

    else if(Segment[0] == "G" || Segment[0] == "USEMTL" || Segment[0] == "O")
    {

    }
  }
  Mesh->VertexCount = VertexCount;
  Mesh->IndexCount = IndexCount;
  Mesh->Group[0].IndexCount = IndexCount;
  Mesh->Group[0].IndexOffset = 0;
  delete []Normals;
  delete []TexCoords;
  return ERR_NONE;
}
