// *** ADDED BY HEADER FIXUP ***
#include <fstream>
#include <string>
#include <vector>
// *** END ***
#ifndef A3OBJPARSER_H
#define A3OBJARSER_H

#include "A3Engine/Parser/Mesh/A3MeshParser.h"

class A3OBJParser:private A3MeshParser
{
public:
  char Parse(ifstream &File, A3Mesh *Mesh);
protected:
private:
  char Scan(ifstream &File, A3Mesh *Mesh);
  char            *FileBuffer = 0;
  vector <string> Line;
  vector <string> Segment;
};

#endif // A3OBJPARSER_H
