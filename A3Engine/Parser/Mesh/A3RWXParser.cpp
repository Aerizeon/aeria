#include "A3RWXParser.h"
#include <strtk.hpp>
/*
Epsilion:	now i just need to seperate the texture groups and then i can use it for the map xD
Epsilion:	how do you handle textures? each clump can have it's own?
Epsilion:	or do you create a new "group" each time a texture command comes up?
[irc-Edwin]:	there can be multiple materials in a clump so yes you need a new group every time
[irc-Edwin]:	but only if there are vertices in it ;P
[irc-Edwin]:	because you�ll often have texture, surface and color commands together
[irc-Edwin]:	there are also tags
Epsilion:	well, im going to ignore tags and surface for now
Epsilion:	do texture commands always come before a face definition, or do they come before vertex definitions too?
[irc-Edwin]:	before face definition, but they�re also valid before vertex definition as far as I know.
[irc-Edwin]:	but they don�t affect vertices so it doesn�t matter
*/
A3RWXParser::A3RWXParser()
{
  //ctor
}

char A3RWXParser::Scan(ifstream &File, A3Mesh *Mesh)
{
  ///TODO: Make it so that invalid textures/colors within a clump/proto do not override
  ///previously defined VALID textures/colors
  //Get filesize and allocate a buffer to contain the data
  File.seekg (0, ios::end);
  FileSize = File.tellg();
  FileBuffer =  new char[FileSize];
  File.seekg(0,ios::beg);
  File.clear();

  //Read file contents and split it into lines
  File.read(FileBuffer,FileSize);
  strtk::parse(FileBuffer,"\r\n",Line,strtk::split_options::compress_delimiters);
  delete[] FileBuffer;
  FileBuffer = NULL;
  A3RWXState State;
  unsigned int LSize = Line.size();
  A3VertexGroup Group;
  Group.Material.Texture  = NULL;
  Group.Material.Mask     = NULL;
  for(unsigned int LIndex = 0; LIndex < LSize; LIndex++)
  {
    Line[LIndex] = trim(Line[LIndex]);
    if(Line[LIndex] == "")
      continue;
    strtk::convert_to_uppercase(Line[LIndex]);
    ScanLine(State, Line, LIndex, Group, Mesh);
  }
  Mesh->Group.push_back(Group);
  Mesh->GroupCount = Mesh->Group.size();
  return ERR_NONE;
}

char A3RWXParser::ScanLine(A3RWXState& State, vector<string>& RLine, unsigned int& LIndex, A3VertexGroup &Group, A3Mesh* Mesh)
{
  Segment.clear();

  strtk::parse(RLine[LIndex]," \t",Segment, strtk::split_options::compress_delimiters);
  unsigned int SegmentCount = Segment.size();
  if(!State.ModelBegin)
  {
    if(Segment[0] == "MODELBEGIN")
    {
      State.ModelBegin = true;
      State.ClumpBegin = 0;
    }
    //For some reason, some models start with a clump, instead of a modelbegin
    if(Segment[0] == "CLUMPBEGIN")
    {
      State.ModelBegin = true;
      State.ClumpBegin = 0;
      //Rewind the stream so we can parse this as a normal model now.
      LIndex--;
    }
  }
  else
  {
    if(Segment[0] == "CLUMPBEGIN")
    {
      State.ClumpBegin++;
    }
    else if(Segment[0] == "PROTOBEGIN")
    {
      A3RWXPrototype P;
      strtk::parse(RLine[LIndex], " \t", strtk::expect("PROTOBEGIN").ref(), P.Name);
      //Nested protobegins may crash here; refcount and ignore new protos?
      string CLine = trim(RLine[++LIndex]);
      strtk::convert_to_uppercase(CLine);
      while(CLine.substr(0,8) != "PROTOEND")
      {
        if(CLine != "")
          P.Contents.push_back(CLine);
        CLine = trim(RLine[++LIndex]);
        strtk::convert_to_uppercase(CLine);
      }
      State.Prototypes.push_back(P);
      return ERR_NONE;
    }
    else if(State.ClumpBegin > 0)
    {
      if(Segment[0] == "CLUMPEND")
      {
        State.ClumpBegin--;
      }
      else if(Segment[0] == "PROTOINSTANCE")
      {
        for(int p = 0; p < State.Prototypes.size(); p++)
        {
          if(State.Prototypes[p].Name == Segment[1])
          {
            A3RWXPrototype *P = &State.Prototypes[p];
            for(unsigned int c = 0; c < P->Contents.size(); c++)
              ScanLine(State, P->Contents, c, Group, Mesh);
            return ERR_NONE;
          }
        }
      }
      else if(Segment[0] == "VERTEX" || Segment[0] == "VERTEXEXT")
      {
        Mesh->VertexCount++;
      }
      else if(Segment[0] == "TRIANGLE")
      {
        Mesh->IndexCount += 3;
        Group.IndexCount += 3;
      }
      else if(Segment[0] == "QUAD")
      {
        Mesh->IndexCount += 6;
        Group.IndexCount += 6;
      }
      else if(Segment[0] == "POLYGON")
      {
        unsigned short NumPoly = 0;
        strtk::parse(RLine[LIndex], " \t", strtk::expect("POLYGON").ref(), NumPoly);
        if(NumPoly >= 3)
        {
          Mesh->IndexCount += (3*(NumPoly-2));
          Group.IndexCount += (3*(NumPoly-2));
        }
      }
      else if(Segment[0] == "TEXTURE")
      {
        if(Segment[1] != "NULL")
        {
          if(Group.IndexCount > 0)
            Mesh->Group.push_back(Group);
          else
          {
            if(Group.Material.Texture != NULL)
              A3Texture::Destroy(Group.Material.Texture);
            if(Group.Material.Mask != NULL)
              A3Texture::Destroy(Group.Material.Mask);
          }
          Group.Material.Texture = NULL;
          Group.Material.Mask = NULL;
          Group.IndexCount = 0;
          Group.Material.Texture = A3Texture::Create(Segment[1].append(".jpg"));
          if(SegmentCount >= 4 && Segment[2] == "MASK")
          {
            Group.Material.Mask = A3Texture::Create(Segment[3].append(".bmp"), A3Texture_Mask);
            Mesh->HasAlpha = true;
          }
          Group.IndexOffset = Mesh->IndexCount*sizeof(unsigned int);
        }
      }
      else if(Segment[0] == "COLOR")
      {
        float r,g,b = 0;
        strtk::parse(RLine[LIndex], " \t", strtk::expect("COLOR").ref(), r,g,b);
        if(Group.IndexCount > 0)
          Mesh->Group.push_back(Group);
        else
        {
          if(Group.Material.Texture != NULL)
            A3Texture::Destroy(Group.Material.Texture);
          if(Group.Material.Mask != NULL)
            A3Texture::Destroy(Group.Material.Mask);
        }
        Group.Material.Texture = NULL;
        Group.Material.Mask = NULL;
        Group.IndexCount = 0;
        r = std::min(1.0f,r);
        g = std::min(1.0f,g);
        b = std::min(1.0f,b);
        Group.Material.Texture = A3Texture::Create((r*255.0),(g*255.0),(b*255.0));

        Group.IndexOffset = Mesh->IndexCount*sizeof(unsigned int);
      }
    }
  }
}

char A3RWXParser::Parse(ifstream &File, A3Mesh *Mesh)
{
  char rc;
  if((rc = Scan(File,Mesh)) != ERR_NONE )
    return rc;
  if(Mesh->VertexCount < 3)
  {
    cout << "Invalid vertex count in " << Mesh->Name << endl;
    cout << "Vertices: " << Mesh->VertexCount << " Indices: " << Mesh->IndexCount << endl;
    return ERR_FILE_TYPE_WRONG;
  }
  Mesh->Vertices = new A3Vertex[Mesh->VertexCount];
  Mesh->Indices = new unsigned int[Mesh->IndexCount];

  unsigned int LSize = Line.size();
  A3RWXState State;
  for(int LIndex = 0; LIndex < LSize; LIndex++)
  {
    Segment.clear();
    ParseLine(State, Line, LIndex, Mesh);
  }
  return ERR_NONE;
}

char A3RWXParser::ParseLine(A3RWXState &State, vector<string> &RLine, int &LIndex, A3Mesh *Mesh)
{
  Segment.clear();
  RLine[LIndex] = trim(RLine[LIndex]);
  if(RLine[LIndex] == "")
    return ERR_NONE;

  strtk::parse(RLine[LIndex]," \t",Segment, strtk::split_options::compress_delimiters);
  strtk::ignore_token ignore;
  unsigned int SegmentCount = Segment.size();
  if(!State.ModelBegin)
  {
    if(Segment[0] == "MODELBEGIN")
    {
      State.ModelBegin = true;
      State.TransformBegin = 0;
      State.ClumpBegin = 0;
      State.Transform.push(glm::mat4(1.0));
    }
    //For some reason, some models start with a clump, instead of a modelbegin
    if(Segment[0] == "CLUMPBEGIN")
    {
      State.ModelBegin = true;
      State.TransformBegin = 0;
      State.ClumpBegin = 0;
      State.Transform.push(glm::mat4(1.0));
      //Rewind the stream so we can parse this as a normal model now.
      LIndex--;
    }
  }
  else
  {
    if(Segment[0] == "#" || Segment[0] == "TAG" || Segment[0] == "HINTS" || Segment[0] == "OPACITY" || Segment[0] == "GEOMETRYSAMPLING")
    {

    }
    else if(Segment[0] == "MODELEND")
    {
      State.ModelBegin = false;
      State.TransformBegin = 0;
      State.ClumpBegin = 0;
      for(int i = 0; i < State.Transform.size(); i++)
        State.Transform.pop();
      return ERR_NONE;
    }
    else if(Segment[0] == "PROTOBEGIN")
    {
      A3RWXPrototype P;
      strtk::parse(RLine[LIndex], " \t", strtk::expect("PROTOBEGIN").ref(), P.Name);
      //Nested protobegins may crash here; refcount and ignore new protos?
      string CLine = trim(RLine[++LIndex]);
      strtk::convert_to_uppercase(CLine);
      while(CLine.substr(0,8) != "PROTOEND")
      {
        if(CLine != "")
          P.Contents.push_back(CLine);
        CLine = trim(RLine[++LIndex]);
        strtk::convert_to_uppercase(CLine);
      }
      State.Prototypes.push_back(P);
      return ERR_NONE;
    }
    else if(Segment[0] == "CLUMPBEGIN")
    {
      State.ClumpVertexOffset.push(State.VertexCounter);
      State.ClumpBegin++;
      State.TransformBegin++;
      State.Transform.push(State.Transform.top());
    }
    else if(Segment[0] == "TRANSFORMBEGIN")
    {
      State.TransformBegin++;
      State.Transform.push(State.Transform.top());
    }
    else if(Segment[0] == "TRANSFORMEND")
    {
      State.TransformBegin--;
      State.Transform.pop();
    }
    else if(Segment[0] == "IDENTITY" && (State.TransformBegin > 0))
    {
      State.Transform.top() = glm::mat4(1.0);
    }
    else if(Segment[0] == "TRANSFORM" && (State.TransformBegin > 0))
    {
      vector<float> Mat;
      strtk::parse(RLine[LIndex].substr(Segment[0].length()+1)," \t",Mat);
      //Multiply the new value by the previous level's value, and push it on the stack
      State.Transform.top() = (State.Transform.top() * glm::mat4( Mat[0], Mat[1], Mat[2], 0.0f,
                               Mat[4], Mat[5], Mat[6], 0.0f,
                               Mat[8], Mat[9], Mat[10],0.0f,
                               Mat[12],Mat[13],Mat[14],1.0f));
    }
    else if(State.ClumpBegin > 0)
    {
      if(Segment[0] == "CLUMPEND")
      {
        State.ClumpVertexOffset.pop();
        State.TransformBegin--;
        State.Transform.pop();
        State.ClumpBegin--;
      }
      else if(Segment[0] == "PROTOINSTANCE")
      {
        for(int p = 0; p < State.Prototypes.size(); p++)
        {
          if(State.Prototypes[p].Name == Segment[1])
          {
            State.Transform.push(State.Transform.top());
            State.ClumpVertexOffset.push(State.VertexCounter);
            State.TransformBegin++;
            A3RWXPrototype *P = &State.Prototypes[p];
            for(int c = 0; c < P->Contents.size(); c++)
              ParseLine(State, P->Contents, c, Mesh);
            State.ClumpVertexOffset.pop();
            State.TransformBegin--;
            State.Transform.pop();
            return ERR_NONE;
          }
        }
      }
      else if (Segment[0] == "ROTATE" && (State.TransformBegin > 0))
      {
        glm::vec3 Axis;
        float Angle;
        strtk::parse(RLine[LIndex]," \t", strtk::expect("ROTATE").ref(), Axis.x, Axis.y, Axis.z, Angle);
        State.Transform.top() = (State.Transform.top() * glm::toMat4(glm::angleAxis(glm::radians(Angle), Axis)));
      }
      else if(Segment[0] == "TRANSLATE")
      {
        glm::vec3 Translate;
        strtk::parse(RLine[LIndex]," \t", strtk::expect("TRANSLATE").ref(), Translate.x, Translate.y, Translate.z);
        State.Transform.top() = glm::translate(State.Transform.top(),Translate);
      }
      else if(Segment[0] == "SCALE")
      {
        glm::vec3 Scale;
        strtk::parse(RLine[LIndex]," \t", strtk::expect("SCALE").ref(), Scale.x, Scale.y, Scale.z);
        State.Transform.top() = glm::scale(State.Transform.top(), Scale);
      }
      else if(Segment[0] == "VERTEX" || Segment[0] == "VERTEXEXT")
      {
        A3Vertex V;
        glm::vec4 U = glm::vec4(0.0,0.0,0.0,1.0);
        if(!strtk::parse(RLine[LIndex]," \t", ignore, U.x, U.y, U.z, strtk::expect("UV").ref(), V.U, V.V))
        {
          if(!strtk::parse(RLine[LIndex]," \t", ignore, U.x, U.y, U.z))
            cout << "Could not parse vertex on line: " << RLine[LIndex] << endl;
          V.U = 0.0;
          V.V = 0.0;
        }
        U = State.Transform.top() * U;
        V.X = U.x/U.w;
        V.Y = U.y/U.w;
        V.Z = U.z/U.w;
        Mesh->Vertices[State.VertexCounter++] = V;
      }
      else if(Segment[0] == "TRIANGLE")
      {
        unsigned int FaceIndex[3];
        strtk::parse(RLine[LIndex]," \t", strtk::expect("TRIANGLE").ref(), FaceIndex[0], FaceIndex[1], FaceIndex[2]);
        Mesh->Indices[State.IndexCounter++] = FaceIndex[2]+State.ClumpVertexOffset.top()-1;
        Mesh->Indices[State.IndexCounter++] = FaceIndex[0]+State.ClumpVertexOffset.top()-1;
        Mesh->Indices[State.IndexCounter++] = FaceIndex[1]+State.ClumpVertexOffset.top()-1;
      }
      else if(Segment[0] == "QUAD")
      {
        unsigned int FaceIndex[4];
        strtk::parse(RLine[LIndex]," \t", strtk::expect("QUAD").ref(), FaceIndex[0], FaceIndex[1], FaceIndex[2], FaceIndex[3]);
        Mesh->Indices[State.IndexCounter++] = FaceIndex[2]+State.ClumpVertexOffset.top()-1;
        Mesh->Indices[State.IndexCounter++] = FaceIndex[0]+State.ClumpVertexOffset.top()-1;
        Mesh->Indices[State.IndexCounter++] = FaceIndex[1]+State.ClumpVertexOffset.top()-1;
        Mesh->Indices[State.IndexCounter++] = FaceIndex[2]+State.ClumpVertexOffset.top()-1;
        Mesh->Indices[State.IndexCounter++] = FaceIndex[3]+State.ClumpVertexOffset.top()-1;
        Mesh->Indices[State.IndexCounter++] = FaceIndex[0]+State.ClumpVertexOffset.top()-1;
      }
      else if(Segment[0] == "POLYGON")
      {
        unsigned int PolyCount = 0;
        vector<unsigned short> FaceIndex;
        strtk::parse(RLine[LIndex], " \t", strtk::expect("POLYGON").ref(), PolyCount, FaceIndex);
        if(PolyCount < 3 || PolyCount != FaceIndex.size())
          return ERR_NONE;
        for(int p = 1; p < FaceIndex.size()-1; p++)
        {
          Mesh->Indices[State.IndexCounter++] = FaceIndex[0]+State.ClumpVertexOffset.top()-1;
          Mesh->Indices[State.IndexCounter++] = FaceIndex[p]+State.ClumpVertexOffset.top()-1;
          Mesh->Indices[State.IndexCounter++] = FaceIndex[p+1]+State.ClumpVertexOffset.top()-1;
        }
      }
    }
  }
  return 0;
}
A3RWXParser::~A3RWXParser()
{
  //dtor
}
