#ifndef A3RWXPARSER_H
#define A3RWXPARSER_H
#include "A3Include.h"
#include "A3Engine/Parser/Mesh/A3MeshParser.h"
#include <stack>

struct A3RWXPrototype
{
  string Name;
  vector<string> Contents;
};

struct A3RWXState
{
  bool                ModelBegin          = false;
  unsigned int        ClumpBegin          = 0;
  unsigned int        TransformBegin      = 0;
  glm::vec3           Shading             = glm::vec3(0.0);
  glm::vec3           Color               = glm::vec3(0.0);
  stack<glm::mat4>    Transform;
  unsigned int        IndexCounter        = 0;
  unsigned int        VertexCounter       = 0;
  stack<unsigned int> ClumpVertexOffset;
  vector<A3RWXPrototype>  Prototypes;
  string              ProtoName           = "";
  string              TextureMode         = "lit";
};

class A3RWXParser: private A3MeshParser
{
public:
  A3RWXParser();
  char Parse(ifstream &File, A3Mesh *Mesh);
  virtual ~A3RWXParser();
protected:
private:
  char Scan(ifstream &File, A3Mesh *Mesh);
  char ScanLine(A3RWXState &CState, vector<string> &RLine, unsigned int &LIndex, A3VertexGroup &Group, A3Mesh *Mesh);
  char ParseLine(A3RWXState &CState, vector<string> &RLine, int &LIndex, A3Mesh *Mesh);
  char            *FileBuffer = 0;
  vector <string> Line;
  vector <string> Segment;
};

#endif // A3RWXPARSER_H
