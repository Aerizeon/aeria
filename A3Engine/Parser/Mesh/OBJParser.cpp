#include "OBJParser.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

void OBJParser::Scan(ifstream &File, Mesh *MeshData)
{
    MeshFragment *F = new MeshFragment();
    string Line;
    Line.reserve(128);
    while(getline(File,Line))
    {
        boost::trim(Line);
        boost::to_upper(Line);
        vector<string> Token;
        boost::split(Token, Line, boost::is_any_of(" "), boost::token_compress_on );
        int TokenCount = Token.size();
        if(Token[0] == "V")
            MeshData->CVertex++;
        //if(Token[0] == "VT")
          //  MeshData->CTexCoord+=2;
        //if(Token[0] == "VN")
          //  MeshData->CNormal+=2;
        if(Token[0] == "F" && TokenCount == 4)
        {
            MeshData->CIndex+=3;
            F->CIndex+=3;
        }

        if(Token[0] == "F" && TokenCount == 5)
        {
            MeshData->CIndex+=6;
            F->CIndex+=6;
        }
        if(Token[0] == "G" || Token[0] == "USEMTL")
        {
            if(F->CIndex  > 0)
            {
                //If previous fragment was used, push old fragment to stack, and create a new one
                MeshData->Fragment.push_back(F);
                F = new MeshFragment();
                F->IndexOffset = MeshData->CIndex*sizeof(unsigned int);
            }
        }

    }
    File.clear() ;
    File.seekg(0, ios::beg);
    MeshData->Fragment.push_back(F);
    MeshData->CFragment =  MeshData->Fragment.size();
}
void OBJParser::Parse(ifstream &File, Mesh *MeshData)
{
    vector<Material> Materials;
    int MaterialCount = 0;

    int LineCtr =0;
    Scan(File,MeshData);
    int FragCtr = 0;
    MeshFragment *CurrentFragment = MeshData->Fragment[0];
    string Line;
    vector<Vertex> UV;
    vector<Vertex> VN;
    MeshData->Vertices = new Vertex[MeshData->CVertex];
    CurrentFragment->Indices = new unsigned int[CurrentFragment->CIndex];
    int VCounter = 0;
    int ICounter = 0;
    while(getline(File,Line))
    {
        boost::trim(Line);
        cout << Line.length() << endl;
        if(Line.length() > 2)
        {

            boost::to_upper(Line);
            cout << Line << endl;
            vector<string> Token;
            boost::split(Token, Line, boost::is_any_of(" "), boost::token_compress_off );
            unsigned int TokenCount = Token.size();
            if(Token[0] == "#" || Token[0] == " " || Token[0] == "") {}
            else if (Token[0] == "V")
            {

                Vertex V;
                V.X = boost::lexical_cast<float>(Token[1]);
                V.Y = boost::lexical_cast<float>(Token[2]);
                V.Z = boost::lexical_cast<float>(Token[3]);
                V.W = 1.0f;
                float xa = abs(V.X);
                float ya = abs(V.Y);
                float za = abs(V.Z);
                float tmp = (xa > ya?xa:(ya >za?ya:za));
                /*if(tmp > MeshData->BoundingRadius)
                    MeshData->BoundingRadius = tmp;*/
                MeshData->Vertices[VCounter++] = V;
            }
            else if (Token[0] == "VT")
            {
                Vertex V;
                V.U = boost::lexical_cast<float>(Token[1]);
                V.V = boost::lexical_cast<float>(Token[2]);
                if(V.U < 0.0) V.U+=1.0f;
                if(V.V < 0.0) V.V+=1.0f;
                cout << "U: " << V.U << " V: " << V.V << endl;
                UV.push_back(V);
            }
            else if (Token[0] == "VN")
            {
                Vertex V;
                V.NX = boost::lexical_cast<float>(Token[1]);
                if(TokenCount > 2)
                    V.NY = boost::lexical_cast<float>(Token[2]);
                if(TokenCount > 3)
                    V.NZ = boost::lexical_cast<float>(Token[3]);
                VN.push_back(V);
            }
            else if (Token[0] == "F")
            {
                vector<string> Face;
                Face.reserve(128);
                unsigned int Index[4] = {0,0,0,0};
                for(unsigned int i=1; i<TokenCount; i++)
                {
                    Face.clear();
                    boost::split(Face, Token[i], boost::is_any_of("/"), boost::token_compress_off );
                    int FaceCount = Face.size();
                    unsigned int FaceIndex = boost::lexical_cast<unsigned int>(Face[0])-1;
                    Index[i-1] = FaceIndex;
                    switch(FaceCount)
                    {
                    case 3:
                    {
                        unsigned int VNIndex = boost::lexical_cast<unsigned int>(Face[2])-1;
                        Vertex VNL = VN[VNIndex];
                        MeshData->Vertices[FaceIndex].NX = VNL.NX;
                        MeshData->Vertices[FaceIndex].NY = VNL.NY;
                        MeshData->Vertices[FaceIndex].NZ = VNL.NZ;
                    }
                    case 2:
                    {
                        if(Face[1] != "")
                        {
                            unsigned int UVIndex = boost::lexical_cast<unsigned int>(Face[1])-1;
                            Vertex UVL = UV[UVIndex];
                            MeshData->Vertices[FaceIndex].U = UVL.U;
                            MeshData->Vertices[FaceIndex].V = UVL.V;
                        }
                        break;

                    }
                    default:
                    {
                        break;
                        //cout << "Error: Unknown face definition on " << MeshData->Name << endl;
                    }
                    }
                }
                CurrentFragment->Indices[ICounter++] = Index[2];
                CurrentFragment->Indices[ICounter++] = Index[0];
                CurrentFragment->Indices[ICounter++] = Index[1];
                if(TokenCount == 5)
                {
                    //This is a quad face, which must be converted to a triangle
                    CurrentFragment->Indices[ICounter++] = Index[2];
                    CurrentFragment->Indices[ICounter++] = Index[3];
                    CurrentFragment->Indices[ICounter++] = Index[0];
                }
            }
            else if (Token[0] == "MTLLIB")
            {
                ifstream MTLFile;
                string MTLPath = Settings::CachePath + "models/" + Token[1];
                MTLFile.open(MTLPath,ifstream::in);
                if(!MTLFile)
                {
                    cout << "Could not open material " << MTLPath << endl;
                }
                else
                {
                    string MTLLine;
                    while(getline(MTLFile,MTLLine))
                    {
                        boost::trim(MTLLine);
                        boost::to_upper(MTLLine);
                        vector<string> MTLToken;
                        boost::split(MTLToken, MTLLine, boost::is_any_of(" "), boost::token_compress_on );
                        int MTLTokenCount = MTLToken.size();
                        if(MTLToken[0] == "#" || Token[0] == " " || Token[0] == "") {}
                        else if (MTLToken[0] == "NEWMTL")
                        {
                            MaterialCount++;
                            Material M;
                            M.Name = MTLToken[1];
                            Materials.push_back(M);
                        }
                        else if (MTLToken[0] == "MAP_KD")
                        {
                            if(MaterialCount > 0)
                                Materials[MaterialCount-1].TextureDiffuse = MTLToken[1];
                        }
                    }
                }
                MTLFile.close();
            }
            else if (Token[0] == "G")
            {
                if(ICounter > 0)
                {
                    FragCtr++;
                    ICounter = 0;
                    CurrentFragment = MeshData->Fragment[FragCtr];
                    CurrentFragment->Indices = new unsigned int[CurrentFragment->CIndex];
                }
            }
            else if (Token[0] == "USEMTL")
            {
                if(ICounter > 0)
                {
                    FragCtr++;
                    ICounter = 0;
                    CurrentFragment = MeshData->Fragment[FragCtr];
                    CurrentFragment->Indices = new unsigned int[CurrentFragment->CIndex];
                }
                //Switching active material
                for(int mat = 0; mat<MaterialCount; mat++)
                {
                    if(Materials[mat].Name == Token[1])
                    {
                        cout << "Assigning texture " << Materials[mat].TextureDiffuse << " to " << MeshData->Name << endl;
                        CurrentFragment->TextureData = Texture::Create(Materials[mat].TextureDiffuse);
                    }
                }
            }
            else
            {
                // cout << "Unknown token " << Token[0] << endl;
            }
            //cout << "Line:" << ++LineCtr << endl;
        }
    }
}
