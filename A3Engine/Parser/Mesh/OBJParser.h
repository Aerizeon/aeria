#ifndef WAVEFRONT_H
#define WAVEFRONT_H

#include "A3Engine/Data/Mesh.h"
#include "A3Engine/Settings.h"

class OBJParser
{
public:
    static void Scan(ifstream &File, A3Mesh *Mesh);
    static void Parse(ifstream &File, A3Mesh *Mesh);
protected:
private:
};

#endif // WAVEFRONT_H
