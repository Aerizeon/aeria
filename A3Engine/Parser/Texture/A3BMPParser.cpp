#include "A3BMPParser.h"

#include <iostream>
#include <fstream>
#include "A3Include.h"

/*
    ERR_NONE                = 0,
    ERR_FILE_TYPE_WRONG     = 1,
    ERR_FILE_PATH_WRONG     = 2,
    ERR_FILE_FORMAT_WRONG   = 4,
    ERR_FILE_LENGTH_WRONG   = 8,
    ERR_FILE_DATA_WRONG     = 16
    */

char A3BMPParser::Parse(ifstream &File,A3Texture *TexData)
{
  BMPFileHeader   Header;
  BMPInfoHeader   Info;
  unsigned int    RowSize   = 0;
  unsigned int    Index     = 0;
  size_t          ColorSize = sizeof(BMPColor);

  if(!File.read((char*)&Header,sizeof(BMPFileHeader)))
    return ERR_FILE_LENGTH_WRONG;

  if(Header.Type != 19778)
    return ERR_FILE_TYPE_WRONG;

  if(!File.read((char*)&Info,sizeof(BMPInfoHeader)))
    return ERR_FILE_LENGTH_WRONG;

  if(Info.Compression != 0)
    return ERR_FILE_FORMAT_WRONG;

  if(Info.Bits != 1 && Info.Bits != 8 && Info.Bits != 24)
    return ERR_FILE_FORMAT_WRONG;

  if(Info.Colors == 0 && Info.Bits <= 8)
    Info.Colors = pow(2,Info.Bits);

  BMPColor Palette[Info.Colors];

  //Move to the start of the palette data
  File.seekg(Info.HeaderSize + sizeof(BMPFileHeader));

  if(Info.Colors > 0)
    File.read((char*)&Palette, Info.Colors * sizeof(BMPColor));

  //Move to start of image data
  File.seekg(Header.ImageOffset);
  RowSize = ((Info.Width * Info.Bits / 8) + 3) & ~3;

  TexData->Channels = 3;
  TexData->Size     = glm::vec2(Info.Width, Info.Height);
  TexData->Data     = new char[Info.Width * Info.Height * TexData->Channels];
  Index             = Info.Width * Info.Height * TexData->Channels;

  for(unsigned int Y = 0; Y < Info.Height; Y++)
  {
    char BMPRow[RowSize];
    if(!File.read((char*)&BMPRow, RowSize))
    {
      delete []TexData->Data;
      return ERR_FILE_LENGTH_WRONG;
    }

    if(Info.Bits == 1)
    {
      for(unsigned int X = Info.Width/8; X > 0; X--)
      {
        for(int bit = 0; bit < 8; bit++)
        {
          BMPColor Color = Palette[(BMPRow[X] & (1 << bit)) !=0];
          TexData->Data[--Index] = Color.Red;
          TexData->Data[--Index] = Color.Green;
          TexData->Data[--Index] = Color.Blue;
        }
      }
    }
    else if(Info.Bits == 2)
    {
      for(unsigned int X = 0; X < Info.Width; X++)
      {

      }
    }
    else if(Info.Bits == 4)
    {
      for(unsigned int X = 0; X < Info.Width; X++)
      {

      }
    }
    else if(Info.Bits == 8)
    {
      for(unsigned int X = Info.Width; X > 0; X--)
      {
        //This is supposed to reference the palette, however
        //I cannot get it to work unless I take the direct value
        //and treat it as grayscale - there should be some way to
        //detect 8-bit grayscale vs 8-bit palette
        BMPColor Color = Palette[(unsigned char)BMPRow[X]];
        TexData->Data[--Index] = Color.Red;
        TexData->Data[--Index] = Color.Green;
        TexData->Data[--Index] = Color.Blue;
      }
    }
    else if(Info.Bits == 16)
    {
      for(unsigned int X = 0; X < Info.Width; X++)
      {


      }
    }
    else if(Info.Bits == 24)
    {
      for(unsigned int X = 0; X < Info.Width; X++)
      {
        BMPColor Color = *(BMPColor*)(&BMPRow+X);
        TexData->Data[--Index] = Color.Red;
        TexData->Data[--Index] = Color.Green;
        TexData->Data[--Index] = Color.Blue;
      }
    }
    else if(Info.Bits == 32)
    {
      for(unsigned int X = 0; X < Info.Width; X++)
      {

      }
    }
    else if(Info.Bits == 64)
    {
      for(unsigned int X = 0; X < Info.Width; X++)
      {

      }
    }
  }

  /* unsigned int NPixel = 0;
   if(bmih.biBitCount == 1)
   {
     cout << "Palette Colors: " << NumColors << endl;
     for(unsigned int y = bmih.biHeight; y > 0 ; y--)
     {
       for(unsigned int x = 0; x < bmih.biWidth/8; x++)
       {
       for(int bit = 0; bit < 8; bit++)
       {
         TexData->Data[NPixel++] = ((Tmp[(y*(bmih.biWidth/8))+x] & (1<<bit)) !=0)?Palette[1].rgbRed:Palette[0].rgbRed;
         TexData->Data[NPixel++] = ((Tmp[(y*(bmih.biWidth/8))+x] & (1<<bit)) !=0)?Palette[1].rgbGreen:Palette[0].rgbGreen;
         TexData->Data[NPixel++] = ((Tmp[(y*(bmih.biWidth/8))+x] & (1<<bit)) !=0)?Palette[1].rgbBlue:Palette[0].rgbBlue;
       }
     }
     }
   }
   else if(bmih.biBitCount == 8)
   {
     for(unsigned int y = bmih.biHeight; y > 0 ; y--)
     {
       for(unsigned int x = 0; x < bmih.biWidth; x++)
       {
         TexData->Data[NPixel++] = Tmp[(y*bmih.biWidth)+x];
         TexData->Data[NPixel++] = Tmp[(y*bmih.biWidth)+x];
         TexData->Data[NPixel++] = Tmp[(y*bmih.biWidth)+x];
       }
     }
   }
   else if(bmih.biBitCount == 24)
   {
    for(unsigned int y = bmih.biHeight; y > 0 ; y--)
     {
       for(unsigned int x = 0; x < bmih.biWidth; x++)
       {
       TexData->Data[(NPixel)+2] = Tmp[(y*bmih.biWidth)+x+0];
       TexData->Data[(NPixel)+1] = Tmp[(y*bmih.biWidth)+x+1];
       TexData->Data[(NPixel)+0] = Tmp[(y*bmih.biWidth)+x+2];
       NPixel++;
     }
     }
   }
   TexData->Size.x = bmih.biWidth;
   TexData->Size.y = bmih.biHeight;*/
  TexData->State = ST_LOADED;
  return ERR_NONE;
}
