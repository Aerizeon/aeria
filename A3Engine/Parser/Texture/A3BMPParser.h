#ifndef BMPPARSER_H
#define BMPPARSER_H

#include <istream>

#include "A3Engine/Data/A3Texture.h"

using namespace std;

#ifndef _WIN32
typedef unsigned char   BYTE;
typedef unsigned short  WORD;
typedef unsigned long   DWORD;
typedef signed long     LONG;
#endif
#pragma pack(push, 1)
struct BMPFileHeader
{
  WORD    Type;
  DWORD   Size;
  WORD	  Reserved1;
  WORD	  Reserved2;
  DWORD   ImageOffset;
};

struct BMPInfoHeader
{
  DWORD   HeaderSize;
  LONG    Width;
  LONG    Height;
  WORD    Planes;
  WORD    Bits;
  DWORD   Compression;
  DWORD   ImageSize;
  LONG    XPixelsPerMeter;
  LONG    YPixelsPerMeter;
  DWORD   Colors;
  DWORD   ImportantColors;
};

//Colour palette
struct BMPColor
{
  BYTE	Blue;
  BYTE	Green;
  BYTE	Red;
  BYTE	Alpha;
};
#pragma pack(pop)

class A3BMPParser
{
public:
  A3BMPParser();
  static char Parse(ifstream &File, A3Texture *TexData);
  virtual ~A3BMPParser();
protected:
private:
};

#endif // BMPPARSER_H
