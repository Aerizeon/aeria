#include "A3JPGParser.h"

bool A3JPGParser::Test(ifstream &File)
{
  char iHeader[3];
  char cHeader[3] = {0xFF, 0xD8, 0xFF};
  File.read(iHeader, 3);
  for(int i = 2; i >= 0; i--)
  {
    if(iHeader[i] != cHeader[i])
    {
      File.seekg(0, ios::beg);
      return false;
    }
  }
  File.seekg(0, ios::beg);
  return true;
}

char A3JPGParser::Parse(ifstream &File, A3Texture *TexData)
{
  jpeg_decompress_struct Image;
  ErrorHandler Error;
  Image.err = jpeg_std_error(&Error.pub);
  Error.pub.error_exit = A3JPGParser::Error_Exit;
  JSAMPARRAY ScanLine = NULL;
  unsigned int Stride = 0;

  jpeg_create_decompress(&Image);
  A3JPGParser::Set_Src(&Image, &File);
  if(jpeg_read_header(&Image, TRUE) != JPEG_HEADER_OK)
  {
    jpeg_destroy_decompress(&Image);
    return ERR_FILE_FORMAT_WRONG;
  }

  jpeg_start_decompress(&Image);
  TexData->Size = glm::vec2(Image.output_width,Image.output_height);
  //Force output to RGBA
  TexData->Channels = 4;

  if (setjmp(Error.setjmp_buffer))
  {
    jpeg_destroy_decompress(&Image);
    return ERR_FILE_FORMAT_WRONG;
  }

  if(Image.output_components != 3)
  {
    jpeg_destroy_decompress(&Image);
    return ERR_FILE_DATA_WRONG;
  }

  Stride = Image.output_width * Image.output_components;
  //Forcing output to RGBA
  TexData->Data = new char[Image.output_height * Image.output_width * 4];
  ScanLine = (*Image.mem->alloc_sarray) ((j_common_ptr) &Image, JPOOL_IMAGE, Stride, 1);
  if(!ScanLine)
  {
    jpeg_destroy_decompress(&Image);
    delete []TexData->Data;
    TexData->Data = NULL;
    return ERR_FILE_DATA_WRONG;
  }
  unsigned int OPos = 0;
  while(Image.output_scanline < Image.output_height)
  {
    unsigned int SPos = Image.output_scanline * Stride;
    jpeg_read_scanlines(&Image, ScanLine,1);
    for(int i = 0; i < Stride/3; i++)
    {
      TexData->Data[OPos++] = ScanLine[0][i*3+0];
      TexData->Data[OPos++] = ScanLine[0][i*3+1];
      TexData->Data[OPos++] = ScanLine[0][i*3+2];
      TexData->Data[OPos++] = 255;
    }
  }
  jpeg_finish_decompress(&Image);
  jpeg_destroy_decompress(&Image);
  return ERR_NONE;
}

char A3JPGParser::Write(ofstream& File, A3Texture* TexData)
{
  if(TexData->Channels != 3)
  {
    cout << "Cannot write JPG file: Wrong number of channels (" << TexData->Channels << ")" << endl;
    return ERR_FILE_FORMAT_WRONG;
  }

  jpeg_compress_struct Image;
  ErrorHandler Error;
  Image.err = jpeg_std_error(&Error.pub);
  Error.pub.error_exit = A3JPGParser::Error_Exit;
  jpeg_create_compress(&Image);
  A3JPGParser::Set_Dst(&Image, &File);
  Image.image_width  = TexData->Size.x;
  Image.image_height = TexData->Size.y;
  Image.input_components = 3;
  Image.in_color_space = JCS_RGB;
  jpeg_set_defaults(&Image);
  jpeg_set_quality(&Image, 100, TRUE);
  jpeg_start_compress(&Image, TRUE);
  if (setjmp(Error.setjmp_buffer))
  {
    jpeg_destroy_compress(&Image);
    return ERR_FILE_FORMAT_WRONG;
  }
  JSAMPROW Row = 0;
  while(Image.next_scanline < Image.image_height)
  {
    Row = (JSAMPROW)(((char*)TexData->Data) + Image.next_scanline * (TexData->Size.x * 3));
    jpeg_write_scanlines(&Image, &Row, 1);
  }
  jpeg_finish_compress(&Image);
  File.close();
  jpeg_destroy_compress(&Image);
  return ERR_NONE;
}

void A3JPGParser::Set_Src  (j_decompress_ptr Image, std::istream *in)
{
  JPGIStream *Stream = 0;
  if(Image->src == NULL)
  {
    Image->src = (struct jpeg_source_mgr *)(*Image->mem->alloc_small)((j_common_ptr) Image, JPOOL_PERMANENT, sizeof(JPGIStream));
  }
  Stream = reinterpret_cast<JPGIStream*> (Image->src);
  Stream->File = in;
  Stream->Buffer[10] = 0;
  Stream->pub.init_source = A3JPGParser::Init_Src;
  Stream->pub.fill_input_buffer = A3JPGParser::Fill_Buffer;
  Stream->pub.skip_input_data = A3JPGParser::Skip;
  Stream->pub.resync_to_restart = jpeg_resync_to_restart; /* use default method */
  Stream->pub.term_source = A3JPGParser::Term_Src;
  Stream->pub.bytes_in_buffer = 0; /* forces fill_input_buffer on first read */
  Stream->pub.next_input_byte = NULL; /* until buffer loaded */
}

void A3JPGParser::Set_Dst(j_compress_ptr Image, std::ostream* out)
{
  JPGOStream *Stream = 0;
  if(Image->dest == NULL)
  {
    Image->dest = (struct jpeg_destination_mgr *)(*Image->mem->alloc_small)((j_common_ptr) Image, JPOOL_PERMANENT, sizeof(JPGOStream));
  }
  Stream = reinterpret_cast<JPGOStream*> (Image->dest);
  Stream->File = out;
  Stream->pub.init_destination    = A3JPGParser::Init_Dst;
  Stream->pub.empty_output_buffer = A3JPGParser::Empty_Buffer;
  Stream->pub.term_destination    = A3JPGParser::Term_Dst;
}

void A3JPGParser::Init_Src (j_decompress_ptr Image)
{
  //This is only necessary if something happens to the file
  //between test and parse, that doesnt reset the image.
  //JPGIStream *Stream = (JPGIStream*)Image->src;
  //Stream->File->seekg(0, ios::beg);
}

void A3JPGParser::Init_Dst(j_compress_ptr Image)
{
  JPGOStream *Stream = (JPGOStream*)Image->dest;
  Stream->pub.next_output_byte    = (JOCTET*)Stream->Buffer;
  Stream->pub.free_in_buffer      = JPEG_BUF_SIZE;
}

boolean A3JPGParser::Fill_Buffer (j_decompress_ptr Image)
{
  JPGIStream *Stream = (JPGIStream*)Image->src;
  Stream->File->read(Stream->Buffer, JPEG_BUF_SIZE);
  size_t Read = Stream->File->gcount();
  if(Read == 0)
  {
    Stream->Buffer[0] = 0xFF;
    Stream->Buffer[1] = JPEG_EOI;
    Read = 2;
  }
  Stream->pub.next_input_byte = (CONST JOCTET*)Stream->Buffer;
  Stream->pub.bytes_in_buffer = Read;
  return TRUE;
}

boolean A3JPGParser::Empty_Buffer (j_compress_ptr Image)
{
  JPGOStream *Stream = (JPGOStream*)Image->dest;
  Stream->File->write(Stream->Buffer, JPEG_BUF_SIZE);
  Stream->pub.next_output_byte = (JOCTET*)Stream->Buffer;
  Stream->pub.free_in_buffer = JPEG_BUF_SIZE;
  return TRUE;
}

void A3JPGParser::Skip        (j_decompress_ptr Image, long Count)
{
  if(Count > 0)
  {
    JPGIStream *Stream = (JPGIStream*)Image->src;
    while (Count > (long)Stream->pub.bytes_in_buffer)
    {
      Count -= (long)Stream->pub.bytes_in_buffer;
      A3JPGParser::Fill_Buffer(Image);
    }
    Stream->pub.next_input_byte += Count;
    Stream->pub.bytes_in_buffer -= Count;
  }
}

void A3JPGParser::Term_Src        (j_decompress_ptr Image)
{
  JPGIStream *Stream = (JPGIStream*)Image->src;
  Stream->File->clear();
}

void A3JPGParser::Term_Dst(j_compress_ptr Image)
{
  JPGOStream *Stream = (JPGOStream*)Image->dest;
  size_t DCount = JPEG_BUF_SIZE - Stream->pub.free_in_buffer;
  if(DCount > 0)
  {
    Stream->File->write(Stream->Buffer,DCount);
  }
  Stream->File->flush();
}

void A3JPGParser::Error_Exit(j_common_ptr Image)
{
  cout << "ERROR READING OR WRITING JPEG IMAGE-----------" << endl;
  ErrorHandler *Err = (ErrorHandler*)Image->err;
  (*Image->err->output_message) (Image);
  longjmp(Err->setjmp_buffer,1);
}
