#ifndef A3JPGPARSER_H
#define A3JPGPARSER_H

#include <rpc.h>
#include <cstdio>
#include <basetsd.h>
#define HAVE_BOOLEAN 1
#include <jpeglib.h>

#include <csetjmp>
#include "A3Engine/Data/A3Texture.h"

const static size_t JPEG_BUF_SIZE = 16384;

struct ErrorHandler
{
  struct jpeg_error_mgr pub;
  jmp_buf setjmp_buffer;
};

struct JPGIStream
{
  jpeg_source_mgr pub;
  std::istream* File;
  char Buffer [JPEG_BUF_SIZE];
};

struct JPGOStream
{
  jpeg_destination_mgr pub;
  std::ostream* File;
  char Buffer [JPEG_BUF_SIZE];
};

class A3JPGParser
{
public:
  static bool Test(ifstream &File);
  static char Parse(ifstream &File, A3Texture *TexData);
  static char Write(ofstream &File, A3Texture *TexData);
protected:
private:
  static void Set_Src  (j_decompress_ptr Image, std::istream *in);
  static void Set_Dst   (j_compress_ptr Image, std::ostream *out);
  static void Init_Src (j_decompress_ptr Image);
  static void Init_Dst   (j_compress_ptr Image);
  static boolean Fill_Buffer (j_decompress_ptr Image);
  static boolean Empty_Buffer (j_compress_ptr Image);
  static void Skip        (j_decompress_ptr Image, long Count);
  static void Term_Src       (j_decompress_ptr Image);
  static void Term_Dst       (j_compress_ptr Image);
  static void Error_Exit  (j_common_ptr Image);
};

#endif // A3JPGPARSER_H
