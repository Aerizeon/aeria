#include "A3PNGParser.h"

bool A3PNGParser::Test(ifstream &File)
{
  png_byte PNGMagic[PNGHEADERSIZE];
  File.read((char*)PNGMagic,PNGHEADERSIZE);

  if(!png_sig_cmp(PNGMagic, 0, PNGHEADERSIZE))
  {
    return true;
  }
  File.seekg(0, ios::beg);
  return false;

}
char A3PNGParser::Parse(ifstream &File, A3Texture *TexData)
{
  png_structp pngPtr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

  if (!pngPtr)
    return ERR_FILE_FORMAT_WRONG;

  png_infop infoPtr = png_create_info_struct(pngPtr);

  if (!infoPtr)
  {
    png_destroy_read_struct(&pngPtr, (png_infopp)0, (png_infopp)0);
    return ERR_FILE_FORMAT_WRONG;
  }
  png_bytep* rowPtrs = NULL;

  if (setjmp(png_jmpbuf(pngPtr)))
  {
    png_destroy_read_struct(&pngPtr, &infoPtr,(png_infopp)0);
    if (rowPtrs != NULL) {
      delete [] rowPtrs;
      rowPtrs = NULL;
    }
    if ( TexData->Data != NULL) {
      delete []TexData->Data;
      TexData->Data = NULL;
    }
    return ERR_FILE_FORMAT_WRONG;
  }

  png_set_read_fn(pngPtr,(png_voidp)&File, PNGReadFromStream);
  png_set_sig_bytes(pngPtr, PNGHEADERSIZE);
  png_read_info(pngPtr, infoPtr);
  png_uint_32 imgWidth =  png_get_image_width(pngPtr, infoPtr);
  TexData->Size.x = imgWidth;
  png_uint_32 imgHeight = png_get_image_height(pngPtr, infoPtr);
  TexData->Size.y = imgHeight;
  png_uint_32 bitdepth   = png_get_bit_depth(pngPtr, infoPtr);
  TexData->Channels = png_get_channels(pngPtr, infoPtr);
  png_uint_32 color_type = png_get_color_type(pngPtr, infoPtr);
  switch (color_type)
  {
  case PNG_COLOR_TYPE_PALETTE:
    png_set_palette_to_rgb(pngPtr);
    TexData->Channels = 3;
    break;
  case PNG_COLOR_TYPE_GRAY:
    if (bitdepth < 8)
      png_set_expand_gray_1_2_4_to_8(pngPtr);
    bitdepth = 8;
    break;
  }
  if (png_get_valid(pngPtr, infoPtr, PNG_INFO_tRNS))
  {
    png_set_tRNS_to_alpha(pngPtr);
    TexData->Channels+=1;
  }
  //Array of row pointers. One for every row.
  rowPtrs = new png_bytep[imgHeight];
  TexData->Data = new char[imgWidth * imgHeight * bitdepth * TexData->Channels / 8];
  //This is the length in bytes, of one row.
  const unsigned int stride = imgWidth * bitdepth * TexData->Channels/ 8;

  //set all the row pointers to the starting

  //Adresses for every row in the buffer

  for (size_t i = 0; i < imgHeight; i++)
  {
    //Set the pointer to the data pointer + i times the row stride
    png_uint_32 q = ( i ) * stride;
    rowPtrs[i] = (png_bytep) TexData->Data + q;
  }
  png_read_image(pngPtr, rowPtrs);
  delete[] (png_bytep)rowPtrs;
  rowPtrs = NULL;
  png_destroy_read_struct(&pngPtr, &infoPtr,(png_infopp)0);
  TexData->State = ST_LOADED;
  return ERR_NONE;
}

void A3PNGParser::PNGReadFromStream(png_structp pngPtr, png_bytep data, png_size_t length)
{
  png_voidp File = png_get_io_ptr(pngPtr);
  ((ifstream*)File)->read((char*)data, length);
}
