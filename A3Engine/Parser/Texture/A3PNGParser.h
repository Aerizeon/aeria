// *** ADDED BY HEADER FIXUP ***
#include <fstream>
// *** END ***
#ifndef PNGPARSER_H
#define PNGPARSER_H
#define PNGHEADERSIZE 8

#include "A3Engine/Data/A3Texture.h"
#include <png.h>

class A3PNGParser
{
public:
  static bool Test(ifstream &File);
  static char Parse(ifstream &File, A3Texture *TexData);
protected:
private:
  static void PNGReadFromStream(png_structp pngPtr, png_bytep data, png_size_t length);
};

#endif // PNGPARSER_H
