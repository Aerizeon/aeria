#ifndef BOUNDINGVOLUME_H
#define BOUNDINGVOLUME_H
#include <A3Engine/Structure/A3Vector.h>
struct Ray
{
    Origin;
    Vector3f Direction;
};

enum BoundingType
{
    BoundingSphere,
    BoundingCube,
    BoundingRectangle,
    BoundingHull
};

class BoundingVolume
{
public:
    BoundingType    Type;
    Vector3f        Origin;
    float Height;

    BoundingVolume();
    float Collision(Ray r);
    virtual ~BoundingVolume();
protected:
private:
    float CheckSphere(Ray r);
    float CheckCube(Ray r);
    float CheckRectangle(Ray r);
    float CheckHull(Ray r);
};

#endif // BOUNDINGVOLUME_H
