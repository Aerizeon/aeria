// Timer.h: Timer class used for determining elapsed time and
//              frames per second.
//
//////////////////////////////////////////////////////////////////////

#ifndef TIMER_H
#define TIMER_H


//////////////////////////////////////////////////////////////////////
// INCLUDES
//////////////////////////////////////////////////////////////////////
#ifdef WIN32
#include <windows.h>
#else
#include <sys/time.h>
#include <stdlib.h>
#endif
#include <stdio.h>
#include <math.h>

class CTimer
{
private:
#ifdef _WIN32
  long long int       StartTime;
  double       		Frequency;
  double              Resolution;
#else
  timeval StartTime;
#endif

public:


  CTimer( void )
  {
  }

  virtual ~CTimer( void )
  {
  }

  void Init( void )
  {
#ifdef _WIN32
    LARGE_INTEGER LI;
    QueryPerformanceFrequency(&LI);
    Frequency = LI.QuadPart;
    Resolution = 1.0/Frequency;
    QueryPerformanceCounter(&LI);
    StartTime = LI.QuadPart;
#else
    gettimeofday(&StartTime,NULL);
#endif
  }

  double GetTime( void )
  {
#ifdef _WIN32
    LARGE_INTEGER EndTime;
    QueryPerformanceCounter(&EndTime);
    return (EndTime.QuadPart - StartTime )/Frequency;
#else
    timeval EndTime;
    gettimeofday(&EndTime, NULL);

    long seconds  = EndTime.tv_sec  - StartTime.tv_sec;
    long useconds = EndTime.tv_usec - StartTime.tv_usec;

    long mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
    return mtime;
#endif

  }
};

#endif // TIMER_H
