#include "A3Vector.h"
#include <math.h>
#include <iostream>

A3Region2f::A3Region2f(glm::vec2 RPosition, glm::vec2 RSize)
  :Position(RPosition), Size(RSize)
{
}

bool A3Region2f::Test(glm::vec2 Point)
{
  return (Point.x > Position.x && Point.y > Position.y && Point.x < (Position.x+Size.x) && Point.y < (Position.y + Size.y));
}

