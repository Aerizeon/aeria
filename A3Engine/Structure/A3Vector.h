#ifndef VECTOR_H
#define VECTOR_H

#define RADIANS 3.14159265358979323846/180
#define DEGREES 180/3.14159265358979323846
#define HALFRADIANS 3.14159265358979323846/360.0
#include <glm/glm.hpp>


/*struct A3Projection3D
{
    A3Vector3f X;
    A3Vector3f Y;
    A3Vector3f Z;
};*/

#pragma pack(push)  /* push current alignment to stack */
#pragma pack(1)     /* set alignment to 1 byte boundary */
struct A3Vertex
{
  float X   = 0;
  float Y   = 0;
  float Z   = 0;
  float NX  = 0;
  float NY  = 0;
  float NZ  = 0;
  float U   = 0;
  float V   = 0;
  A3Vertex(float x = 0, float y = 0, float z = 0, float nx = 0, float ny = 0, float nz = 0, float u = 0, float v = 0)
    :X(x),Y(y),Z(z),NX(nx),NY(ny),NZ(nz),U(u),V(v)
  {

  }
};
#pragma pack(pop)   /* restore original alignment from stack */


class A3Region2f
{
public:
  glm::vec2 Position;
  glm::vec2 Size;
  A3Region2f(glm::vec2 RPosition = glm::vec2(), glm::vec2 RSize = glm::vec2());
  bool Test (glm::vec2 Point);
};

class A3Rect2f
{
public:
};
#endif // VECTORS_H
