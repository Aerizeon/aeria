#include "A3Include.h"

SQInteger SquirrelFileReader(SQUserPointer File)
{
  std::istream* in = reinterpret_cast<std::ifstream*> (File);
  char c = in->get();
  if(in->eof())
    return 0;
  return c;
}

void SquirrelPrintTable(Sqrat::Object t)
{
  HSQUIRRELVM &vm = t.GetVM();
  const SQChar *name = 0;
  stringstream Title;

  if(ArrayTable.get_num_columns() == 0)
  {
    ArrayTable.AddColumn("Key Type",13);
    ArrayTable.AddColumn("Key ",25);
    ArrayTable.AddColumn("Value Type",13);
    ArrayTable.AddColumn("Value",80);
  }
  sq_getstring(vm,-2,&name);
  if(name)
    cout << "TAB:" << name << endl;
  if((name = sq_getlocal(vm,1,1)))
  {
    switch(sq_gettype(vm,1))
    {
    case OT_ARRAY:
      Title << "[Array] ";
      break;
    case OT_TABLE:
      Title << "[Table] ";
      break;
    case OT_CLASS:
      Title << "[Class] ";
      break;
    default:
      cout << "Unknown Type " << sq_gettype(vm,1) << endl;
      return;
    }
    Title << name;
    ArrayTable.set_title(Title.str());
    ArrayTable.PrintHeader();
    sq_push(vm,2);
    sq_pushnull(vm);  //null iterator
    while(SQ_SUCCEEDED(sq_next(vm,-2)))
    {
      SqData Key = SquirrelGetData(vm,-2,OP_STRING_QUOTE);
      SqData Val = SquirrelGetData(vm,-1,OP_STRING_QUOTE);
      if(Key.Value.size() > 23)
        Key.Value = Key.Value.substr(0,20).append("...");
      if(Val.Value.size() > 78)
        Val.Value = Val.Value.substr(0,75).append("...");
      ArrayTable << Key.Type << Key.Value << Val.Type << Val.Value;
      sq_pop(vm,2); //pops key and val before the nex iteration
    }
    sq_pop(vm,2); //pops the null iterator and table
    ArrayTable.PrintFooter();
  }
  else
  {
    cout << "Unable to print variable of type " << sq_gettype(vm,1) << endl;
  }
}

SQInteger SquirrelRuntimeError(HSQUIRRELVM vm)
{
  SQStackInfos si;
  SQInteger level=0; //0 -> This, 1 -> Next
  stringstream Title;
  const SQChar *error_message = NULL;
  if(sq_gettop(vm) >= 1)
  {
    if(SQ_SUCCEEDED(sq_getstring(vm,2,&error_message)))
      cout << "SQCRASH: " << error_message  << endl << "Printing Stack..." << endl;

    while(SQ_SUCCEEDED(sq_stackinfos(vm,level,&si)))
    {
      Title.str(std::string());
      Title << "[" << si.source << ":" << si.line << "] " << si.funcname << "()";
      StackTable.set_highlight(0);
      StackTable.set_title(Title.str());
      SquirrelPrintLocals(vm,level);
      level++;
    }
    cout << "Stack dump complete!" << endl;
  }
  return 0;
}

void SquirrelDebug(HSQUIRRELVM vm, SQInteger Type, const SQChar *Source, SQInteger Line, const SQChar *Function)
{
  string fname    = Function?Function:"<null>";
  string srcfile  = Source?Source:"<null>";
  stringstream Title;
  Title << "[" << (Source?Source:"<NULL>") << ":" << Line << "]  " <<(Type == 'l'?"::":(Type=='c'?"->":"<-")) << (Function?Function:"<NULL>") << "()";
  if(Type == 'r')
  {
    StackTable.set_title(Title.str());
    //StackTable.set_highlight(0);
    SquirrelPrintLocals(vm,0);
  }
  else
    cout << Title.str() << endl;
}

void SquirrelPrintLocals(HSQUIRRELVM vm,int level)
{
  const SQChar *name    = 0;
  unsigned int Sequence = 0;

  if(StackTable.get_num_columns() == 0)
  {
    StackTable.AddColumn("Seq",6);
    StackTable.AddColumn("Type",13);
    StackTable.AddColumn("Name",25);
    StackTable.AddColumn("Value",80);
  }
  StackTable.set_indent("   ",level);
  StackTable.PrintHeader();
  while((name = sq_getlocal(vm,level,Sequence)))
  {
    SqData Dat = SquirrelGetData(vm,-1,OP_STRING_QUOTE);
    if(Dat.Value.length() > 78)
      Dat.Value = Dat.Value.substr(0,80-4).append("...");
    StackTable << Sequence << Dat.Type << name << Dat.Value;
    Sequence++;
  }
  sq_pop(vm,1);
  StackTable.PrintFooter();
}

SqData SquirrelGetData(HSQUIRRELVM vm, int i, SqOpts OPTS)
{
  SqData IData;
  std::stringstream string_out;
  bool expand = (OPTS & OP_TABLE_EXPAND);
  bool quote = (OPTS & OP_STRING_QUOTE);
  int tmpint = 0;
  switch(sq_gettype(vm,i))
  {
  case OT_NULL:
    IData.Type = "NULL";
    IData.Value = "NULL";
    break;
  case OT_FLOAT:
    SQFloat F;
    sq_getfloat(vm,i,&F);
    IData.Type = "FLOAT";
    string_out << std::setiosflags(std::ios::fixed) << std::setprecision(5) << F;
    IData.Value = string_out.str();
    break;
  case OT_INTEGER:
    SQInteger I;
    sq_getinteger(vm,i,&I);
    IData.Type = "INTEGER";
    string_out << I;
    IData.Value = string_out.str();
    break;
  case OT_STRING:
    const SQChar *ST;
    sq_getstring(vm,i,&ST);
    IData.Type = "STRING";
    string_out << (quote?"'":"") << (ST?ST:"") << (quote?"'":"");
    IData.Value = string_out.str();
    break;
  case OT_BOOL:
    SQBool B;
    sq_getbool(vm,i,&B);
    IData.Type = "BOOLEAN";
    IData.Value = (B?"True":"False");
    break;
  case OT_USERPOINTER:
    SQUserPointer P;
    sq_getuserpointer(vm,i,&P);
    IData.Type = "POINTER";
    string_out << P;
    IData.Value = string_out.str();
    break;
  case OT_USERDATA:
    SQUserPointer D;
    SQUserPointer T;
    sq_getuserdata(vm,i,&D,&T);
    IData.Type = "USER DATA";
    string_out << D;
    IData.Value = string_out.str();
    break;
  case OT_TABLE:
    sq_push(vm,i);
    sq_pushnull(vm);  //null iterator
    string_out << "{";
    if(expand)
      string_out << endl;
    while(SQ_SUCCEEDED(sq_next(vm,-2)))
    {
      if(tmpint++ > 0)
      {
        string_out << ", ";
        if(expand)
          string_out << endl;
      }

      //here -1 is the value and -2 is the key
      string_out << "["<<SquirrelGetData(vm,-2,OPTS).Value << "]: " << SquirrelGetData(vm,-1,OPTS).Value;
      sq_pop(vm,2); //pops key and val before the nex iteration
    }
    sq_pop(vm,2); //pops the null iterator and table
    if(expand)
      string_out << endl;
    string_out << "}";
    if(expand)
      string_out << endl;
    IData.Type = "TABLE";
    IData.Value = string_out.str();
    break;
  case OT_ARRAY:
    sq_push(vm,i);
    sq_pushnull(vm);  //null iterator
    string_out << "{";
    if(expand)
      string_out << endl;
    while(SQ_SUCCEEDED(sq_next(vm,-2)))
    {
      if(tmpint++ > 0)
      {
        string_out << ", ";
        if(expand)
          string_out << endl;
      }
      //here -1 is the value and -2 is the key
      string_out << SquirrelGetData(vm,-1,OPTS).Value;
      sq_pop(vm,2); //pops key and val before the nex iteration
    }
    sq_pop(vm,2); //pops the null iterator and array
    if(expand)
      string_out << endl;
    string_out << "}";
    if(expand)
      string_out << endl;
    IData.Type = "ARRAY";
    IData.Value = string_out.str();
    break;
  case OT_CLOSURE:
    const SQChar *SC;
    string_out << "SQ::";
    sq_getclosurename(vm,i);  //Push name to top of stack
    sq_getstring(vm,-1,&SC);  //retrieve from top
    sq_poptop(vm);            //remove from top
    if(SC)
      string_out << SC << "()";
    else
      string_out << "<Unknown Class/Function>";
    IData.Type = "CLOSURE";
    IData.Value = string_out.str();
    break;
  case OT_NATIVECLOSURE:
    SQUserPointer NID;
    SQUserPointer NIT;
    IData.Type = "C++ CLOSURE";
    sq_getinstanceup(vm,i,&NID,&NIT);
    string_out << NID;
    IData.Value = string_out.str();
    break;
  case OT_GENERATOR:
    IData.Type = "GENERATOR";
    IData.Value = "<ERR:GEN>";
    break;
  case OT_INSTANCE:
    SQUserPointer ID;
    SQUserPointer IT;
    sq_getinstanceup(vm,i,&ID,&IT);
    IData.Type = "INSTANCE";
    string_out << ID;
    IData.Value = string_out.str();
    break;
  case OT_CLASS:
    sq_push(vm,i);
    sq_pushnull(vm);  //null iterator
    string_out << "{";
    if(expand)
      string_out << endl;
    while(SQ_SUCCEEDED(sq_next(vm,-2)))
    {
      if(tmpint++ > 0)
      {
        string_out << ", ";
        if(expand)
          string_out << endl;
      }
      string_out << "["<<SquirrelGetData(vm,-2,OPTS).Value << "]: " << SquirrelGetData(vm,-1,OPTS).Value;
      sq_pop(vm,2); //pops key and val before the nex iteration
    }
    sq_pop(vm,2); //pops the null iterator and table
    if(expand)
      string_out << endl;
    string_out << "}";
    if(expand)
      string_out << endl;
    IData.Type = "CLASS";
    IData.Value = string_out.str();
    break;
  case OT_WEAKREF:
    IData.Type = "WEAKREF";
    IData.Value = "<ERR:WREF>";
    break;
  default:
    IData.Type = "ERROR";
    IData.Value = "ERROR";
    break;
  }
  return IData;
}
