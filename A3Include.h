#ifndef A3INCLUDE_H
#define A3INCLUDE_H

#include <GL/glew.h>

#ifdef __APPLE__
#include <OpenGL/OpenGL.h>
#include <OpenGL/glu.h>
#elif _WIN32
#include <windows.h>
#include <GL/wglew.h>
#else
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>
#endif

#include <cmath>

#include <string>
#include <cctype>
#include <algorithm>

#include <list>
#include <vector>

#include <fstream>
#include <iostream>
#include <iomanip>

#include "bprinter/table_printer.h"
#include "A3Engine/Structure/A3Vector.h"
#include "A3Engine/A3Settings.h"

#include <squirrel.h>
#include <sqrat.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>


using namespace std;

inline std::string trim(const std::string &s)
{
  auto wsfront=std::find_if_not(s.begin(),s.end(),[](int c) {
    return std::isspace(c);
  });
  auto wsback=std::find_if_not(s.rbegin(),s.rend(),[](int c) {
    return std::isspace(c);
  }).base();
  return (wsback<=wsfront ? std::string() : std::string(wsfront,wsback));
}

enum SqOpts
:unsigned char
{
  OP_NONE         = 0,
  OP_TABLE_EXPAND = 1,
  OP_STRING_QUOTE = 2,
};
enum A3_State
{
  ST_NULL,        ///Object is uninitalized
  ST_LOADING,     ///Object is in the process of loading data
  ST_LOADED,      ///Object has loaded data, and is waiting to be initalized
  ST_UPDATED,     ///Object has changed it's data, and is waiting for reinitalize
  ST_ENABLED,     ///Object is initalized and renderable
  ST_DISABLED,    ///Object has been disabled
  ST_DESTROYING,  ///Object is being destroyed
  ST_ERROR        ///Object has encountered a fatal error.
};



struct SqData
{
  std::string Type;
  std::string Value;
};

static bprinter::TablePrinter StackTable(&std::cout,string(1,186));
static bprinter::TablePrinter ArrayTable(&std::cout,string(1,186));
inline SqData SquirrelGetData(HSQUIRRELVM vm, int i, SqOpts OPTS);
/* Useful utility functions */
inline unsigned int NextPow2 (unsigned int x)
{
  --x;
  x |= x >> 1;
  x |= x >> 2;
  x |= x >> 4;
  x |= x >> 8;
  x |= x >> 16;
  return x+1;
}

inline int NextPow2a (int a )
{
  int rval=1;
  while(rval<a) rval<<=1;
  return rval;
}

SQInteger SquirrelFileReader(SQUserPointer File);

inline void SquirrelErrorHandler (HSQUIRRELVM vm, const SQChar *Error, const SQChar *Source, SQInteger Line, SQInteger Column)
{
  cout << "SQEXCEPTION IN " << Source << ":\t" << Error << " on line " << Line << ", column " << Column <<endl;
}

void SquirrelPrintLocals(HSQUIRRELVM,int level);
void SquirrelDebug(HSQUIRRELVM vm, SQInteger Type, const SQChar *Source, SQInteger Line, const SQChar *Function);
void SquirrelPrintTable(Sqrat::Object t);
SQInteger SquirrelRuntimeError(HSQUIRRELVM sqvm);
SqData SquirrelGetData(HSQUIRRELVM vm, int i, SqOpts OPTS);

inline void SquirrelOutputHandler (HSQUIRRELVM VM, const SQChar *s, ...)
{
  va_list arglist;
  va_start(arglist, s);
  vprintf(s,arglist);
  va_end(arglist);
  printf("\n");
}

inline std::wstring s2ws(const std::string& s)
{
  int slength = (int)s.length();
  int len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
  std::wstring r(len, L'\0');
  MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, &r[0], len);
  return r;
}

inline std::string ws2s(const std::wstring& s)
{
  int slength = (int)s.length() + 1;
  int len = WideCharToMultiByte(CP_ACP, 0, s.c_str(), slength, 0, 0, 0, 0);
  std::string r(len, '\0');
  WideCharToMultiByte(CP_ACP, 0, s.c_str(), slength, &r[0], len, 0, 0);
  return r;
}
#endif // A3INCLUDE_H
