#include "W32Main.h"
#include "resource.h"
#include <math.h>
#include <unistd.h>
#include "A3Engine/A3Input.h"
#include "A3Engine/Data/A3Mesh.h"

A3Input *I;
unsigned int BufTex[4];
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{

  WNDCLASSEX wcex;
  HWND  hwnd;
  MSG 	msg;
  BOOL 	bQuit = false;

  /* register window class */
  wcex.cbSize = sizeof(WNDCLASSEX);
  wcex.style = CS_OWNDC;
  wcex.lpfnWndProc = WindowProc;
  wcex.cbClsExtra = 0;
  wcex.cbWndExtra = 0;
  wcex.hInstance = hInstance;
  wcex.hIcon =    (HICON)LoadImage(GetModuleHandle(NULL), MAKEINTRESOURCE(BASE_ICON), IMAGE_ICON, 64, 64, 0);
  wcex.hIconSm =  (HICON)LoadImage(GetModuleHandle(NULL), MAKEINTRESOURCE(BASE_ICON), IMAGE_ICON, 16, 16, 0);
  wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
  wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wcex.lpszMenuName = NULL;
  wcex.lpszClassName = "A3D";
  if (!RegisterClassEx(&wcex))
    return 0;
  wcex.lpfnWndProc = DWindowProc;
  wcex.lpszClassName = "A3DX";
  if (!RegisterClassEx(&wcex))
    return 0;
  HWND hFake = CreateWindow("A3DX", "FAKE", WS_OVERLAPPEDWINDOW | WS_MAXIMIZE | WS_CLIPCHILDREN, 0, 0, CW_USEDEFAULT, CW_USEDEFAULT, NULL, NULL, hInstance, NULL);
  InitializeOpenGL(hFake);
  DestroyWindow(hFake);

  Settings::Handle = hwnd = CreateWindowEx(0,
                            "A3D",
                            "A3D - Nowhere",
                            WS_BORDER|WS_OVERLAPPEDWINDOW,
                            CW_USEDEFAULT,
                            CW_USEDEFAULT,
                            512,
                            512,
                            NULL,
                            NULL,
                            hInstance,
                            NULL);
  //SetWindowLong(hwnd, GWL_STYLE, WS_THICKFRAME);
  //SetWindowPos(hwnd, 0, 200, 200, 256, 256, SWP_FRAMECHANGED);
  EnableOpenGL(hwnd);
  I = new A3Input(hwnd);
  A3World::ShaderLighting = new A3Program("def_base");
  //A3World::ShaderLighting = new A3Program("lighting");
  A3Program *Def_Lighting = new A3Program("def_lighting");
    glGenTextures(4, BufTex);
  A3World::Create();
  A3UI::Create();

  ShowWindow(hwnd, nCmdShow);

    unsigned int DefFBO = 0;
    glGenFramebuffers(1, &DefFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, DefFBO);
    GLenum Buffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2};
    glDrawBuffers(3, Buffers);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, BufTex[0], 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, BufTex[1], 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, BufTex[2], 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,  GL_TEXTURE_2D, BufTex[3], 0);/**/
  /*END TESTING DEFERRED SHADING*/
  A3Mesh *DS = A3Mesh::Create("drawsurf.obj");
  while (!bQuit)
  {
    /* check for messages */
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
      /* handle or dispatch messages */
      if (msg.message == WM_QUIT)
      {
        bQuit = TRUE;
      }
      else
      {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
      }
    }
    glBindFramebuffer(GL_FRAMEBUFFER, DefFBO);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(A3World::ShaderLighting->ID);//Render textures/normals/depth to textures
    //A3UI::Render(A3World::CurrentCamera.FrameDelta);
    A3World::Render(A3World::CurrentCamera.FrameDelta);
    glBindFramebuffer(GL_FRAMEBUFFER,0);
    Def_Lighting->UpdateLights(A3World::CurrentCamera.FrameDelta);
    glActiveTexture(GL_TEXTURE0); glBindTexture(GL_TEXTURE_2D, BufTex[0]);//color
    glActiveTexture(GL_TEXTURE1); glBindTexture(GL_TEXTURE_2D, BufTex[1]);//normal
    glActiveTexture(GL_TEXTURE2); glBindTexture(GL_TEXTURE_2D, BufTex[2]);//position
    glUseProgram(Def_Lighting->ID);//Render composite to a single quad.*/
    DS->Render(0.0, true);

    /*glActiveTexture(GL_TEXTURE2); glBindTexture(GL_TEXTURE_2D, 0);
    glActiveTexture(GL_TEXTURE1); glBindTexture(GL_TEXTURE_2D, 0);
    glActiveTexture(GL_TEXTURE0); glBindTexture(GL_TEXTURE_2D, 0);/**/
    //A3UI::Render(A3World::CurrentCamera.FrameDelta);
    SwapBuffers(GLDC);
  }
  DisableOpenGL(hwnd);
  DestroyWindow(hwnd);
  exit(0);
  return msg.wParam;
}
int MouseX, MouseY;
int ScreenX, ScreenY;
unsigned int MouseButtons;
LRESULT CALLBACK DWindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
  switch (uMsg)
  {
  case WM_CLOSE:
    PostQuitMessage(0);
    break;
  case WM_DESTROY:
    return 0;
  default:
    return DefWindowProc(hwnd, uMsg, wParam, lParam);
  }
}

/*   Vector3f FinalNear = UnProject(Vector3f(MouseX,ScreenY-MouseY,0.0),
                                          Vector2f(ScreenX,ScreenY),
                                          World::CurrentCamera.ModelView,
                                          World::CurrentCamera.Projection);
           Vector3f FinalFar = UnProject(Vector3f(MouseX,ScreenY-MouseY,1.0),
                                         Vector2f(ScreenX,ScreenY),
                                         World::CurrentCamera.ModelView,
                                         World::CurrentCamera.Projection);


           World::RayOrigin = FinalNear;
           World::RayDirection = (FinalFar-FinalNear);
           World::RayDirection.Normalize();
           World::SelectStart = true;*/
LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
  switch (uMsg)
  {
  case WM_CLOSE:
    PostQuitMessage(0);
    break;
  case WM_DESTROY:
    return 0;
    break;
  case WM_INPUT:
    I->GetInput(hwnd, lParam);
    break;
  case WM_SIZE:
    ScreenX = LOWORD(lParam);
    ScreenY = HIWORD(lParam);
    glViewport(0,0,ScreenX,ScreenY);
    A3World::CurrentCamera.Resize(ScreenX,ScreenY);
    A3UI::CurrentCamera.Resize(ScreenX,ScreenY);
    A3UI::Root->SetSize(glm::vec2(ScreenX,ScreenY));
    glBindTexture(GL_TEXTURE_2D, BufTex[0]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, ScreenX, ScreenY, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glBindTexture(GL_TEXTURE_2D, 0);

    glBindTexture(GL_TEXTURE_2D, BufTex[1]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, ScreenX, ScreenY, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glBindTexture(GL_TEXTURE_2D, 0);

    glBindTexture(GL_TEXTURE_2D, BufTex[2]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, ScreenX, ScreenY, 0, GL_RGBA, GL_FLOAT, NULL);
    glBindTexture(GL_TEXTURE_2D, 0);

    glBindTexture(GL_TEXTURE_2D, BufTex[3]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, ScreenX, ScreenY, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glBindTexture(GL_TEXTURE_2D, 0);
    break;
  case WM_KEYDOWN:
  {
    // cout << (char)MapVirtualKeyEx(wParam,2,0) << endl;
    switch (wParam)
    {
    case VK_UP:
      cout << ((cos(A3World::CurrentCamera.Rotation.y) * 150.0f)*A3World::CurrentCamera.FrameDelta) << endl;
      A3World::CurrentCamera.Position.z += (cos(A3World::CurrentCamera.Rotation.y) * 0.25f);
      A3World::CurrentCamera.Position.x -= (sin(A3World::CurrentCamera.Rotation.y) * 0.25f);
      break;
    case VK_DOWN:
      A3World::CurrentCamera.Position.z -= (cos(A3World::CurrentCamera.Rotation.y) * 0.25f);
      A3World::CurrentCamera.Position.x += (sin(A3World::CurrentCamera.Rotation.y) * 0.25f);
      break;
    case VK_LEFT:
      A3World::CurrentCamera.Rotation.y -= glm::radians(1.0f);
      break;
    case VK_RIGHT:
      A3World::CurrentCamera.Rotation.y += glm::radians(1.0f);
      cout << A3World::CurrentCamera.Rotation.y << endl;
      break;
    case VK_PRIOR:
      A3World::CurrentCamera.Position.y -= 0.25f;
      break;
    case VK_NEXT:
      A3World::CurrentCamera.Position.y += 0.25f;
      break;
    case VK_HOME:
      A3World::CurrentCamera.Rotation.x -= glm::radians(1.0f);
      break;
    case VK_END:
      A3World::CurrentCamera.Rotation.x += glm::radians(1.0f);
      break;
    case VK_ESCAPE:
      PostQuitMessage(0);
      break;
    }
  }
  break;

  default:
    return DefWindowProc(hwnd, uMsg, wParam, lParam);
  }

  return 0;
}
void InitializeOpenGL(HWND hwnd)
{
  PIXELFORMATDESCRIPTOR pfd;
  GLDC = GetDC(hwnd);
  memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));
  pfd.nSize= sizeof(PIXELFORMATDESCRIPTOR);
  pfd.nVersion   = 1;
  pfd.dwFlags    = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
  pfd.iPixelType = PFD_TYPE_RGBA;
  pfd.cColorBits = 32;
  pfd.cDepthBits = 8;
  pfd.iLayerType = PFD_MAIN_PLANE;
  SetPixelFormat(GLDC, ChoosePixelFormat(GLDC, &pfd), &pfd);
  GLRC = wglCreateContext(GLDC);
  wglMakeCurrent(GLDC, GLRC);
  if(glewInit() != GLEW_OK)
  {
    cout << "GLEW Could not start, features beyond GL 1.1 will not be available." << endl;
    return;
  }
  wglMakeCurrent(NULL, NULL);
  wglDeleteContext(GLRC);
  GLRC = NULL;
  GLDC = 0;
}

void EnableOpenGL(HWND hwnd)
{
  int PixelAttributes[] =
  {
    WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
    WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
    WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
    WGL_ACCELERATION_ARB, WGL_FULL_ACCELERATION_ARB,
    WGL_SWAP_METHOD_ARB, WGL_SWAP_EXCHANGE_ARB,
    WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
    WGL_COLOR_BITS_ARB, 32,
    WGL_DEPTH_BITS_ARB, 24,
    //WGL_ALPHA_BITS_ARB, 8,
    WGL_SAMPLE_BUFFERS_ARB, 1,
    WGL_SAMPLES_ARB, 1,
    0,
  };
  int ContextAttributes[] =
  {
    WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
    WGL_CONTEXT_MINOR_VERSION_ARB, 1,
    0
  };
  int PixelFormat;
  unsigned int FormatCount;

  PIXELFORMATDESCRIPTOR pfd;
  if((GLDC = GetDC(hwnd)) == 0)
  {
    cout << "Failed to acquire device context" << endl;
    return;
  }
  if(!wglChoosePixelFormatARB(GLDC, PixelAttributes, NULL, 1, &PixelFormat, &FormatCount))
  {
    cout << "Could not generate pixel format"<< endl;
    return;
  }
  if(!SetPixelFormat(GLDC, PixelFormat, &pfd))
  {
    cout << "Could not set pixel format" << endl;
    return;
  }

  if((GLRC = wglCreateContextAttribsARB(GLDC, 0, ContextAttributes)) == nullptr)
  {
    cout << "Could not create a Render Context" << endl;
    return;
  }

  if(!wglMakeCurrent(GLDC, GLRC))
  {
    cout << "Could not set Render Context" << endl;
    return;
  }

  cout << "OpenGL Version:\t" << glGetString(GL_VERSION) << endl;
  cout << "GLSL Version:\t" << glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;
  if(glewInit() != GLEW_OK)
  {
    cout << "GLEW Could not start, features beyond GL 1.1 will not be available." << endl;
    return;
  }

  glClearDepth(1.0f);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);
  glClearColor(36.0/255.0,36.0/255.0,36.0/255.0,1.0);
  glDepthFunc(GL_LEQUAL);
}

void DisableOpenGL (HWND hwnd)
{
  A3World::Destroy();
  A3UI::Destroy();
  wglMakeCurrent(NULL, NULL);
  wglDeleteContext(GLRC);
  ReleaseDC(hwnd, GLDC);
}

/*glm::vec3 UnProject(glm::vec3 Mouse, glm::vec2 WindowSize, A3Matrix &ModelView, A3Matrix &Projection)
{
    A3Matrix UnProj;
    ModelView.Multiply(Projection,UnProj);
    UnProj.Invert();
    A3Vector4f In(Mouse.X/WindowSize.X*2.0f-1.0f,
                  Mouse.Y/WindowSize.Y*2.0f-1.0f,
                  2.0f*Mouse.Z-1.0f,
                  1.0f);
    A3Vector4f Out = UnProj*In;
    float r = 1/Out.W;
    return A3Vector3f(Out.X*r,Out.Y*r,Out.Z*r);
}*/
