#ifndef W32MAIN_H
#define W32MAIN_H

#include <windows.h>
#include <iostream>

#include "A3Include.h"
#include "A3Engine/A3UI.h"
#include "A3Engine/A3World.h"

using namespace std;

static HDC 		GLDC;
static HGLRC 	GLRC;

LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK DWindowProc(HWND, UINT, WPARAM, LPARAM);
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow);
void InitializeOpenGL(HWND hwnd);
void EnableOpenGL(HWND hwnd);
void DisableOpenGL(HWND);

glm::vec3 UnProject(glm::vec3 Mouse, glm::vec2 WindowSize, glm::mat4 &ModelView, glm::mat4 &Projection);

#endif // W32MAIN_H
