#ifndef BPRINTER_TABLE_PRINTER_H_
#define BPRINTER_TABLE_PRINTER_H_

#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <sstream>
#include <cmath>
#include <windows.h>

namespace bprinter {
class endl {};
/** \class TablePrinter

  Print a pretty table into your output of choice.

  Usage:
    TablePrinter tp(&std::cout);
    tp.AddColumn("Name", 25);
    tp.AddColumn("Age", 3);
    tp.AddColumn("Position", 30);

    tp.PrintHeader();
    tp << "Dat Chu" << 25 << "Research Assistant";
    tp << "John Doe" << 26 << "Professional Anonymity";
    tp << "Jane Doe" << tp.SkipToNextLine();
    tp << "Tom Doe" << 7 << "Student";
    tp.PrintFooter();

  \todo Add support for padding in each table cell
  */
class TablePrinter {
public:
  TablePrinter(std::ostream * output, const std::string & separator = "|");
  ~TablePrinter();

  int get_num_columns() const;
  int get_table_width() const;
  void set_separator(const std::string & separator);
  void set_indent(const std::string &indentor, const int indent);
  void set_title(const std::string &title);
  void set_highlight(const int row);

  void AddColumn(const std::string & header_name, int column_width);
  void PrintHeader();
  void PrintFooter();

  TablePrinter& operator<<(endl input) {
    while (j_ != 0) {
      *this << separator_;
    }
    return *this;
  }

  // Can we merge these?
  TablePrinter& operator<<(float input);
  TablePrinter& operator<<(double input);

  template<typename T> TablePrinter& operator<<(T input) {
    if (j_ == 0)
    {
      *out_stream_ << std::setfill(indentor_[0]) << std::setw(table_indent_) << "" << separator_;
    }

    // Leave 3 extra space: One for negative sign, one for zero, one for decimal
    if(table_row_highlight_ >= 0 && i_ == table_row_highlight_)
      SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED);

    *out_stream_ << std::setw(column_widths_.at(j_)-1) << input << " ";

    if(table_row_highlight_ >= 0 && i_ == table_row_highlight_)
      SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED);

    if (j_ == get_num_columns()-1) {
      *out_stream_ << separator_ << std::endl;
      i_ = i_ + 1;
      j_ = 0;
    } else {
      *out_stream_ << separator_;
      j_ = j_ + 1;
    }

    return *this;
  }

private:
  void PrintHorizontalLine();

  template<typename T> void OutputDecimalNumber(T input);
  int table_width_ = 0;
  int max_precision_ = 5;
  int table_indent_ = 0;
  int table_row_highlight_ = -1;
  int i_ = 0;
  int j_ = 0;
  std::ostream * out_stream_;
  std::vector<std::string> column_headers_;
  std::vector<int> column_widths_;
  std::string separator_;
  std::string indentor_;
  std::string title_;




};

}

#include "impl/table_printer.tpp.h"
#endif
