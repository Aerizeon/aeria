#version 130

#if __VERSION__ < 130
	varying in vec3		FragmentNormal;
	varying in vec2		FragmentTextureCoord;
	varying in vec4		FragmentShadowCoord;
	
	varying out vec4	FragmentColor;
#else
	in vec3				FragmentNormal;
	in vec2				FragmentTextureCoord;
	in vec4				FragmentShadowCoord;
	
	out	vec4			FragmentColor;
#endif

uniform mat4		CameraCombinedMatrix;
uniform mat4		ObjectCombinedMatrix;
uniform sampler2D	ObjectTextureDiffuse;
uniform vec4		ObjectColor;

uniform mat4		ShadowCombinedMatrix;
uniform sampler2D	ShadowTextureDiffuse;

void main(void)
{
	FragmentColor = vec4(1.0, 1.0, 1.0, 1.0);
}
