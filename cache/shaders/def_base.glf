#version 400

#if __VERSION__ < 130
	varying in vec3		FragmentNormal;
	varying in vec3		FragmentPosition;
	varying in vec2		FragmentTextureCoord;
	varying in vec4		FragmentShadowCoord;
	
	varying out vec4[2]	FragmentOutput;
#else
	in vec3				FragmentNormal;
	in vec3				FragmentPosition;
	in vec2				FragmentTextureCoord;
	in vec4				FragmentShadowCoord;
	
	layout (location = 0) out vec4			ColorOutput;
	layout (location = 1) out vec4			NormalOutput;
	layout (location = 2) out vec4			PositionOutput;
#endif

uniform mat4		CameraCombinedMatrix;
uniform mat4		ObjectCombinedMatrix;
uniform mat4		ObjectSeparateMatrix;
uniform vec4		ObjectColor;

uniform sampler2D	TextureDiffuse;
uniform sampler2D	TextureNormal;
uniform	sampler2D	TextureMask;


void main(void)
{
	vec4	DiffuseColor	= texture2D(TextureDiffuse, FragmentTextureCoord);
	float	MaskColor		= texture2D(TextureMask, FragmentTextureCoord).r;
	ColorOutput =   vec4(DiffuseColor.rgb, DiffuseColor.a * MaskColor);
	if(DiffuseColor.a - (1.0 - (MaskColor-0.25)) < 0.1)
		discard;
	NormalOutput	= vec4(FragmentNormal, 1.0);
	PositionOutput	= vec4(FragmentPosition, 1.0);
}
