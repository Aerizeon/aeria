#version 400
#define MAX_LIGHTS 100

#if __VERSION__ < 130
	varying in vec2		FragmentTextureCoord;
	
	varying out vec4	FragmentColor;
#else
	in vec2				FragmentTextureCoord;
	
	out	vec4			FragmentColor;
#endif

struct LightSettings
{
	vec3	Position;
	vec3	Color;
	vec3	Attenuation;
};

uniform LightBuffer
{
	LightSettings	LightDynamic[MAX_LIGHTS];
	LightSettings	LightStatic;
	int				LightCount;
};

uniform sampler2D	ColorBuffer;
uniform sampler2D	NormalBuffer;
uniform	sampler2D	PositionBuffer;


vec3 UC2ToneMap(in vec3 x)
{
	return (((0.15 * x + 0.05) * x + 0.004)/((0.15 * x + 0.5) * x + 0.06)) - 0.066666;
}
const vec3 GammaCorrection = vec3(1.0/2.2);

void main()
{
	FragmentColor		= texture2D(ColorBuffer, FragmentTextureCoord);
	vec4 NormalBuf		= texture2D(NormalBuffer, FragmentTextureCoord);
	vec4 PositionBuf	= texture2D(PositionBuffer, FragmentTextureCoord);
	vec3 FinalColor 	= LightStatic.Color;
	float NdotL;
	vec3 LPosition;
	float LDistance;
	float Attenuation;
	for(int i = 0; i < LightCount; i++)
	{
		LPosition 	= LightDynamic[i].Position - PositionBuf.xyz;
		LDistance 	= length(LPosition);
		NdotL		= max(0.0, dot(NormalBuf.xyz, normalize(LPosition)));
		//Attenuation = 1.0f/(1.0f + LightDynamic[i].Radius * pow(LDistance,2));
		Attenuation	= 1.0f/(LightDynamic[i].Attenuation.x + (LightDynamic[i].Attenuation.y * LDistance) + (LightDynamic[i].Attenuation.z * pow(LDistance,2)));
		//if(LDistance < 2.0)
			FinalColor	+=  LightDynamic[i].Color * NdotL * Attenuation;
	}

	FragmentColor.rgb *= pow(FinalColor, GammaCorrection);
}
