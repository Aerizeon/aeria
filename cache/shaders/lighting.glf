#version 150

#if __VERSION__ < 130
	varying in vec3		FragmentNormal;
	varying in vec3		FragmentPosition;
	varying in vec2		FragmentTextureCoord;
	varying in vec4		FragmentShadowCoord;
	
	varying out vec4	FragmentColor;
#else
	in vec3				FragmentNormal;
	in vec3				FragmentPosition;
	in vec2				FragmentTextureCoord;
	in vec4				FragmentShadowCoord;
	
	out	vec4			FragmentColor;
#endif

uniform mat4		CameraCombinedMatrix;
uniform mat4		ObjectCombinedMatrix;
uniform mat4		ObjectSeparateMatrix;
uniform sampler2D	ObjectTextureDiffuse;
uniform vec4		ObjectColor;

uniform mat4		ShadowCombinedMatrix;
uniform sampler2D	ShadowTextureDiffuse;
uniform	sampler2D	ObjectMaskTexture;

void main()
{
	float ShadowPercent = 1.0;
	vec3 GlobalAmbient	= vec3(0.05,0.05,0.05);
	vec3 LightAmbient 	= vec3(1.0,1.0,1.0);
	vec3 LightPos 		= vec3(0,1.0,-9);
	float MatAmbient	= 0.0;
	float MatDiffuse	= 1.0;
	float MatSpecular	= 3.0;
	float AttenConst	= 0.0001;
	float AttenLinear	= 0.0001;
	float AttenQuad		= 0.0007;
	vec3  LPosition 	= LightPos - FragmentPosition;
	float LDistance 	= length(-LPosition);
	//float LAttenuation	= 1.0/(AttenConst + (AttenLinear * LDistance) + (AttenQuad * LDistance * LDistance));
	float LAttenuation = 1.0;
	float NdotL			= dot(normalize(FragmentNormal), normalize(LPosition));
	float LIntensity	= max(NdotL * LAttenuation, 0.0);
	vec3 FinalColor		= LightAmbient * LIntensity;

	
	
	float Alpha = texture2D(ObjectTextureDiffuse, FragmentTextureCoord).a * texture2D(ObjectMaskTexture, FragmentTextureCoord).r;
	FragmentColor =   vec4(FinalColor * texture2D(ObjectTextureDiffuse, FragmentTextureCoord).rgb, texture2D(ObjectTextureDiffuse, FragmentTextureCoord).a * Alpha);
}
