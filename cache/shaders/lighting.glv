#version 150

#if __VERSION__ < 130
	varying in vec3		VertexPosition;
	varying in vec3		VertexNormal;
	varying in vec2		VertexTextureCoord;
	
	varying out vec3	FragmentNormal;
	varying out vec3	FragmentPosition;
	varying out vec2	FragmentTextureCoord;
	varying out vec4	FragmentShadowCoord;
#else
	in vec3		VertexPosition;
	in vec3		VertexNormal;
	in vec2		VertexTextureCoord;
	
	out vec3	FragmentNormal;
	out vec3	FragmentPosition;
	out vec2	FragmentTextureCoord;
	out vec4	FragmentShadowCoord;
#endif

uniform mat4		CameraCombinedMatrix;
uniform mat4		ObjectCombinedMatrix;
uniform mat4		ObjectSeparateMatrix;
uniform sampler2D	ObjectTextureDiffuse;
uniform vec4		ObjectColor;

uniform mat4		ShadowCombinedMatrix;
uniform sampler2D	ShadowTextureDiffuse;

void main()
{
	FragmentTextureCoord 	= VertexTextureCoord;
	if(VertexNormal == vec3(0,0,0))
		FragmentNormal 		= normalize(transpose(inverse(mat3(ObjectSeparateMatrix))) * vec3(0,1,0));
	else
		FragmentNormal 		= normalize(transpose(inverse(mat3(ObjectSeparateMatrix))) * VertexNormal);
	FragmentPosition		= vec3(ObjectSeparateMatrix * vec4(VertexPosition, 1.0));
	gl_Position 			= ObjectCombinedMatrix * vec4(VertexPosition,1.0);
}