#version 430

#if __VERSION__ < 130
	varying in vec3		FragmentNormal;
	varying in vec2		FragmentTextureCoord;
	varying in vec4		FragmentShadowCoord;
	
	varying out vec4	FragmentColor;
#else
	in vec3				FragmentNormal;
	in vec2				FragmentTextureCoord;
	in vec4				FragmentShadowCoord;
	
	out	vec4			FragmentColor;
#endif

uniform mat4		CameraCombinedMatrix;
uniform mat4		ObjectCombinedMatrix;
uniform sampler2D	ObjectTextureDiffuse;
uniform vec4		ObjectColor;

uniform mat4		ShadowCombinedMatrix;
uniform sampler2D	ShadowTextureDiffuse;
 
void main()
{
	if(FragmentTextureCoord.x == 0 || FragmentTextureCoord.x == 1 || FragmentTextureCoord.y == 0 || FragmentTextureCoord.y == 1 )
	{
		FragmentColor = vec4(255,0,0,255);
	}
	else
	{
		FragmentColor = texture2D(ObjectTextureDiffuse, FragmentTextureCoord) * ObjectColor;
	}
	//FragmentColor = vec4(texture2D(ObjectTextureDiffuse, FragmentTextureCoord).a,texture2D(ObjectTextureDiffuse, FragmentTextureCoord).a,texture2D(ObjectTextureDiffuse, FragmentTextureCoord).a, texture2D(ObjectTextureDiffuse, FragmentTextureCoord).a);
}
