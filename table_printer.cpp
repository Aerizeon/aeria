#include "bprinter/table_printer.h"
#include <stdexcept>
#include <iomanip>
#include <stdexcept>

namespace bprinter {
TablePrinter::TablePrinter(std::ostream * output, const std::string & separator) {
  out_stream_ = output;
  separator_ = separator;

}

TablePrinter::~TablePrinter() {

}

int TablePrinter::get_num_columns() const {
  return column_headers_.size();
}

int TablePrinter::get_table_width() const {
  return table_width_;
}

void TablePrinter::set_separator(const std::string &separator) {
  separator_ = separator;
}

void TablePrinter::set_indent(const std::string &indentor, const int indent) {
  table_indent_ = indent;
  indentor_ = indentor;
}

void TablePrinter::set_title(const std::string &title) {
  title_ = title;
}

void TablePrinter::set_highlight(const int row) {
  table_row_highlight_ = row;
}

/** \brief Add a column to our table
 **
 ** \param header_name Name to be print for the header
 ** \param column_width the width of the column (has to be >=5)
 ** */
void TablePrinter::AddColumn(const std::string & header_name, int column_width) {
  if (column_width < 4) {
    throw std::invalid_argument("Column size has to be >= 4");
  }

  column_headers_.push_back(header_name);
  column_widths_.push_back(column_width);
  table_width_ += column_width + separator_.size(); // for the separator
}

void TablePrinter::PrintHorizontalLine() {
  *out_stream_ << "+"; // the left bar

  for (int i=0; i<table_width_-1; ++i)
    *out_stream_ << "-";

  *out_stream_ << "+"; // the right bar
  *out_stream_ << "\n";
}

void TablePrinter::PrintHeader() {
  /*Begin Title Top Line*/
  *out_stream_ << std::setfill(indentor_[0]) << std::setw(table_indent_) << "";
  *out_stream_ << std::string(1,201);
  *out_stream_ << std::setfill((char)205) << std::setw(get_table_width()-1) << "";
  *out_stream_ << std::string(1,187) << std::endl;
  /*End Title Top Line*/

  /*Begin Title Line*/
  float HalfTable = floor(((float)get_table_width())/2.0f);
  float HalfTitle = ceil(((float)title_.size())/2.0f);
  *out_stream_ << std::setfill(indentor_[0]) << std::setw(table_indent_) << "";
  *out_stream_ << separator_ << std::setfill(' ') << std::setw(HalfTable+HalfTitle-1) << title_;
  *out_stream_ << std::setfill(' ') << std::setw(get_table_width()-(HalfTable+HalfTitle)) << "";
  *out_stream_ << separator_ << std::endl;
  /*End Title Line*/

  /*Begin Header Top Line*/
  *out_stream_ << std::setfill(indentor_[0]) << std::setw(table_indent_) << "";
  *out_stream_ << std::string(1,204);
  for (int i=0; i<get_num_columns(); ++i)
  {
    for(int c=0; c<column_widths_.at(i); c++)
      *out_stream_ << std::string(1,205);
    if (i != get_num_columns()-1)
    {
      *out_stream_ << std::string(1,203);
    }
  }
  *out_stream_ << std::string(1,185) << std::endl;
  /*End Header Top Line*/

  /*Begin Header Line*/
  *out_stream_ << std::setfill(indentor_[0]) << std::setw(table_indent_) << "";
  *out_stream_ << std::string(1,186);

  for (int i=0; i<get_num_columns(); ++i)
  {
    *out_stream_ << std::setw(column_widths_.at(i)-1) << column_headers_.at(i).substr(0, column_widths_.at(i)-1) << " ";
    if (i != get_num_columns()-1)
      *out_stream_ << separator_;
  }
  *out_stream_ << separator_ << std::endl;
  /*End Header Line*/

  /*Begin Header Bottom Line*/
  *out_stream_ << std::setfill(indentor_[0]) << std::setw(table_indent_) << "";
  *out_stream_ << std::string(1,204);
  for (int i=0; i<get_num_columns(); ++i)
  {
    for(int c=0; c<column_widths_.at(i); c++)
      *out_stream_ << std::string(1,205);
    if (i != get_num_columns()-1)
    {
      *out_stream_ << std::string(1,206);
    }
  }
  *out_stream_ << std::string(1,185) << std::endl;
  /*End Header Bottom Line*/
}

void TablePrinter::PrintFooter() {
  *out_stream_ << std::setfill(indentor_[0]) << std::setw(table_indent_) << "";
  *out_stream_ << std::string(1,200);
  for (int i=0; i<get_num_columns(); ++i)
  {
    for(int c=0; c<column_widths_.at(i); c++)
      *out_stream_ << std::string(1,205);
    if (i != get_num_columns()-1) {
      *out_stream_ << std::string(1,202);
    }
  }
  *out_stream_ << std::string(1,188) << std::endl;
  title_ = "";
  i_ = 0;
  j_ = 0;
  table_row_highlight_ = -1;
}

TablePrinter& TablePrinter::operator<<(float input) {
  OutputDecimalNumber<float>(input);
  return *this;
}

TablePrinter& TablePrinter::operator<<(double input) {
  OutputDecimalNumber<double>(input);
  return *this;
}

}
